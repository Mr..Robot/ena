@extends('layouts.app', ['page' => 'search'])
@section('styles')
<link href="{{ asset('/css/admin/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/admin/AdminLTE.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/pace/pace.min.css') }}">
<link href="{{ asset('/css/star-rating.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/themes/krajee-fa/theme.css') }}" rel="stylesheet" type="text/css" media="all">
<style>
  .clear-rating {
    display: none !important;
  }
</style>
@endsection
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card-body">
        <div class="box box-default">
          <div class="box-header with-border text-center">
            <h3 class="box-title"> البحث بالاعراض للحصول على افضل اخصائي</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
          </div>
          <div class="box-body">
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    {{-- <div class="form-group {{ $errors->has('pecialty_id') ? ' has-error' : '' }}" id="specialties">
                        <label for="pecialty_id">التخصص</label>
                        <select name="pecialty_id" class="specSelect form-control select2" required id="pecialty_id" >
                          <option value="-1" >كل التخصصات</option>
                            @foreach (App\Specialty::onlyActive()->all() as $specialty)
                            <option value="{{ $specialty->id }}">{{ $specialty->namear }} | {{ $specialty->nameen }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    
                    <div id = "symptoms" class="form-group">
                        <label for="symptomsSelect">الأعراض</label>
                      
                            <select name="symptomsSelect" id="symptomsSelect" multiple="multiple" class="select2 form-control"  aria-placeholder="اختر اعراض">
                             @foreach (App\Symptom::onlyActive()->all() as $Symptom)
                              <option value="{{ $Symptom->id }}">{{ $Symptom->commonname }} | {{ $Symptom->name }}</option>
                              @endforeach
                            </select>
                            {{-- @if ($errors->has('symptomsSelect'))
                            <span class="is-invalid" role="alert">
                              <strong>{{ $errors->first('symptomsSelect') }}</strong>
                            </span>
                            @endif                           --}}
                          </div>
                          <div class="form-group">
                          <button type="submit" class="btn btn-block btn-primary btn-flat ajax">بحث</button>
                    
                    </div>
                </div>                
              </div>
                {{--  </form>  --}}
            </div>
  
          </div>
          <!-- /.box -->
          <!-- Default box -->
          <div class="box" id="json_result">
              <div class="box-header with-border">
                <h3 class="box-title">قائمة بالامراض المحتملة</h3>
      
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                          title="Collapse">
                    <i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12 text-center">
                <div class="ajax-content row">

                </div>
              </div>
            </div>
              </div>
              </div>
            <!-- /.box -->
            <div class="box" id="json_result">
                <div class="box-header with-border">
                  <h3 class="box-title">قائمة الاخصائيين </h3>
        
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                      <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                      <i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 text-center">

                      <div class="ajax-content2 row">
  
                          </div>
                </div>
              </div>
                </div>
              </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('/js/admin/select2.full.min.js') }}"></script>
<script src="{{ asset('PACE/pace.min.js') }}"></script>
<script src="{{ asset('/js/star-rating.js') }}"></script>
<script src="{{ asset('/themes/krajee-fa/theme.js') }}"></script>
<script type="text/javascript">
  $(document).ajaxStart(function () {Pace.restart()  })
  var disease = [];

  // var fruits = ["Banana", "Orange", "Apple", "Mango"]; fruits.push("Kiwi")
  $(function () {
    $('.select2').select2()

        $('.kv-fa').rating({
            theme: 'krajee-fa',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });
        
       
        $("#rate").rating();
  })
 
    $('.ajax').click(function () {
      //  if ($(".specSelect" ).val() >= 1){
    //  var uldiv = $('#symptomsSelect').siblings('span.select2').find('ul');
    //  var count = uldiv.find('li').length - 1;
      var dataVars = {};
      dataVars['_token'] = "{{ csrf_token() }}";
      dataVars['ids'] = $("#symptomsSelect").val();
      
      $.ajax( 'http://ena.test/result_doctors', {
        type: 'GET',
        dataType: 'json',
        data: dataVars,
        success: function( resp ) {
          // console.log(resp);
          // $('#json_result').show();
          $(".ajax-content" ).empty();
          
          // $.each(resp.docs, function() { 
          //  // disease[this.id] = this;
      
          //  
          // });
          var i = 0;
          $.each(resp, function() { 
            disease.push(this); 
         //  console.log(disease[0].doctors);
            
            $("<div class='col-md-4 disease' onClick='reply_click("+i+")' id="+this.id+"><div class='external-event bg-light-blue'>" + this.commonname +' | ' + this.name +"</div></div>").appendTo(".ajax-content");
          i++;
          });
          $('.kv-fa').rating({ theme: 'krajee-fa', filledStar: '<i class="fa fa-star"></i>', emptyStar: '<i class="fa fa-star-o"></i>'
            }); $("#rate").rating();
        },
        error: function( req, status, err ) {
          console.log( req );
        }
   }); 
  //disease[]
    });


 
  // $(".specSelect" ).change(function() {
  //   if (this.value >= 1){
  //     $.ajax( 'http://ena.test/result/'+ this.value, {
  //       type: 'GET',
  //       dataType: 'json',
  //       success: function( resp ) {
  //         $('#serch').show();
  //         $( "#symptomsSelect" ).empty();
  //         $.each(resp, function() { 
  //           $("<option value='" + this.id + "'>" + this.commonname +' | ' + this.name +"</option>").appendTo("#symptomsSelect");
  //         });
  //       },
  //       error: function( req, status, err ) {
  //         console.log( req );
  //       }
  //     });
  //   } else {
  //     alert("أختار تخصص صحيح");
  //   }
  // });
 

function reply_click(i) { 
console.log(disease[0].doctors[0]);
//for (var key in data.messages) { var obj = data.messages[key]; // ... }



  $(".ajax-content2" ).empty();
  $.each(disease[i].doctors, function() {
    $("<div class='col-md-4'><div class='box box-primary'><div class='box-body'><div class='col-sm-4'><img class='profile-user-img img-responsive' src=/avatars/" + this.user.avatar + " alt='User profile picture'><input id='rate' name='rate' data-size='xs' class='rating kv-fa rating-loading' data-min='0' data-max='5' data-step='0.5' value="+ this.rated +"></div><div class='col-sm-8'><h3 class='profile-username text-right'>"+ this.user.person.fullName +"</h3><h5 class='text-right'> " + this.specialty.namear+ "</h5><br><a href='doctors/" + this.user.id + "' class='btn btn-info pull-left'><b>الملف الشخصي</b></a><a href='chats/" + this.user.id + "' class='btn btn-primary pull-right'><b>Message</b></a></div></div></div></div>").appendTo(".ajax-content2");
    });
  $(".rating").rating();
   }
  </script>
@endsection
