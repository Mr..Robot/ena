<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{ asset('/css/admin/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/_all-skins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/AdminLTE.min.css') }}" rel="stylesheet">
    @yield('styles')
    <style>
    .log-in-menu{
      width: 180px !important;
    }
    </style>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body dir="rtl" class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
                <a class="navbar-brand" >

          <a href="{{ url('/') }}" class="navbar-brand"><b>Doctor Online</a>
        </div>
         <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li {{ $page == 'doctors' ? ' class=active' : '' }}><a href="{{ url('/doctors/all') }}">الاخصائيين</a></li>
            <li {{ $page == 'search' ? ' class=active' : '' }}><a href="{{ url('/search') }}">البحث</a></li>
          </ul>
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
              @guest
              @else
            <li class="messages-menu">
              <a href="/chats/" >
                <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{ Chat::messages()->for(Auth::user())->unreadCount() }}</span>
              </a>
              {{-- <ul class="dropdown-menu">
                <li class="header">لديك {{ Chat::messages()->for(Auth::user())->unreadCount() }} رسائل غير مقروة</li>
                <li>
                  <ul class="menu">
                    <li>
                      <a href="#">
                        <div class="pull-left">
                        <img src="/avatars/{{Auth::user()->avatar}}" class="img-circle" alt="User Image">
                        </div>
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="footer"><a href="{{url('/chats')}}">رؤية جميع الرسائل</a></li>
              </ul> --}}
            </li>
            @endguest
            
            @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">دخول</a>
            </li>
            @if (Route::has('register'))
                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">سجل الان</a>
                </li> --}}
                <li class="dropdown user user-menu">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="hidden-xs">  سجل الان</span>
                  </a>
                  <ul class="dropdown-menu log-in-menu">
                    <li class="user-footer">
                      <div class="pull-left">
                        <a href="{{ route('register') }}" class="btn btn-default btn-flat">مستخدم</a>
                      </div>
                      <div class="pull-right">
                        <a href="/doctors/register" class="btn btn-default btn-flat">طبيب</a>
                      </div>
                    </li>
                  </ul>
                </li>                
            @endif
        @else
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="/avatars/{{Auth::user()->avatar}}" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">  {{ Auth::user()->name }} </span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="/avatars/{{Auth::user()->avatar}}" class="img-circle" alt="User Image">

                  <p>
                        {{ Auth::user()->name }} 
                    <small>مسجل منذ مارس 2019</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="/doctors/{{ Auth::id() }}" class="btn btn-default btn-flat">الملف الشخصي</a>
                  </div>
                  <div class="pull-right">
                      <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                       تسجيل الخروج
                   </a>
 
                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                   </form>
 
                  </div>
                </li>
              </ul>
            </li>
            <li class="messages-menu">
              <a href="/profile" >
                <i class="fa fa-cog"></i>
              </a>
            </li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div style="background-image: url('/img/banner/home.jpg');
  height: 500px; background-position: fixed; background-repeat: no-repeat; background-size: cover; " class="content-wrapper">
    <div class="container">
    <section class="content">
              {{-- GGGGGGGGGGGGGGgg --}}
              @yield('content')
      </section>
    </div>
  </div>
</div>


<script src="{{ asset('/js/admin/jquery.js') }}"></script>
<script src="{{ asset('/js/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/js/js/fastclick.js') }}"></script>
<script src="{{ asset('/js/js/adminlte.min.js') }}"></script>

@if (session('message'))
    <script>
        showNotice("{{ session('type') }}", "{{ session('message') }}");
    </script>
@endif

{{-- You can put page wise javascript in scripts section --}}
@yield('scripts')
</body>
</html>