<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{ asset('/css/admin/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/vendor.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/admin/app.css') }}" rel="stylesheet">

    {{-- You can put page wise internal css style in styles section --}}
    @yield('styles')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body dir="rtl" class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        {{-- Header --}}
        <header class="main-header">

            {{--  Logo  --}}
            <a href="{{ route('dashboard.index') }}" class="logo">
                <span class="logo-mini">{{ config('app.name') }}</span>
                <span class="logo-lg">{{ config('app.name') }}</span>
            </a>

            {{--  Header Navbar  --}}
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        {{--  User Account Menu  --}}
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/avatars/{{ Auth::user()->avatar }}" class="user-image" alt="Admin avatar">

                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="/avatars/{{ Auth::user()->avatar }}" class="img-circle" alt="Admin avatar">

                                    <p>{{ Auth::user()->name }}</p>
                                </li>

                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="/doctors/{{ Auth::id() }}" class="btn btn-default btn-flat">
                                            الملف الشخصي
                                        </a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat">تسجيل الخروج</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            {{--  Sidebar  --}}
            <section class="sidebar">

                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="/avatars/{{ Auth::user()->avatar }}" class="img-circle" alt="Admin avatar">
                    </div>

                    <div class="pull-left info">
                        <p>{{ Auth::user()->name }}</p>
                    </div>
                </div>

                {{--  Sidebar Menu  --}}
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MENU</li>
@if (Auth::user()->isAdmin)
                    
                    <li {{ $page == 'specialty' ? ' class=active' : '' }}>
                        <a href="{{ route('dashboard.specialties.index') }}">
                            <i class="fa fa-stethoscope"></i>
                            <span>التخصصات</span>
                        </a>
                    </li>

                    <li {{ $page == 'disease' ? ' class=active' : '' }}>
                        <a href="{{ route('dashboard.diseases.index') }}">
                            <i class="fa fa-warning"></i>
                            <span>الأمراض</span>
                        </a>
                    </li>

    

                    <li {{ $page == 'symptom' ? ' class=active' : '' }}>
                        <a href="{{ route('dashboard.symptoms.index') }}">
                            <i class="fa fa-info-circle"></i>
                            <span>الأعراض</span>
                        </a>
                    </li>
@endif
                    <li {{ $page == 'certificate' ? ' class=active' : '' }}>
                        <a href="{{ route('dashboard.certificates.index') }}">
                            <i class="fa fa-file-image-o"></i>
                            <span>الشهادات</span>
                        </a>
                    </li>

                    <li {{ $page == 'appointment' ? ' class=active' : '' }}>
                        <a href="{{ route('dashboard.appointments.index') }}">
                            <i class="fa fa-calendar"></i>
                            <span>المواعيد</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>


        <div class="content-wrapper">
            {{--  Page header  --}}
            <section class="content-header">
                <h1>
                    @yield('title')
                </h1>
            </section>

            {{--  Page Content  --}}
            <section class="content container-fluid">
                @if ($errors->all())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $message)
                            <li>{{ $message }}</li>
                        @endforeach
                    </ul>
                @endif

                @yield('content')
            </section>
        </div>

    </div>
    <script src="{{ asset('/js/admin/jquery.js') }}"></script>
    <script src="{{ mix('/js/admin/vendor.js') }}"></script>
    <script src="{{ mix('/js/admin/app.js') }}"></script>

    @if (session('message'))
        <script>
            showNotice("{{ session('type') }}", "{{ session('message') }}");
        </script>
    @endif

    {{-- You can put page wise javascript in scripts section --}}
    @yield('scripts')
</body>
</html>