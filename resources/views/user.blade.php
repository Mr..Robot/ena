@extends('layouts.app', ['page' => 'user']) 
@section('title', 'الملف الشخصي') 
@section('content')
<div class="row">
    <!-- /.col -->
    <div class="col-md-12">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li><a href="#settings" class="active" aria-expanded="true" data-toggle="tab">تعديل البيانات</a></li>
            </ul>
            <div class=" active tab-content">
                <div class=" tab-pane" id="settings">
                    <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('doctor.update', ['person' => $person->id]) }}">
                        @csrf @method('PUT')
    
                        <div class="box-body">
                            <div class="form-group">
                                <label for="first_name">الإسم الأول</label>
                                <input type="text" class="form-control" name="first_name" required placeholder="الإسم الأول" value="{{ old('first_name', $person->first_name) }}"
                                    id="first_name">
                            </div>
    
                            <div class="form-group">
                                <label for="second_name">إسم الأب</label>
                                <input type="text" class="form-control" name="second_name" required placeholder="إسم الأب" value="{{ old('second_name', $person->second_name) }}"
                                    id="second_name">
                            </div>
    
                            <div class="form-group">
                                <label for="last_name">إسم الجد</label>
                                <input type="text" class="form-control" name="last_name" required placeholder="إسم الجد" value="{{ old('last_name', $person->last_name) }}"
                                    id="last_name">
                            </div>
    
                            <div class="form-group">
                                <label for="family_name">اللقب</label>
                                <input type="text" class="form-control" name="family_name" required placeholder="اللقب" value="{{ old('family_name', $person->family_name) }}"
                                    id="family_name">
                            </div>
    
                            <div class="form-group">
                                <label for="country">الدولة</label>
                                <input type="text" class="form-control" name="country" required placeholder="الدولة" value="{{ old('country', $person->country) }}"
                                    id="country">
                            </div>
    
    
                            <div class="form-group">
                                <label for="state">المدينة</label>
                                <input type="text" class="form-control" name="state" required placeholder="المدينة" value="{{ old('state', $person->state) }}"
                                    id="state">
                            </div>
    
    
    
                            <div class="form-group">
                                <label for="date_of_birth">تاريخ الميلاد</label>
                                <input type="date" class="form-control" name="date_of_birth" required placeholder="تاريخ الميلاد" value="{{ old('date_of_birth', $person->date_of_birth) }}"
                                    id="date_of_birth">
                            </div>
    
                            <div class="form-group">
                                <label for="gender-id">الجنس</label>
                                <select class="form-control" name="gender_id" required id="gender-id">
                                                            @foreach ($genders as $gender)
                                                                <option value="{{ $gender->id }}"
                                                                    {{ old('gender_id', $person->gender_id) == $gender->id ? 'selected' : '' }}
                                                                >
                                                                    {{ $gender->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                            </div>
    
                            <div class="form-group">
                                <label for="nationality-id">الجنسية</label>
                                <select class="form-control" name="nationality_id" required id="nationality-id">
                                                            @foreach ($nationalities as $nationality)
                                                                <option value="{{ $nationality->id }}"
                                                                    {{ old('nationality_id', $person->nationality_id) == $nationality->id ? 'selected' : '' }}
                                                                >
                                                                    {{ $nationality->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                            </div>
    
                            <div class="form-group">
                                <label for="avatar" class="col-md-9 col-form-label text-md-right">الصورة الشخصية</label>
                                <input type="file" id="avatar" name="avatar">
                            </div>
                        </div>
    
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">حفظ التعديلات</button>
    
                            <a href="{{ url('/') }}" class="btn btn-default">
                                                        إلغاء الامر
                                                    </a>
                        </div>
                    </form>
    
                </div>
    
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل الملف الشخصي</h3>
            </div>

            <form method="post" action="{{ route('user.profile_update') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">اسم المستخدم </label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="اسم المستخدم" value="{{ old('name', $user->name) }}">
                    </div>

                    <div class="form-group">
                        <label for="email">البريد الإلكتروني</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="البريد الإلكتروني" value="{{ old('email', $user->email) }}">
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">تعديل  الملف الشخصي</button>
                </div>
            </form>
        </div>
    </div>

    {{-- كلمة المرور update --}}
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تغيير كلمة المرور</h3>
            </div>

            <form method="post" action="{{ route('user.password_update') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="current-password">كلمة المرور الحالية</label>
                        <input type="password" name="current_password" class="form-control" id="current-password" placeholder="كلمة المرور الحالية"
                            pattern=".{6,}" title="6 characters minimum">
                    </div>

                    <div class="form-group">
                        <label for="password">كلمة المرور الجديدة</label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="كلمة المرور الجديدة" pattern=".{6,}"
                            title="6 characters minimum">
                    </div>

                    <div class="form-group">
                        <label for="confirm-password">تأكيد كلمة المرور</label>
                        <input type="password" name="password_confirmation" class="form-control" id="confirm-password" placeholder="تأكيد كلمة المرور"
                            pattern=".{6,}" title="6 characters minimum">
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">تغيير كلمة المرور</button>
                </div>
            </form>
        </div>
    </div>
  </div>
@endsection