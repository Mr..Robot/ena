@extends('layouts.app', ['page' => 'doctors'])

@section('styles')
<link href="{{ asset('/css/admin/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/admin/AdminLTE.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/pace/pace.min.css') }}">
<link href="{{ asset('/css/star-rating.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/themes/krajee-fa/theme.css') }}" rel="stylesheet" type="text/css" media="all">
<style>
  .clear-rating {
    display: none !important;
  }
</style>
@endsection
  
@section('title', 'الأطباء') 

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card-body">
        <div class="box box-default">
          <div class="box-header with-border">
         
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <form id="docsJson" action="{{route('doc.all',['sid'=>0])}}" method="get">
                <div class="form-group {{ $errors->has('specialty') ? ' has-error' : '' }}" id="specialties">
                  <label>اختر بالتخصص</label>
                  <select name="specialty" class="specSelect form-control select2" required id="specialty">
                    <option value="0" >جميع التخصصات</option>
                    @foreach (App\Specialty::onlyActive()->all() as $specialty)
                    <option value="{{ $specialty->id }}"  {{ $id == $specialty->id ? 'selected': '' }}>{{ $specialty->namear }} | {{ $specialty->nameen }}</option>
                    @endforeach
                    </select>
                </div>
                </form>
              </div>
            </div>
            {{-- </form> --}}
          </div>

        </div>

        <!-- /.box -->
        <div class="box" id="json_result">
          <div class="box-header with-border">
            <h3 class="box-title">قائمة الاخصائيين </h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12 text-center">

@foreach ($doctors as $chunk)
<div class="row">

  @foreach ($chunk as $doctor)

  <div class="col-md-4">
    <div class="box box-primary">
      <div class="box-body">
        <div class="col-sm-4">
          <img class="profile-user-img img-responsive" src="/avatars/{{ $doctor->user->avatar }}" alt="User profile picture">
          <input id="rate" name="rate" data-size="xs" class="rating kv-fa rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $doctor->rated }}">
        </div>
        <div class="col-sm-8">
          <h3 class="profile-username text-right">{{ $doctor->user->person->fullname() }}</h3>
          <h5 class="text-right">{{ $doctor->specialty->namear }}</h5>
        
        <br>
        <a href="{{ url('doctors/'.$doctor->user->id) }}" class="btn btn-info pull-left"><b>الملف الشخصي</b></a>
        <a href="{{ url('chats/'.$doctor->user->id) }}" class="btn btn-primary pull-right"><b>Message</b></a>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  @endforeach
</div>
@endforeach

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
@section('scripts')

<script src="{{ asset('/js/star-rating.js') }}">
</script>
<script src="{{ asset('/themes/krajee-fa/theme.js') }}"></script>
<script type="text/javascript">
  $(document).on('ready', function () {
    $('.select2').select2()
        $('.kv-fa').rating({
            theme: 'krajee-fa',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });
        
       
        $("#rate").rating();
    });
    
    // $('.rating').on( 'change', function () {
    //     $("#docsJson" ).submit();
    //  });
  
  $(".specSelect" ).change(function() {
     var slug = $(this).val(); 
     var base = '{!! route('doc.all',["sid" => '+this.val()+']) !!}'; 
     var url = base+'/sid='+slug; 
    window.location.href=url;      
 $("#docsJson" ).submit();   
  }); 
    </script>

@endsection