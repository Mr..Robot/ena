@extends('layouts.app', ['page' => 'profile'])

@section('styles')
<link href="{{ asset('/css/admin/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
<link href="{{ asset('/css/admin/AdminLTE.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/star-rating.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/themes/krajee-fa/theme.css') }}" rel="stylesheet" type="text/css" media="all">
<style >
.clear-rating{
    display: none !important;
}
</style>
@endsection
@section('title', 'ملف الشخصي')
@section('content')
    <div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
        <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="/avatars/{{ $user->avatar }}" alt="User profile picture">
            <h3 class="profile-username text-center">{{ $user->person->fullname() }}</h3>
            <p class="text-muted text-center">{{ $doctor->specialty->namear }}</p>
            
            
            @if (Auth::id() == $user->id)
            {{-- <form id="dahshboard" action="" method="get"> --}}
                    {{-- <input type="hidden" name="dashboard" value="true"> --}}
                    <a href="/dashboard"  class="btn btn-success btn-block">لوحة التحكم</a>
            </form>
            <br>
            
            <a href="{{ url('chats/') }}" class="btn btn-primary btn-block"><b>قائمة الرسائل</b></a>
            {{-- @if (Auth::user()->doctor->dgree == 1)
            <a href="{{ url('promotion/create',['sid'=>$doctor->id]) }}" class="btn btn-warning btn-block"><b>طلب ترقية</b></a>
            @endif
               --}}
            @else
            <a href="{{ url('chats/'.$doctor->user->id) }}" class="btn btn-primary btn-block"><b>أرسل رسالة</b></a>
            @endif
             <hr>
             @auth
             <form id="docsJson" action="{{route('doc.rate',['sid'=>$doctor->id])}}" method="post">
                            @csrf
                            <div class="rating text-center">
                        
                                <input id="rate" name="rate" class="rating kv-fa rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $doctor->userAverageRating }}"
                                    data-size="md">
                                <input type="hidden" name="id" required="" value="{{ $doctor->id }}">
                            </div>
                        </form>
             @else
                 <div class="rating text-center">
                
                    <input id="rate1"  class="rating kv-fa rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $doctor->userAverageRating }}"
                        data-size="md">
                   
                </div>
             @endauth
        </div>
        </div>
        <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">عني</h3>
        </div>
        <div class="box-body">
            <strong><i class="fa fa-book margin-r-5"></i> التعليم</strong>
            
            @foreach (App\certificate::where('doctor_id', $doctor->id)->get() as $certificate)
            <p class="text-muted">
           
            {{$certificate->name  }}
            </p>

            @endforeach
            <hr>

            <strong><i class="fa fa-map-marker margin-r-5"></i> العنوان</strong>

            <label class="text-muted">{{ $doctor->user->person->country }}</label>,<label class="text-muted">{{ $doctor->user->person->state }}</label>

        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
    <div class="col-md-9">
        <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
           @if (Auth::id() == $user->id)
            <li ><a href="#activity" data-toggle="tab"> شهادات</a></li>
            @endif
            <li class="active"><a href="#diseases" data-toggle="tab"> الامراض</a></li>
            <li><a href="#timeline" data-toggle="tab">جدول المواعيد</a></li>
            @if (Auth::id() == $user->id) 
            <li><a href="#settings" data-toggle="tab">تعديل البيانات</a></li>
             @endif 
        </ul>
        <div class="tab-content">
            @if (Auth::id() == $user->id)
    <div class=" tab-pane" id="activity">
                <table class="table table-bordered">
                    
       <tr>
            <th>#</th>
            
            <th>إسم الشهادة</th>
            <th>الملف</th>
            <th>عرض</th>
            
        </tr>
        
        @forelse ($certificates as $certificate)
        @if(in_array($certificate->id, $doctor->certificates))
        <tr>
            <td>{{ $certificate->id }}</td>
            
            <td>{{ $certificate->name }}</td>
            <td>
                <img src="{{ $certificate->getFirstMediaUrl('link') }}" width="50" alt="صورة الملف">
            </td>
            <td><a href="{{ $certificate->getFirstMediaUrl('link') }}">عرض</a></td>
            
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan="5">لا توجد أي سجلات</td>
        </tr>

        @endforelse
        </table>
    </div>
            @endif
            <div class="active tab-pane" id="diseases">
            @if (Auth::id() == $user->id)
                <form action="{{route('doc.update', ['person' => $person->id]) }}" method="post">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="diseases">الأمراض</label>
                        <select class="form-control select2" name="diseases[]" required multiple id="diseases">
                            @foreach ($diseases->where('specialty_id', $doctor->specialty_id); as $disease)
                                <option value="{{ $disease->id }}"
                                    {{ in_array($disease->id, old('diseases', $doctor->diseases)) ? 'selected' : '' }}
                                >
                                    {{ $disease->name.' || ' .$disease->commonname }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </form>
                @else
                <div class="form-group">
                    <label for="diseases">الأمراض</label>
                    <select disabled class="form-control select2" name="diseases[]" required multiple id="diseases">
                        @foreach ($diseases as $disease)
                            <option value="{{ $disease->id }}" {{ in_array($disease->id, old('diseases', $doctor->diseases)) ? 'selected' : '' }}>
                                {{ $disease->name.' || ' .$disease->commonname }}
                            </option>
                        @endforeach
                    </select>
                </div>
                @endif
            </div>
            <div class="tab-pane" id="timeline">
              <div class="box">
                {{-- <div class="box-header">
                    <h3 class="box-title">جدول المواعيد</h3>
                </div> --}}
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    {{-- <form role="form" method="POST" action="{{ route('appointments.store') }}">
                        @csrf
                    
                        <div class="box-body">
                            <div class="form-group">
                                <label for="from">من</label>
                    
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" name="from" required placeholder="من" value="{{ old('from') }}" id="from">
                    
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            </div>
                    
                            <div class="form-group">
                                <label for="to">إلى</label>
                                <input type="text" class="form-control timepicker" name="to" required placeholder="إلى" value="{{ old('to') }}" id="to">
                            </div>
                    
                            <div class="form-group">
                                <label for="day-id">يوم</label>
                                <select class="form-control" name="day_id" required id="day-id">
                                                @foreach ($days as $day)
                                                    <option value="{{ $day->id }}"
                                                        {{ old('day_id') == $day->id ? 'selected' : '' }}
                                                    >
                                                        {{ $day->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                            </div> --}}
                    
                            {{--
                            <div class="form-group">
                                <label for="doctor-id">الطبيب</label>
                                <select class="form-control" name="doctor_id" required id="doctor-id">
                                                @foreach ($doctors as $doctor)
                                                    <option value="{{ $doctor->id }}"
                                                        {{ old('doctor_id') == $doctor->id ? 'selected' : '' }}
                                                    >
                                                        {{ $doctor->ref_no }}
                                                    </option>
                                                @endforeach
                                            </select>
                            </div> --}}
{{--                     
                            <div class="form-group">
                                <label for="clink-id">المصحة</label>
                                <select class="form-control" name="clink_id" required id="clink-id">
                                                @foreach ($clinks as $clink)
                                                    <option value="{{ $clink->id }}"
                                                        {{ old('clink_id') == $clink->id ? 'selected' : '' }}
                                                    >
                                                        {{ $clink->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                            </div>
                        </div>
                    
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">حفظ </button>
                    
                            <a href="#" class="btn btn-default">
                                            إلغاء الامر
                                        </a>
                        </div>
                    </form> --}}
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>اليوم</th>
                            <th>المصحة</th>
                            <th>من</th>
                            <th>إلى</th>
                        </tr>
                        @forelse ($doctor->appointments as $i => $appointment)
                        <tr>
                            <td>{{ $i +1}}</td>
                            <td>{{ $appointment->day->name }}</td>
                            <td>{{ $appointment->clink->name }}</td>
                            <td>{{ $appointment->from }}</td>
                            <td>{{ $appointment->to }}</td>
                            {{-- <td>
                                <a href="{{ route('admin.appointments.edit', ['appointment' => $appointment->id]) }}">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                        </a>
                        
                                <form action="{{ route('admin.appointments.destroy', ['appointment' => $appointment->id]) }}" method="POST" class="inline pointer">
                                    @csrf @method('DELETE')
                        
                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                                                <i class="fa fa-trash-o"></i>
                                                            </a>
                                </form>
                            </td> --}}
                        </tr>
                        @empty
                        <tr>
                            <td colspan="5">لا توجد أي سجلات</td>
                        </tr>
                        @endforelse
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </div>
                @if (Auth::id() == $user->id)
                <div class="box-footer clearfix">
                  <small>أذا اردت تعديل جدول الاعمال يرجى الدخول الى لوحة التحكم</small>
                </div>
                @endif
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
            </div>
            {{-- @if (Auth::id() == $user->id) --}}
  
            <div class="tab-pane" id="settings">
                <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('doctor.update', ['person' => $person->id]) }}">
                    @csrf
                    @method('PUT')
    
                    <div class="box-body">
                        <div class="form-group">
                            <label for="first_name">الإسم الأول</label>
                            <input type="text"
                                class="form-control"
                                name="first_name"
                                required
                                placeholder="الإسم الأول"
                                value="{{ old('first_name', $person->first_name) }}"
                                id="first_name"
                            >
                        </div>
    
                        <div class="form-group">
                            <label for="second_name">إسم الأب</label>
                            <input type="text"
                                class="form-control"
                                name="second_name"
                                required
                                placeholder="إسم الأب"
                                value="{{ old('second_name', $person->second_name) }}"
                                id="second_name"
                            >
                        </div>
    
                        <div class="form-group">
                            <label for="last_name">إسم الجد</label>
                            <input type="text"
                                class="form-control"
                                name="last_name"
                                required
                                placeholder="إسم الجد"
                                value="{{ old('last_name', $person->last_name) }}"
                                id="last_name"
                            >
                        </div>
    
                        <div class="form-group">
                            <label for="family_name">اللقب</label>
                            <input type="text"
                                class="form-control"
                                name="family_name"
                                required
                                placeholder="اللقب"
                                value="{{ old('family_name', $person->family_name) }}"
                                id="family_name"
                            >
                        </div>

                        <div class="form-group">
                            <label for="country">الدولة</label>
                            <input type="text"
                                class="form-control"
                                name="country"
                                required
                                placeholder="الدولة"
                                value="{{ old('country', $person->country) }}"
                                id="country"
                            >
                        </div>
    
                        
                        <div class="form-group">
                            <label for="state">المدينة</label>
                            <input type="text"
                                class="form-control"
                                name="state"
                                required
                                placeholder="المدينة"
                                value="{{ old('state', $person->state) }}"
                                id="state"
                            >
                        </div>
 
                        
    
                        <div class="form-group">
                            <label for="date_of_birth">تاريخ الميلاد</label>
                            <input type="date"
                                class="form-control"
                                name="date_of_birth"
                                required
                                placeholder="تاريخ الميلاد"
                                value="{{ old('date_of_birth', $person->date_of_birth) }}"
                                id="date_of_birth"
                            >
                        </div>
    
                        <div class="form-group">
                            <label for="gender-id">الجنس</label>
                            <select class="form-control"
                                name="gender_id"
                                required
                                id="gender-id"
                            >
                                @foreach ($genders as $gender)
                                    <option value="{{ $gender->id }}"
                                        {{ old('gender_id', $person->gender_id) == $gender->id ? 'selected' : '' }}
                                    >
                                        {{ $gender->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
    
                        <div class="form-group">
                            <label for="nationality-id">الجنسية</label>
                            <select class="form-control"
                                name="nationality_id"
                                required
                                id="nationality-id"
                            >
                                @foreach ($nationalities as $nationality)
                                    <option value="{{ $nationality->id }}"
                                        {{ old('nationality_id', $person->nationality_id) == $nationality->id ? 'selected' : '' }}
                                    >
                                        {{ $nationality->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="avatar" class="col-md-9 col-form-label text-md-right">الصورة الشخصية</label>
                            <input type="file" id="avatar" name="avatar">
                        </div>
                    </div>
    
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">حفظ التعديلات</button>
    
                        <a href="{{ url('/') }}" class="btn btn-default">
                            إلغاء الامر
                        </a>
                    </div>
                </form>
              
            </div>
         
            {{-- @endif  --}}
        </div>
        </div>
    </div>
    </div>
        </section>

@endsection
@section('scripts')
<script src="{{ asset('/js/admin/select2.full.min.js') }}"></script>
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/star-rating.js') }}"></script>
<script src="{{ asset('/themes/krajee-fa/theme.js') }}"></script>
<script>
    $(document).on('ready', function () {
        $('.kv-fa').rating({
            theme: 'krajee-fa',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });
        
       
        $("#rate").rating();
    });
    
    $('.rating').on( 'change', function () {
        $("#docsJson" ).submit();
     });
    
    $(function () {
        $('.select2').select2()
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
    });
</script>
@endsection