@extends('layouts.app', ['page' => 'doctors'])
@section('styles')
<link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
<link href="{{ asset('/css/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
@endsection
 
@section('title', 'تسجيل حساب طبيب') 
@section('content')
<div class="register-box" dir="rtl">
  <div class="register-box-body">
    <p class="login-box-msg">تسجيل حساب طبيب</p>
    <form autocomplete="off" action="{{ route('doc.store') }}" enctype="multipart/form-data" method="post">
      @csrf

      <div class="form-group focused {{ $errors->has('first_name') ? ' has-error' : '' }} ">
        <input type="text" class="form-control" id="first_name" name="first_name" value="{{ old('first_name') }}" placeholder="الاسم الأول"
          required> @if ($errors->has('first_name'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('first_name') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('second_name') ? ' has-error' : '' }} ">
        <input type="text" class="form-control" id="second_name" name="second_name" value="{{ old('second_name') }}" placeholder="اسم الاب"
          required> @if ($errors->has('second_name'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('second_name') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('last_name') ? ' has-error' : '' }} ">
        <input type="text" class="form-control" id="last_name" name="last_name" value="{{ old('last_name') }}" placeholder="اسم الجد"
          required> @if ($errors->has('last_name'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('last_name') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('family_name') ? ' has-error' : '' }} ">
        <input type="text" class="form-control" id="family_name" name="family_name" value="{{ old('family_name') }}" placeholder="اللقب"
          required> @if ($errors->has('family_name'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('family_name') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('date_of_birth') ? ' has-error' : '' }} ">
        <label class="control-label" for="date_of_birth">تاريخ الميلاد</label>
        <input type="text" class="form-control" id="date_of_birth" name="date_of_birth" value="{{ old('date_of_birth') }}" required>        @if ($errors->has('date_of_birth'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('date_of_birth') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('nationality_id') ? ' has-error' : '' }} ">
        <label class="control-label" for="nationality_id"></i>الجنسية</label>
        <select class="form-control select2" name="nationality_id" style="width: 100%;">
@foreach ($nationalities as $nationality)
<option value="{{ $nationality->id }}" {{ old('nationality_id') == $nationality->id ? 'selected': '' }}>{{ $nationality->name }}</option>
@endforeach
</select> @if ($errors->has('nationality_id'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('nationality_id') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('email') ? ' has-error' : '' }} ">
        <label class="control-label" for="email"></i>البريد الإلكتروني</label>
        <input type="email" class="form-control" id="email"  name="email" value="{{ old('email') }}" placeholder="البريد الإلكتروني"
          required> @if ($errors->has('email'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('email') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('password') ? ' has-error' : '' }} ">
        <label class="control-label" for="password"></i>كلمة المرور</label>
        <input type="password" class="form-control" id="password"  name="password" value="{{ old('password') }}" placeholder="كلمة المرور"
          required> @if ($errors->has('password'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('password') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('password_confirmation') ? ' has-error' : '' }} ">
        <label class="control-label" for="password_confirmation"></i>تأكيد كلمة المرور</label>
        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}"
          placeholder="تأكيد كلمة المرور" required> @if ($errors->has('password_confirmation'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('password_confirmation') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('gender') ? ' has-error' : '' }} ">
        <label class="control-label" for="gender"></i>الجنس</label>
        <select class="form-control select2" name="gender" style="width: 100%;">
<option value="-1">أختر الجنس</option>
<option value="1"  {{ old('gender') == 1 ? 'selected': '' }}>ذكر</option>
<option value="2"  {{ old('gender') == 2 ? 'selected': '' }}>أنثى</option>
</select> @if ($errors->has('gender'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('gender') }}</span></strong>
        </span>
        @endif
      </div>
      <div class="form-group focused {{ $errors->has('specialty_id') ? ' has-error' : '' }} ">
        <label class="control-label" for="specialty_id"></i>التخصص</label>
        <select class="form-control select2" name="specialty_id" style="width: 100%;">
    @foreach ($specialties as $specialty)
    <option value="{{ $specialty->id }}" {{ old('specialty_id') == $specialty->id ? 'selected': '' }}>{{ $specialty->namear }}</option>
    @endforeach
    </select> @if ($errors->has('specialty_id'))
        <span class="is-invalid" role="alert">
    <strong> <span class="help-block">{{ $errors->first('specialty_id') }}</span></strong>
        </span>
        @endif
      </div>
      <div class="form-group focused {{ $errors->has('ref_no') ? ' has-error' : '' }} ">
        <label class="control-label" for="ref_no"></i>الرقم النقابي</label>
        <input type="text" class="form-control" id="ref_no"  name="ref_no" value="{{ old('ref_no') }}" placeholder="الرقم النقابي"
          required> @if ($errors->has('ref_no'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('ref_no') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('dgree') ? ' has-error' : '' }} ">
        <label class="control-label" for="dgree"></i>الدرجة</label>
        <input type="text" class="form-control" id="dgree" name="dgree" value="{{ old('dgree') }}" placeholder="الدرجة"
          required> @if ($errors->has('dgree'))
        <span class="is-invalid" role="alert">
<strong> <span class="help-block">{{ $errors->first('dgree') }}</span></strong>
        </span>
        @endif
      </div>

      <div class="form-group focused {{ $errors->has('link') ? ' has-error' : '' }} ">
        <label class="control-label" for="link"></i>صورة المؤهل العلمي</label>
        <input required type="file" class="form-control" id="link"  name="link" value="{{ old('link') }}"
          placeholder="صورة المؤهل العلمي"> @if ($errors->has('link'))
        <span class="is-invalid" role="alert">
    <strong> <span class="help-block">{{ $errors->first('link') }}</span></strong>
        </span>
        @endif
      </div>

      

      <div class="form-group focused">
        <button type="submit" class="btn btn-block btn-primary btn-flat"> تسجيل</button>
      </div>


      <div class="form-group focused">
        <a href="{{ route('login') }}" class="btn btn-block btn-primary btn-flat" role="button"><i class="ni ni-ui-04 d-lg-none"></i>
      <span class="customer-font nav-link-inner--text"> <i class="fa fa-user"></i> تسجيل الدخول</span>
      </a>
      </div>






    </form>
  </div>
</div>
@endsection
 
@section('scripts')
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('/js/js/bootstrap-datepicker.min.js') }}"></script>

<script>
  $(function () {
          $('#date_of_birth').datepicker({
            autoclose: true
          })
        });

</script>
@endsection