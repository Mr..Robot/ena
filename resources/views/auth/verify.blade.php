@extends('layouts.app', ['page' => 'verify'])
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

                <div class="box box-danger">
                        <div class="box-header with-border">
                          <h3 class="box-title">أكد بريدك الالكتروني</h3>
{{--              
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                          </div>  --}}
                          <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                                @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    تم ارسال رسالة علي بريدك الالكتروني تحتوي علي رابط التأكيد
                                </div>
                            @endif
        
                            قبل كل شي تفقد بريدك الالكتروني سوف تجد رسالة التأكيد
                            إذا لم تستقبلها, <a href="{{ route('verification.resend') }}">إضغط هنا لاعادة الارسال</a>.
                       
                        </div>
                        <!-- /.box-body -->
                      </div>

  
        </div>
    </div>
</div>
@endsection
