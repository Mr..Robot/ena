@extends('layouts.app', ['page' => 'email'])
@section('content')
<div class="login-box" dir="rtl">
    <div class="login-box-body">
        <p class="login-box-msg">إعادة ظبط كلمة المرور</p>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 col-form-label text-md-right ">البريد الإلكتروني</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="is-invalid" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('email') }}</span></strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-12 offset-md-3">
                    <button type="submit" class="btn btn-primary">
                        أرسال رسالة اعادة الظبط
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection