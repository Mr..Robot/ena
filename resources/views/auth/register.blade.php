@extends('layouts.app',['page' => 'register'])
@section('styles')
    <link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
@endsection
@section('title', 'تسجيل حساب مستخدم') 
@section('content')
<div class="register-box">
    <div class="register-box-body">
            <p class="login-box-msg">تسجيل كمستخدم</p>
            <form action="{{ route('user.register') }}" autocomplete="off" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">الاسم</label>
                            <input autocomplete="false" id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" placeholder="الاسم" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong> <span class="help-block">{{ $errors->first('first_name') }}</span></strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="second_name" class="col-md-12 col-form-label text-md-right">اسم الاب</label>
                            <input autocomplete="false" id="second_name" type="text" class="form-control{{ $errors->has('second_name') ? ' is-invalid' : '' }}" name="second_name" value="{{ old('second_name') }}" placeholder="اسم الاب" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span> 
                            @if ($errors->has('second_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong> <span class="help-block">{{ $errors->first('second_name') }}</span></strong>
                                </span> 
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="last_name" class="col-md-12 col-form-label text-md-right">اسم الجد</label>
                            <input autocomplete="false" id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" placeholder="اسم الجد" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong> <span class="help-block">{{ $errors->first('last_name') }}</span></strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="family_name" class="col-md-4 col-form-label text-md-right">اللقب</label>
                            <input autocomplete="false" id="family_name" type="text" class="form-control{{ $errors->has('family_name') ? ' is-invalid' : '' }}" name="family_name" value="{{ old('family_name') }}" placeholder="اللقب" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span> 
                            @if ($errors->has('family_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong> <span class="help-block">{{ $errors->first('family_name') }}</span></strong>
                                </span> 
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group has-feedback">
                    <label for="email" class="col-md-9 col-form-label text-md-right">البريد الإلكتروني</label>
                    <input autocomplete="false" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="البريد الإلكتروني" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span> 
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('email') }}</span></strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback">
                    <label for="password" class="col-md-9 col-form-label text-md-right">كلمة المرور</label>
                    <input autocomplete="false" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="كلمة المرور" required>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('password') }}</span></strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback">
                    <label for="password-confirm" class="col-md-9 col-form-label text-md-right">تأكيد كلمة المرور</label>
                    <input autocomplete="false" id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="تأكيد كلمة المرور" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="form-group focused {{ $errors->has('gender') ? ' has-error' : '' }} ">
                    <label class="control-label" for="gender">الجنس</label>
                    <select class="form-control select2" name="gender" style="width: 100%;">
                        <option value="-1">أختر الجنس</option>
                        <option value="1"  {{ old('gender') == 1 ? 'selected': '' }}>ذكر</option>
                        <option value="2"  {{ old('gender') == 2 ? 'selected': '' }}>أنثى</option>
                    </select> 
                    @if ($errors->has('gender'))
                        <span class="is-invalid" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('gender') }}</span></strong>
                    </span>
                    @endif
                </div>
                <div class="form-group focused {{ $errors->has('nationality_id') ? ' has-error' : '' }} ">
                    <label class="control-label" for="nationality_id">الجنسية</label>
                    <select class="form-control select2" name="nationality_id" style="width: 100%;">
                        <option value="-1">أختر الجنسية</option>
                        @foreach ($nationalities as $nationality)
                            <option value="{{ $nationality->id }}" {{ old('nationality_id') == $nationality->id ? 'selected': '' }}>{{ $nationality->name }}</option>
                        @endforeach
                    </select> 
                    @if ($errors->has('nationality_id'))
                        <span class="is-invalid" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('nationality_id') }}</span></strong>
                        </span>
                    @endif
                </div>
                {{-- <div class="row">
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="country" class="col-md-4 col-form-label text-md-right">الدولة</label>
                            <input autocomplete="false" id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country"
                                value="{{ old('country') }}" placeholder="الدولة" required autofocus>
                           @if ($errors->has('country'))
                            <span class="invalid-feedback" role="alert">
                                <strong> <span class="help-block">{{ $errors->first('country') }}</span></strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-feedback">
                            <label for="state" class="col-md-4 col-form-label text-md-right">المنطقة</label>
                            <input autocomplete="false" id="state" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}"
                                name="state" value="{{ old('state') }}" placeholder="المنطقة" required autofocus>
                            @if ($errors->has('state'))
                            <span class="invalid-feedback" role="alert">
                                                    <strong> <span class="help-block">{{ $errors->first('state') }}</span></strong>
                            </span>
                            @endif
                        </div>
                    </div>
                </div> --}}
                <div class="form-group focused">
                        <button type="submit" class="btn btn-block btn-primary btn-flat"> تسجيل</button>
                    </div>
                
            </form>
            <br>
            <hr>
            <div class="form-group focused">
           
            <a href="/login" class="btn btn-primary btn-block btn-flat" class="text-center">هل لديك حساب ؟؟</a>
            </div>
            {{-- <div class="col-md-6">
            <a href="/doctors/register" class="text-center">هل انت طبيب؟ </a>
            </div> --}}
       

    </div>
</div>
@endsection
 
@section('scripts')
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
{{--
<script>
    $(function () {
                            $('input').iCheck({
                            checkboxClass: 'icheckbox_square-blue',
                            radioClass: 'iradio_square-blue',
                            increaseArea: '20%' /* optional */
                            });
                            });
</script> --}}
@endsection