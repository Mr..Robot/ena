@extends('layouts.app',['page' => 'login'])
@section('styles')
<link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
@endsection
@section('title', 'تسجيل الدخول')
@section('content')
    <div class="login-box" dir="rtl">
        <div class="login-box-body">
            <p class="login-box-msg">تسجيل الدخول</p>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group focused {{ $errors->has('email') ? ' has-error' : '' }} ">
                    <label class="control-label" for="email"></i>البريد الإلكتروني</label>
                    <input type="email" class="form-control" id="email" type="text" name="email" value="{{ old('email') }}" placeholder="البريد الإلكتروني" required>
                    @if ($errors->has('email'))
                        <span class="is-invalid" role="alert">
                        <strong> <span class="help-block">{{ $errors->first('email') }}</span></strong>
                        </span>
                    @endif
                </div>
                <div class="form-group focused {{ $errors->has('password') ? ' has-error' : '' }} ">
                    <label class="control-label" for="password"></i>كلمة المرور</label>
                    <input type="password" class="form-control" id="password" type="text" name="password" value="{{ old('password') }}" placeholder="كلمة المرور" required>
                    @if ($errors->has('password'))
                        <span class="is-invalid" role="alert">
                        <strong> <span class="help-block">{{ $errors->first('password') }}</span></strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                تذكرني
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">تسجيل الدخول</button>
                    </div>
               
            </form>
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    نسيت كلمة المرور؟
                </a><br>
            @endif
            <div class="form-group">
                <a href="#" class="text-center">ليس لديك حساب علي Doctor online? </a>
            </div>
            <div class="form-group">
                <a href="/doctors/register" class="text-center">إنضم الان اذا كنت طبيب</a>
            </div>
            <div class="form-group">
                <a href="/register" class="text-center">إنضم الان اذا كنت مستخدم</a>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });
        });
    </script>
@endsection

