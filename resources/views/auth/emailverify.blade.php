@extends('layouts.app', ['page' => 'email'])
@section('content')
@if (session('resent'))
        
        
        <div class="alert alert-success" role="alert">
            <p>
                بعد تفيعل البريد الخاص بك يرجئ الانتظار حتى يقوم مشرف النظام بمراجعة طلبك وقبوله
            </p>
        </div>
        
        <!-- /.box-body -->
        @endif
<div class="login-box" dir="rtl">
    <div class="login-box-body">
        <p class="login-box-msg">تأكيد البريد الالكتروني</p>
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>

        </div>
        @endif

        <form method="GET" action="{{ route('verify.send') }}">
            @csrf
            <div class="form-group row">
                <label class="col-md-12 col-form-label text-md-right ">الرجاء ادخال البريد الالكتروني لارسال رسالةالتأكيد.</label>
            </div>
            <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="col-md-4 col-form-label text-md-right ">البريد الإلكتروني</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="is-invalid" role="alert">
                            <strong> <span class="help-block">{{ $errors->first('email') }}</span></strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-12 offset-md-3">
                    <button type="submit" class="btn btn-primary">
                        أرسال رسالة التأكيد
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection