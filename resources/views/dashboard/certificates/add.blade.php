@extends('layouts.dashboard', ['page' => 'certificate'])

@section('title', 'إضافة  شهادة')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  شهادة</h3>
            </div>

            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('dashboard.certificates.store') }}">
                @csrf
            <input type="hidden" name="doctor_id" value="{{ Auth::user()->doctor->id}}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="name">إسم الشهادة</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="إسم الشهادة"
                            value="{{ old('name') }}"
                            id="name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="link">الملف</label>
                        <input type="file"
                            class="form-control"
                            name="link"
                            required
                            value="{{ old('link') }}"
                            id="link"
                        >
                    </div>

                    {{-- <div class="form-group">
                        <label for="doctor-id">إسم الطبيب</label>
                        <select class="form-control"
                            name="doctor_id"
                            required
                            id="doctor-id"
                        >
                            @foreach ($doctors as $doctor)
                                <option value="{{ $doctor->id }}"
                                    {{ old('doctor_id') == $doctor->id ? 'selected' : '' }}
                                >
                                    {{ $doctor->ref_no }}
                                </option>
                            @endforeach
                        </select>
                    </div> --}}
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('dashboard.certificates.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
