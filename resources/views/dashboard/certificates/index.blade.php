@extends('layouts.dashboard', ['page' => 'certificate'])

@section('title', 'الشهادات')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الشهادات</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('dashboard.certificates.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>إسم الطبيب</th>
                        <th>إسم الشهادة</th>
                        <th>الملف</th>
                        <th>عرض</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($certificates as $certificate)
                        <tr>
                            <td>{{ $certificate->id }}</td>
                            <td>{{ $certificate->doctor->user->person->fullName() }}</td>
                            <td>{{ $certificate->name }}</td>
                            <td>
                                <img src="{{ $certificate->getFirstMediaUrl('link') }}"
                                    width="50"
                                    alt="صورة الملف"
                                >
                            </td>
                            <td><a href="{{ $certificate->getFirstMediaUrl('link') }}" >عرض</a></td>
                            <td>
                                <a href="{{ route('dashboard.certificates.edit', ['certificate' => $certificate->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('dashboard.certificates.destroy', ['certificate' => $certificate->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="5">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            {{-- <div class="box-footer clearfix">
                {{ $certificates->links('vendor.pagination.default') }}
            </div> --}}
        </div>
    </div>
</div>
@endsection
