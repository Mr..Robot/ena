@extends('layouts.dashboard', ['page' => 'appointment'])

@section('title', 'إضافة  موعد')

@section('styles')

<link href="{{ asset('/css/admin/bootstrap-timepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  موعد</h3>
            </div>

            <form role="form" method="POST" action="{{ route('dashboard.appointments.store') }}">
                @csrf
<input type="hidden" name="doctor_id" value="{{ Auth::user()->doctor->id}}">
                <div class="box-body">
                    <div class="form-group">
                        <label for="from">من</label>
      
                        <div class="input-group">
                            <input type="text" class="form-control timepicker" name="from" required placeholder="من" value="{{ old('from') }}" id="from" >
      
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>                 
                    </div>

                    <div class="form-group">
                        <label for="to">إلى</label>
                        <input type="text"
                         class="form-control timepicker"
                            name="to"
                            required
                            placeholder="إلى"
                            value="{{ old('to') }}"
                            id="to"
                        >
                    </div>

                    <div class="form-group">
                        <label for="day-id">يوم</label>
                        <select class="form-control"
                            name="day_id"
                            required
                            id="day-id"
                        >
                            @foreach ($days as $day)
                                <option value="{{ $day->id }}"
                                    {{ old('day_id') == $day->id ? 'selected' : '' }}
                                >
                                    {{ $day->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    {{-- <div class="form-group">
                        <label for="doctor-id">الطبيب</label>
                        <select class="form-control"
                            name="doctor_id"
                            required
                            id="doctor-id"
                        >
                            @foreach ($doctors as $doctor)
                                <option value="{{ $doctor->id }}"
                                    {{ old('doctor_id') == $doctor->id ? 'selected' : '' }}
                                >
                                    {{ $doctor->ref_no }}
                                </option>
                            @endforeach
                        </select>
                    </div> --}}

                    <div class="form-group">
                        <label for="clink-id">المصحة</label>
                        <select class="form-control"
                            name="clink_id"
                            required
                            id="clink-id"
                        >
                            @foreach ($clinks as $clink)
                                <option value="{{ $clink->id }}"
                                    {{ old('clink_id') == $clink->id ? 'selected' : '' }}
                                >
                                    {{ $clink->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('dashboard.appointments.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/admin/bootstrap-timepicker.min.js') }}"></script>

<script>
 $(function () {
    // Initialize Select2 Elements
    // $('.select2').select2()

    // Timepicker
    $('.timepicker').timepicker({
      showInputs: false,
      showMeridian:false
    })
  })
</script>

@endsection