@extends('layouts.dashboard', ['page' => 'appointment'])

@section('title', 'تعديل موعد')
@section('styles')

<link href="{{ asset('/css/admin/bootstrap-timepicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل موعد</h3>
            </div>

            <form role="form" method="POST" action="{{ route('dashboard.appointments.update', ['appointment' => $appointment->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="from">من</label>
                        <input type="text"
                            class="form-control timepicker"
                            name="from"
                            required
                            placeholder="من"
                            value="{{ old('from', $appointment->from) }}"
                            id="from"
                        >
                    </div>

                    <div class="form-group">
                        <label for="to">إلى</label>
                        <input type="text"
                            class="form-control timepicker"
                            name="to"
                            required
                            placeholder="إلى"
                            value="{{ old('to', $appointment->to) }}"
                            id="to"
                        >
                    </div>

                    <div class="form-group">
                        <label for="day-id">يوم</label>
                        <select class="form-control"
                            name="day_id"
                            required
                            id="day-id"
                        >
                            @foreach ($days as $day)
                                <option value="{{ $day->id }}"
                                    {{ old('day_id', $appointment->day_id) == $day->id ? 'selected' : '' }}
                                >
                                    {{ $day->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    {{-- <div class="form-group">
                        <label for="doctor-id">طبيب</label>
                        <select class="form-control"
                            name="doctor_id"
                            required
                            id="doctor-id"
                        >
                            @foreach ($doctors as $doctor)
                                <option value="{{ $doctor->id }}"
                                    {{ old('doctor_id', $appointment->doctor_id) == $doctor->id ? 'selected' : '' }}
                                >
                                    {{ $doctor->ref_no }}
                                </option>
                            @endforeach
                        </select>
                    </div> --}}

                    <div class="form-group">
                        <label for="clink-id">المصحة</label>
                        <select class="form-control"
                            name="clink_id"
                            required
                            id="clink-id"
                        >
                            @foreach ($clinks as $clink)
                                <option value="{{ $clink->id }}"
                                    {{ old('clink_id', $appointment->clink_id) == $clink->id ? 'selected' : '' }}
                                >
                                    {{ $clink->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('dashboard.appointments.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('/js/admin/bootstrap-timepicker.min.js') }}"></script>

<script>
 $(function () {
    // Initialize Select2 Elements
    // $('.select2').select2()

    // Timepicker
    $('.timepicker').timepicker({
      showInputs: false,
      showMeridian:false
    })
  })
</script>


@endsection