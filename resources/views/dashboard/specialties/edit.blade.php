@extends('layouts.dashboard', ['page' => 'specialty'])

@section('title', 'تعديل تخصص')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل تخصص</h3>
            </div>

            <form role="form" method="POST" action="{{ route('dashboard.specialties.update', ['specialty' => $specialty->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="namear">الإسم بالعربية</label>
                        <input type="text"
                            class="form-control"
                            name="namear"
                            required
                            placeholder="الإسم بالعربية"
                            value="{{ old('namear', $specialty->namear) }}"
                            id="namear"
                        >
                    </div>

                    <div class="form-group">
                        <label for="nameen">الإسم بالإنجليزية</label>
                        <input type="text"
                            class="form-control"
                            name="nameen"
                            required
                            placeholder="الإسم بالإنجليزية"
                            value="{{ old('nameen', $specialty->nameen) }}"
                            id="nameen"
                        >
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="hidden" name="active" value="0">
                            <input type="checkbox"
                                name="active"
                                value="1"
                                {{ old('active', $specialty->active) == 1 ? 'checked' : '' }}
                            >
                                تفعيل
                        </label>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('dashboard.specialties.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
