@extends('layouts.dashboard', ['page' => 'symptom'])

@section('title', 'إضافة  عرض')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  عرض</h3>
            </div>

            <form role="form" method="POST" action="{{ route('dashboard.symptoms.store') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">إسم العرض</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="إسم العرض"
                            value="{{ old('name') }}"
                            id="name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="commonname">الإسم الشائع</label>
                        <input type="text"
                            class="form-control"
                            name="commonname"
                            required
                            placeholder="الإسم الشائع"
                            value="{{ old('commonname') }}"
                            id="commonname"
                        >
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="hidden" name="active" value="0">
                            <input type="checkbox"
                                name="active"
                                value="1"
                                {{ old('active') == 1 ? 'checked' : '' }}
                            >
                                تفعيل
                        </label>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('dashboard.symptoms.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
