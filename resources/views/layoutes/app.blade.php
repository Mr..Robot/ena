<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{ asset('/css/admin/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/_all-skins.min.css') }}" rel="stylesheet">

    @yield('styles')

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body dir="rtl" class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../../index2.html" class="navbar-brand"><b>Admin</b>LTE</a>
        </div>
         <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Link</a></li>
          </ul>
          {{-- <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form> --}}
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the messages -->
                  <ul class="menu">
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <!-- User Image -->
                          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <!-- Message title and timestamp -->
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <!-- The message -->
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                  </ul>
                  <!-- /.menu -->
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
            </li>
            
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">Alexander Pierce</span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    Alexander Pierce - Web Developer
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">الملف الشخصي</a>
                  </div>
                  <div class="pull-right">
                    <a href="#" class="btn btn-default btn-flat">تسجيل الخروج</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1>الأطباء Directory</h1>
    </section>

    <section class="content">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">الأطباء</h3>
          </div>
          <div class="box-body">
              {{-- GGGGGGGGGGGGGGgg --}}
          </div>
        </div>
      </section>
    </div>
  </div>
</div>


<script src="{{ asset('/js/admin/jquery.js') }}"></script>
<script src="{{ asset('/js/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/js/js/fastclick.js') }}"></script>
<script src="{{ asset('/js/js/adminlte.min.js') }}"></script>

@if (session('message'))
    <script>
        showNotice("{{ session('type') }}", "{{ session('message') }}");
    </script>
@endif

{{-- You can put page wise javascript in scripts section --}}
@yield('scripts')
</body>
</html>





<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{ asset('/css/admin/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/css/_all-skins.min.css') }}" rel="stylesheet">

    @yield('styles')

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body dir="rtl" class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
                <a class="navbar-brand" >

          <a href="{{ url('/') }}" class="navbar-brand"><b>Admin</b>LTE</a>
        </div>
         <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Link</a></li>
          </ul>
          {{-- <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form> --}}
        </div>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown messages-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-success">4</span>
              </a>
              <ul class="dropdown-menu">
                <li class="header">You have 4 messages</li>
                <li>
                  <!-- inner menu: contains the messages -->
                  <ul class="menu">
                    <li><!-- start message -->
                      <a href="#">
                        <div class="pull-left">
                          <!-- User Image -->
                          <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                        </div>
                        <!-- Message title and timestamp -->
                        <h4>
                          Support Team
                          <small><i class="fa fa-clock-o"></i> 5 mins</small>
                        </h4>
                        <!-- The message -->
                        <p>Why not buy a new awesome theme?</p>
                      </a>
                    </li>
                    <!-- end message -->
                  </ul>
                  <!-- /.menu -->
                </li>
                <li class="footer"><a href="#">See All Messages</a></li>
              </ul>
            </li>
            
            @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
        @else
                    
           
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <img src="../../dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs">  {{ Auth::user()->name }} </span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                        {{ Auth::user()->name }} 
                    <small>Member since Nov. 2012</small>
                  </p>
                </li>
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">الملف الشخصي</a>
                  </div>
                  <div class="pull-right">
                     <a class="btn btn-default btn-flat" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}gg
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>


         
                  </div>
                </li>
              </ul>
            </li>
            @endguest
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="content-wrapper">
    <div class="container">
        <section class="content-header">
            <h1>الأطباء Directory</h1>
    </section>

    <section class="content">
        <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title">الأطباء</h3>
          </div>
          <div class="box-body">
              {{-- GGGGGGGGGGGGGGgg --}}
          </div>
        </div>
      </section>
    </div>
  </div>
</div>


<script src="{{ asset('/js/admin/jquery.js') }}"></script>
<script src="{{ asset('/js/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/js/js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('/js/js/fastclick.js') }}"></script>
<script src="{{ asset('/js/js/adminlte.min.js') }}"></script>

@if (session('message'))
    <script>
        showNotice("{{ session('type') }}", "{{ session('message') }}");
    </script>
@endif

{{-- You can put page wise javascript in scripts section --}}
@yield('scripts')
</body>
</html>