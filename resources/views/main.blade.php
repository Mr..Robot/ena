<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="img/favicon.png" type="image/png">
    <title>Hospice Medical</title>
    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="css/bootstrap.css"> --}}
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/linericon/style.css') }}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="css/font-awesome.min.css"> --}}
    <link href="{{ asset('/css/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/owl-carousel/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/lightbox/simpleLightbox.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/nice-select/css/nice-select.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/animate-css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('/vendors/jquery-ui/jquery-ui.css') }}" rel="stylesheet">
    <!-- main css -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet">

<style>
a.nav-link{
    font-size: 24px !important ;
}
.categories_post,.categories_post .avatar{
    width: 300px !important;
    height: 300px !important;
}
</style>



</head>

<body >


    <!--================Header Menu Area =================-->
    <header class="header_area">
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="/">
						<img src="img/logo.png" alt="">
					</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <div class="row ml-0 w-100">
                            <div class="col-lg-12 pr-0">
                                <ul class="nav navbar-nav center_nav pull-right">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#aboutUs">من نحن</a>
                                    </li>


                                    <li class="nav-item submenu dropdown">
                                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">سجل الان</a>
                                        <ul class="dropdown-menu">
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('/register')}}">مستخدم</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="{{url('/doctors/register')}}">طبيب</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="{{url('/login')}}">دخول</a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="{{url('/doctors/all')}}">الاطباء</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/">الرئيسية</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!--================Header Menu Area =================-->

    <!--================ Home Banner Area =================-->
    <section class="home_banner_area">
        <div class="banner_inner d-flex align-items-center">
            <div class="container">
                <div class="banner_content row">
                    <div class="col-lg-12">
                        <h1>نحن نهتم بصحتك كل لحظة</h1>
                        <p style="font-size: 22px; text-align: right;">ادا كنت تبحث عن افضل اخصائي في ليبيا
                            <br> 
                            
                           
                              يوفر لك مجموعة من الأطباء المتخصصين  Doctor online <br>
                            والمميزين المتوفرين على الطلب طلية اليوم </p>
                        <a class="main_btn mr-10" href="/search">ابحث عن افضل اخصائي</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Home Banner Area =================-->

@php
    $docs = App\Doctor::onlyActive()->sortByDesc('rate')->take(3);
@endphp

    {{-- {{ App\Doctor::orderBy('rate', 'desc')->take(3)->get() }} --}}
    <!--================ Procedure Category Area =================-->
    <section class="procedure_category section_gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <h1>افصل الاخصائيين</h1>
                    <p style="font-size: 22px;">
                        خبرائنا هنا من اجلك كل يوم نحن نهتم بمرضانا ونبذل قصار جهدنا لجعلهم سعداء.
                    </p>
                </div>
            </div>

            <div class="row">
                @foreach ($docs as $doc)
                    
                
                <div class="col-lg-4">
                    <div class="categories_post">
                        <img class="avatar" src="/avatars/{{$doc->user->avatar}}" alt="Procedure">
                        <div class="categories_details">
                            <div class="categories_text">
                                <a href="doctors/{{$doc->user->id}}">
                                <div class="border_line"></div>
                                
                                <h5>{{$doc->user->person->fullname}}</h5>
                                
                                <div class="border_line"></div>
                               
                                    <h5>{{$doc->specialty->namear}}</h5>
                                <div class="border_line"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                {{-- <div class="col-lg-4">
                    <div class="categories_post">
                        <img src="img/procedure/p2.jpg" alt="Procedure">
                        <div class="categories_details">
                            <div class="categories_text">
                                <div class="border_line"></div>
                                <a href="single-blog.html">
                                    <h5>Emergency Treatment</h5>
                                </a>
                                <div class="border_line"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="categories_post">
                        <img src="img/procedure/p3.jpg" alt="Procedure">
                        <div class="categories_details">
                            <div class="categories_text">
                                <div class="border_line"></div>
                                <a href="single-blog.html">
                                    <h5>Emergency Treatment</h5>
                                </a>
                                <div class="border_line"></div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!--================ End Procedure Category Area =================-->


    <!--================ Start Offered Services Area =================-->
    <section class="service_area section_gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <h1> احصائيات موقع دكتور اونلاين</h1>
                    <p style="font-size: 22px;">
                       بعض الاحصائيات من موقعنا
                    </p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                        <span class="lnr lnr-bubble"></span>
                        <a href="#">
                            <h3>{{ App\Chat::all()->count() }}</h3>
                        </a>
                        <p>
                           محادثة اجريت من قبل المستخدمين
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                        <span class="lnr lnr-heart-pulse"></span>
                        <a href="#">
                        <h3>{{ App\Doctor::all()->count() }}</h3>
                        </a>
                        <p>
                            طبيب متاح لى الاجابة عن اسألتك الان
                        
                        </p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="single_service">
                        <span class="lnr lnr-users"></span>
                        <a href="#">
                            <h3>{{ App\User::where('isDoctor',false)->count() }}</h3>
                        </a>
                        <p>
                            مستخدم انضم الى اسرة الموقع
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Offered Services Area =================-->


    <!--================ Start recent-blog Area =================-->
    <section class="recent-blog-area section_gap">
        <div class="container">
            <div class="row justify-content-center section-title-wrap">
                <div class="col-lg-12">
                    <h1>سجل معنا الان</h1>

                </div>
            </div>
            <div class="row">
                <div class="single-recent-blog col-lg-6 col-md-6 text-center">
                    <h4>يخولك التسجيل كمستخدم بالقيام </h4>
<br>
                    <h5>البحث عن افضل اخصائي</h5>
<br>

                    <h5>البحث عن امراض</h5>
<br>

                    <h5>المحادثة مع الاخصائيين</h5>
<br>

                    <h5>عرض ملفات الاخصائيين</h5>

 <br>
    
                    <a class="main_btn mr-10" href="/register">سجل الان</a>
                </div>
                <div class="single-recent-blog col-lg-6 col-md-6 text-center">
                    <h4>يخولك التسجيل كطبيب بالقيام </h4>
                <br>
                    <h5>البحث عن افضل اخصائي</h5>
                    <br>
                    <h5>الاجابة على إستشارات المستخدمين</h5>
                    <br>
                    <h5>نشر سيرتك الداتية </h5>
                    <br>
                    <h5>نشر جدول الاعمال والمواعيد</h5>
                    <br>
                
                    <a class="main_btn mr-10" href="/doctors/register">سجل الان</a>
                </div>

            </div>
        </div>
    </section>
    <!--================ end recent-blog Area =================-->

    <!--================ start footer Area =================-->
    <footer id="aboutUs" class="footer-area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-2  col-md-6">
                    <div class="single-footer-widget">
                        <h6>حول الموقع</h6>
                        <ul class="footer-nav">
                            <li>
                                <a href="#">تأسس سنة 2019</a>
                            </li>
                            <li>
                                <a href="#">بدأ كمشروع تخرج </a>
                            </li>
                            <li>
                                <a href="#">حتى اصبح اكبر موقع عربي للأستشارات الطبية اون لاين</a>
                            </li>
                            <li>
                                <a href="#">سارع بالانضمام الى اسرة الموقع</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

            <div class="row footer-bottom d-flex justify-content-between">
                <p class="col-lg-8 col-sm-12 footer-text m-0">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    حقوق النشر محفوظة &copy;
                    <script>
                        document.write(new Date().getFullYear());
                    </script>  <i class="fa fa-heart-o" aria-hidden="true"></i>    
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
                <div class="col-lg-4 col-sm-12 footer-social">
                    <a href="#">
						<i class="fa fa-facebook"></i>
					</a>
                    <a href="#">
						<i class="fa fa-twitter"></i>
					</a>
                    <a href="#">
						<i class="fa fa-dribbble"></i>
					</a>
                    <a href="#">
						<i class="fa fa-behance"></i>
					</a>
                </div>
            </div>
        </div>
    </footer>
    <!--================ End footer Area =================-->



    <!--================ Optional JavaScript =================-->
    <!--================ jQuery first, then Popper.js, then Bootstrap JS =================-->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('vendors/nice-select/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('vendors/isotope/isotope-min.js') }}"></script>
    <script src="{{ asset('vendors/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('js/mail-script.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
</body>

</html>


{{--
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>المستشار الطبي</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="main/images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="main/images/favicon.ico" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="main/css/bootstrap.min.css">
    <link rel="stylesheet" href="main/css/owl.carousel.min.css">
    <link rel="stylesheet" href="main/css/themify-icons.css">
    <link rel="stylesheet" href="main/css/magnific-popup.css">
    <link href="{{ asset('/css/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="main/css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="main/css/normalize.css">
    <link rel="stylesheet" href="main/style.css">
    <link rel="stylesheet" href="main/css/responsive.css">
    <script src="main/js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body dir="rtl" data-spy="scroll" data-target="#primary-menu">

    <div class="preloader">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    <!--Mainmenu-area-->
    <div class="mainmenu-area" data-spy="affix" data-offset-top="100">
        <div class="container">
            <!--Logo-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand logo">
                    <h2>المستشار الطبي</h2>
                </a>
            </div>
            <!--Logo/-->
            <nav class="collapse navbar-collapse" id="primary-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home-page"> الرئيسية</a></li>
                    <li><a href="#service-page">معلومات</a></li>
                    <li><a href="#feature-page">Features</a></li>
                    <li><a href="#price-page">Pricing</a></li>
                    <li><a href="#team-page">Team</a></li>
                    <li><a href="#faq-page">من نحن</a></li>
                    <li><a href="#blog-page">Blog</a></li>
                    <li><a href="#price-page">التسجيل</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--Mainmenu-area/-->



    <!--Header-area-->
    <header class="header-area overlay full-height relative v-center" id="home-page">
        <div class="absolute anlge-bg"></div>
        <div class="container">
            <div class="row v-center">
                <div class="col-xs-12 col-md-7 header-text">
                    <h2>إستشاراتك الطبية مجابة</h2>
                    <p>جميع الاطباء ذوي خبرات علمية ممتازة مرخصون وعلى درجة عالية من الكفاءة في اختصاصاتهم..</p>
                    <a href="/search" class="button white">ابحث عن افضل اخصائي</a>
                </div>
                <div class="hidden-xs hidden-sm col-md-5 text-left">
                    <div class="screen-box screen-slider">
                        <div class="item">
                            <img src="main/images/screen-1.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="main/images/screen-2.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="main/images/screen-3.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="main/images/screen-4.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="main/images/screen-5.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--Header-area/-->



    <!--Feature-area-->
    <section class="gray-bg section-padding" id="service-page">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="main/images/service-icon-2.png" alt="">
                        </div>
                        <h4>AWESOEM DESIGN</h4>
                        <p>مستخدم انضم الى اسرة الموقع</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="main/images/service-icon-3.png" alt="">
                        </div>
                        <h4>EASY TO CUSTOMAIZE</h4>
                        <p>طبيب متاح لى الاجابة عن اسألتك الان</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <div class="box">
                        <div class="box-icon">
                            <img src="main/images/service-icon-1.png" alt="">
                        </div>
                        <h4>1520</h4>
                        <p>محادثة اجريت من قبل المستخدمين</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Feature-area/-->

    <section class="angle-bg sky-bg section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div id="caption_slide" class="carousel slide caption-slider" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>سهولة التسجيل</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>سجل الان مع موقع المستشار الطبي فقط املاء بياناتك الشخصية و قم بتأكيد بريدك الشخصي.</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#price-page" class="button">سجل الان</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>استشارات طبية على مدار الساعة</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p> سوف يقوم بالاجابة على تساؤلاتك الطبية طاقم من امهر الاخصائيين .</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#price-page" class="button">سجل الان</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-4.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>سهولة البحث</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>إختر التخصص والأعراض التي ظهرت وسيقوم الموقع بتوفير الامراض وامهر الاخصائيين
                                                في هذه الامراض.</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="/search" class="button">إبحث الان</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-7.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item row">
                                <div class="v-center">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="caption-title" data-animation="animated fadeInUp">
                                            <h2>Awesome design</h2>
                                        </div>
                                        <div class="caption-desc" data-animation="animated fadeInUp">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                                Duis aute</p>
                                        </div>
                                        <div class="caption-button" data-animation="animated fadeInUp">
                                            <a href="#" class="button">Read more</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo one" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-3.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-3">
                                        <div class="caption-photo two" data-animation="animated fadeInRight">
                                            <img src="main/images/screen-4.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Indicators -->
                        <ol class="carousel-indicators caption-indector">
                            <li data-target="#caption_slide" data-slide-to="0" class="active">
                                <strong>سهولة التسجيل </strong>سجل الان ولا تتردد.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="1">
                                <strong>إستشارات طبية </strong>إطرح اسألتك بكل خصوصية.
                            </li>
                            <li data-target="#caption_slide" data-slide-to="2">
                                <strong>سهولة البحث</strong>البحث بضغطة زر.
                            </li>

                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="section-padding gray-bg" id="team-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>اشهر الاخصائيين في كل تخصص</h2>
                        <p>نحن نفتخر بأن لدينا افضل الاخصائيين في شتى المجالات </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="main/images/team-section-1.png" alt="">
                        </div>
                        <h4>JEMY SEDONCE</h4>
                        <h6>Co. Founder</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="main/images/team-section-2.png" alt="">
                        </div>
                        <h4>DEBORAH BROWN</h4>
                        <h6>UX Designer</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="main/images/team-section-3.png" alt="">
                        </div>
                        <h4>HARRY BANKS</h4>
                        <h6>Founder</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="single-team">
                        <div class="team-photo">
                            <img src="main/images/team-section-4.png" alt="">
                        </div>
                        <h4>VICTORIA CLARK</h4>
                        <h6>Creative Director</h6>
                        <ul class="social-menu">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="price-area overlay section-padding" id="price-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>سجل الأن ولا تتردد</h2>
                        <p>يمكنك التسجيل معنا سواء كنت طبيب او مستخدم تبحث عن اجابات لاسألتك الطبية</p>
                    </div>
                </div>
            </div>
            <div class="row row-centered">
                <div class="col-xs-12 col-sm-offset-1 col-sm-4">
                    <div class="price-table active">
                        <span class="price-info"><i class="fa fa-user" style="font-size: 28px;margin-top: 6px;"></i></span>
                        <h3 class="text-uppercase price-title">مستخدم</h3>
                        <hr>
                        <ul class="list-unstyled">
                            <li><strong>يخولك التسجيل كمستخدم بالقيام </strong></li>
                            <li>البحث عن افضل اخصائي</li>
                            <li>البحث عن امراض</li>
                            <li>المحادثة مع الاخصائيين</li>
                            <li>عرض ملفات الاخصائيين </li>

                        </ul>
                        <hr>
                        <a href="/register" class="button">سجل الأن</a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-offset-2 col-sm-4">
                    <div class="price-table active">
                        <span class="price-info"><i class="fa fa-user-md" style="font-size: 28px;margin-top: 6px;"></i></span>
                        <h3 class="text-uppercase price-title">طبيب</h3>
                        <hr>
                        <ul class="list-unstyled">
                            <li><strong>يخولك التسجيل كطبيب بالقيام </strong></li>
                            <li>الاجابة على إستشارات المستخدمين</li>
                            <li>نشر سيرتك الداتية </li>
                            <li>نشر جدول الاعمال والمواعيد</li>
                            <li>طلب ترقية إلى استشاري</li>
                        </ul>
                        <hr>
                        <a href="/doctors/register" class="button">سجل الأن</a>
                    </div>
                </div>

            </div>
        </div>
    </section>




    <section class="gray-bg section-padding" id="faq-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Frequently Asked Questions</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere
                            harum fugiat!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="panel-group" id="accordion">
                        <div class="panel">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">Sedeiusmod tempor inccsetetur aliquatraiy?</a>
                            </h4>
                            <div id="collapse1" class="panel-collapse collapse in">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodas temporo incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrd exercitation ullamco
                                    laboris nisi ut aliquip ex comodo consequat. Duis aute dolor in reprehenderit.</p>
                            </div>
                        </div>
                        <div class="panel">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Tempor inccsetetur aliquatraiy?</a>
                            </h4>
                            <div id="collapse2" class="panel-collapse collapse">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodas temporo incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrd exercitation ullamco
                                    laboris nisi ut aliquip ex comodo consequat. Duis aute dolor in reprehenderit.</p>
                            </div>
                        </div>
                        <div class="panel">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Lorem ipsum dolor amet, consectetur adipisicing ?</a>
                            </h4>
                            <div id="collapse3" class="panel-collapse collapse">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodas temporo incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrd exercitation ullamco
                                    laboris nisi ut aliquip ex comodo consequat. Duis aute dolor in reprehenderit.</p>
                            </div>
                        </div>
                        <div class="panel">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Lorem ipsum dolor amet, consectetur adipisicing ?</a>
                            </h4>
                            <div id="collapse4" class="panel-collapse collapse">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodas temporo incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrd exercitation ullamco
                                    laboris nisi ut aliquip ex comodo consequat. Duis aute dolor in reprehenderit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>






    <footer class="footer-area relative sky-bg" id="contact-page">
        <div class="absolute footer-bg"></div>
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                        <div class="page-title">
                            <h2>Contact with us</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptates, temporibus at, facere
                                harum fugiat!</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-4">
                        <address class="side-icon-boxes">
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="main/images/location-arrow.png" alt="">
                                </div>
                                <p><strong>Address: </strong> Box 564, Disneyland <br />USA</p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="main/images/phone-arrow.png" alt="">
                                </div>
                                <p><strong>Telephone: </strong>
                                    <a href="callto:8801812726495">+8801812726495</a> <br />
                                    <a href="callto:8801687420471">+8801687420471</a>
                                </p>
                            </div>
                            <div class="side-icon-box">
                                <div class="side-icon">
                                    <img src="main/images/mail-arrow.png" alt="">
                                </div>
                                <p><strong>E-mail: </strong>
                                    <a href="mailto:youremail@example.com">youremail@example.com</a> <br />
                                    <a href="mailto:youremail@example.com">example@mail.com</a>
                                </p>
                            </div>
                        </address>
                    </div>
                    <div class="col-xs-12 col-md-8">
                        <form action="process.php" id="contact-form" method="post" class="contact-form">
                            <div class="form-double">
                                <input type="text" id="form-name" name="form-name" placeholder="Your name" class="form-control" required="required">
                                <input type="email" id="form-email" name="form-email" class="form-control" placeholder="E-mail address" required="required">
                            </div>
                            <input type="text" id="form-subject" name="form-subject" class="form-control" placeholder="Message topic">
                            <textarea name="message" id="form-message" name="form-message" rows="5" class="form-control" placeholder="Your message" required="required"></textarea>
                            <button type="sibmit" class="button">حفظ </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 pull-right">
                        <ul class="social-menu text-right x-left">
                            <li><a href="#"><i class="ti-facebook"></i></a></li>
                            <li><a href="#"><i class="ti-twitter"></i></a></li>
                            <li><a href="#"><i class="ti-google"></i></a></li>
                            <li><a href="#"><i class="ti-linkedin"></i></a></li>
                            <li><a href="#"><i class="ti-github"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id corrupti architecto consequuntur, laborum
                            quaerat sed nemo temporibus unde, beatae vel.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <p>&copy;Copyright 2018 All right resurved. This template is made with <i class="ti-heart" aria-hidden="true"></i>                            by <a href="https://colorlib.com">Colorlib</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>





    <!--Vendor-JS-->
    <script src="main/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="main/js/vendor/bootstrap.min.js"></script>
    <!--Plugin-JS-->
    <script src="main/js/owl.carousel.min.js"></script>
    <script src="main/js/contact-form.js"></script>
    <script src="main/js/jquery.parallax-1.1.3.js"></script>
    <script src="main/js/scrollUp.min.js"></script>
    <script src="main/js/magnific-popup.min.js"></script>
    <script src="main/js/wow.min.js"></script>
    <!--Main-active-JS-->
    <script src="main/js/main.js"></script>
</body>

</html> --}}