@extends('admin.layouts.app', ['page' => 'description'])

@section('title', 'إضافة  وصف')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  وصف</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.descriptions.store') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">الوصف</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="الوصف"
                            value="{{ old('name') }}"
                            id="name"
                        >
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('admin.descriptions.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
