@extends('admin.layouts.app', ['page' => 'description'])

@section('title', 'الأوصاف')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الأوصاف</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.descriptions.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الوصف</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($descriptions as $description)
                        <tr>
                            <td>{{ $description->id }}</td>
                            <td>{{ $description->name }}</td>
                            <td>
                                <a href="{{ route('admin.descriptions.edit', ['description' => $description->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('admin.descriptions.destroy', ['description' => $description->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $descriptions->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
