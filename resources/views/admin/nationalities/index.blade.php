@extends('admin.layouts.app', ['page' => 'nationality'])

@section('title', 'الجنسيات')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الجنسيات</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.nationalities.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>اسم الجنسية</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($nationalities as $nationality)
                        <tr>
                            <td>{{ $nationality->id }}</td>
                            <td>{{ $nationality->name }}</td>
                            <td>
                                @if($nationality->active)
                                <span class="label label-success">مفعل</span> @else
                                <span class="label label-danger">غير مفعل</span> @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.nationalities.edit', ['nationality' => $nationality->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('admin.nationalities.destroy', ['nationality' => $nationality->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $nationalities->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
