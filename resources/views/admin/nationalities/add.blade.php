@extends('admin.layouts.app', ['page' => 'nationality'])

@section('title', 'إضافة  جنسية')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  جنسية</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.nationalities.store') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">اسم الجنسية</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="اسم الجنسية"
                            value="{{ old('name') }}"
                            id="name"
                        >
                    </div>

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="hidden" name="active" value="0">
                            <input type="checkbox"
                                name="active"
                                value="1"
                                {{ old('active') == 1 ? 'checked' : '' }}
                            >
                                تفعيل
                        </label>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('admin.nationalities.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
