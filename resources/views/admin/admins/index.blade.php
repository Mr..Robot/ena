@extends('admin.layouts.app', ['page' => 'admins'])

@section('title', 'المشرفين')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">المشرفين</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.admins.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم كامل</th>
                        <th>إسم المستخدم</th>
                        <th>البريد الالكتروني</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($admins as $admin)
                        <tr>
                            <td>{{ $admin->id }}</td>
                            <td>{{ $admin->name }}</td>
                            <td>{{ $admin->username }}</td>
                            <td>{{ $admin->email }}</td>
                            <td> @if($admin->active)
                                <span class="label label-success">مفعل</span>
                                @else
                                <span class="label label-danger">غير مفعل</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.admins.activation', ['admin' => $admin->id]) }}">
                                    <i class="fa {{$admin->active ? 'fa-times': 'fa-check'}} "></i>
                                </a>
                                <a  href="{{ route('admin.admins.edit', ['admin' => $admin->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('admin.admins.destroy', ['admin' => $admin->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{-- {{ $admins->links('vendor.pagination.default') }} --}}
            </div>
        </div>
    </div>
</div>
@endsection
