@extends('admin.layouts.app', ['page' => 'doctor'])

@section('title', 'تعديل الطبيب')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل الطبيب</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.doctors.update', ['doctor' => $doctor->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="dgree">Dgree</label>
                        <input type="number"
                            class="form-control"
                            name="dgree"
                            required
                            placeholder="Dgree"
                            value="{{ old('dgree', $doctor->dgree) }}"
                            step="any"
                            id="dgree"
                        >
                    </div>

                    <div class="form-group">
                        <label for="ref_no">ref_no</label>
                        <input type="text"
                            class="form-control"
                            name="ref_no"
                            required
                            placeholder="ref_no"
                            value="{{ old('ref_no', $doctor->ref_no) }}"
                            id="ref_no"
                        >
                    </div>

                    <div class="form-group">
                        <label for="specialty-id">التخصص</label>
                        <select class="form-control"
                            name="specialty_id"
                            required
                            id="specialty-id"
                        >
                            @foreach ($specialties as $specialty)
                                <option value="{{ $specialty->id }}"
                                    {{ old('specialty_id', $doctor->specialty_id) == $specialty->id ? 'selected' : '' }}
                                >
                                    {{ $specialty->namear }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="diseases">الأمراض</label>
                        <select class="form-control"
                            name="diseases[]"
                            required
                            multiple
                            id="diseases"
                        >
                            @foreach ($diseases as $disease)
                                <option value="{{ $disease->id }}"
                                    {{ in_array($disease->id, old('diseases', $doctor->diseases)) ? 'selected' : '' }}
                                >
                                    {{ $disease->code }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.doctors.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
