@extends('admin.layouts.app', ['page' => 'disease'])

@section('title', 'الأمراض')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الأمراض</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.diseases.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الرمز</th>
                        <th>الإسم العلمي </th>
                        <th>الإسم الشائع</th>
                        <th>التخصص</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($diseases as $i => $disease)
                        <tr>
                            <td>{{ $i+1 }}</td>
                            <td>{{ $disease->code }}</td>
                            <td>{{ $disease->name }}</td>
                            <td>{{ $disease->commonname }}</td>
                            <td>
                                <span class="label label-info">{{ $disease->specialty->namear }}</span>
                                <br>
                                <span class="label bg-teal-active">  {{ $disease->specialty->nameen }}</span>
                            </td>
                            <td>
                                @if($disease->active)
                                <span class="label label-success">مفعل</span>
                                @else
                                <span class="label label-danger">غير مفعل</span>
                                @endif
                            </td>
                            <td>
                                    
                                <a href="{{ route('admin.diseases.edit', ['disease' => $disease->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                {{-- <form action="{{ route('admin.diseases.destroy', ['disease' => $disease->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form> --}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="6">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $diseases->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
