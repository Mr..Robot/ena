@extends('admin.layouts.app', ['page' => 'disease'])

@section('title', 'تعديل مرض')
@section('styles')
<link href="{{ asset('/css/admin/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/admin/AdminLTE.min.css') }}" rel="stylesheet">
<link href="{{ asset('/plugins/iCheck/all.css') }}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل مرض</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.diseases.update', ['disease' => $disease->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="code">الرمز</label>
                        <input type="text"
                            class="form-control"
                            name="code"
                            required
                            placeholder="الرمز"
                            value="{{ old('code', $disease->code) }}"
                            id="code"
                        >
                    </div>

                    <div class="form-group">
                        <label for="name">الإسم العلمي </label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="الإسم العلمي "
                            value="{{ old('name', $disease->name) }}"
                            id="name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="commonname">الإسم الشائع</label>
                        <input type="text"
                            class="form-control"
                            name="commonname"
                            required
                            placeholder="الإسم الشائع"
                            value="{{ old('commonname', $disease->commonname) }}"
                            id="commonname"
                        >
                    </div>

     

                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="hidden" name="active" value="0">
                            <input type="checkbox"
                            class="flat"
                                name="active"
                                value="1"
                                {{ old('active', $disease->active) == 1 ? 'checked' : '' }}
                            >
                                تفعيل
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="specialty-id">التخصص</label>
                        <select class="form-control"
                            name="specialty_id"
                            required
                            id="specialty-id"
                        >
                            @foreach ($specialties as $specialty)
                                <option value="{{ $specialty->id }}"
                                    {{ old('specialty_id', $disease->specialty_id) == $specialty->id ? 'selected' : '' }}
                                >
                                    {{ $specialty->namear }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="symptoms">الأعراض</label>
                        <select class="form-control select2"
                            name="symptoms[]"
                            
                            multiple
                            id="symptoms"
                        >
                            @foreach ($symptoms as $symptom)
                                <option value="{{ $symptom->id }}"
                                    {{ in_array($symptom->id, old('symptoms', $disease->symptoms)) ? 'selected' : '' }}
                                >
                                    {{ $symptom->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.diseases.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

    <script src="{{ asset('/js/admin/select2.full.min.js') }}"></script>
    <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>

<script>
 $(function () {
    // Initialize Select2 Elements
     $('.select2').select2()

 //iCheck for checkbox and radio inputs
 $('input[type="checkbox"].flat, input[type="radio"].flat').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })
    // Timepicker
    // $('.timepicker').timepicker({
    //   showInputs: false,
    //   showMeridian:false
    // })
  })
</script>

@endsection