@extends('admin.layouts.app', ['page' => 'certificate'])

@section('title', 'تعديل شهادة')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل شهادة</h3>
            </div>

            <form role="form" method="POST" enctype="multipart/form-data" action="{{ route('admin.certificates.update', ['certificate' => $certificate->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">إسم الشهادة</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="إسم الشهادة"
                            value="{{ old('name', $certificate->name) }}"
                            id="name"
                        >
                    </div>

                    <img src="{{ $certificate->getFirstMediaUrl('link') }}"
                        width="50"
                        alt="الملف صورة"
                    >
                    <div class="form-group">
                        <label for="link">الملف</label>
                        <input type="file"
                            class="form-control"
                            name="link"
                            required
                            value="{{ old('link', $certificate->link) }}"
                            id="link"
                        >
                    </div>

                    {{-- <div class="form-group">
                        <label for="doctor-id">إسم الطبيب</label>
                        <select class="form-control"
                            name="doctor_id"
                            required
                            id="doctor-id"
                        >
                            @foreach ($doctors as $doctor)
                                <option value="{{ $doctor->id }}"
                                    {{ old('doctor_id', $certificate->doctor_id) == $doctor->id ? 'selected' : '' }}
                                >
                                    {{ $doctor->ref_no }}
                                </option>
                            @endforeach
                        </select>
                    </div> --}}
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.certificates.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
