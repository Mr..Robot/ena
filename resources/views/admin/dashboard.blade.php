@extends('admin.layouts.app', ['page' => 'dashboard'])

@section('title', 'لوحة التحكم الرئيسية')
@section('styles')
<link href="{{ asset('/css/star-rating.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/themes/krajee-fa/theme.css') }}" rel="stylesheet" type="text/css" media="all">
<style>
  .clear-rating {
    display: none !important;
  }
</style>
@endsection
@section('content')
  
        <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3>{{ App\DoctorJoinRequest::all()->count() }}</h3>
    
                  <p>طلبات الإشتراك</p>
                </div>
                <div class="icon">
                  <i class="fa fa-pencil"></i>
                </div>
                <a href="{{ route('admin.requests.index') }}" class="small-box-footer">
                  تفاصيل اكتر <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3>{{ App\User::all()->count() }}</h3>
    
                  <p>عدد المستخدمين</p>
                </div>
                <div class="icon">
                  <i class="fa fa-users"></i>
                </div>
                <a href="{{ route('admin.people.index') }}" class="small-box-footer">
                    تفاصيل اكتر <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ App\Doctor::all()->count() }}</h3>
    
                  <p>عدد الاطباء</p>
                </div>
                <div class="icon">
                  <i class="fa fa-user-md"></i>
                </div>
                <a href="{{ route('admin.doctors.index') }}" class="small-box-footer">
                    تفاصيل اكتر <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ App\Chat::all()->count() }}</h3>
                    
                    <p>مجموع رسائل المستخدمين</p>
                </div>
                <div class="icon">
                  <i class="fa fa-envelope"></i>
                </div>
                <a href="#" class="small-box-footer">
                        تفاصيل اكتر <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
            </div>
            <!-- ./col -->
          </div>
  <div class="row">
        <div class="col-md-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">أهم الأخصائيين </h3>
                    <div class="box-tools pull-right">
            
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <ul class="users-list clearfix">  
                        @foreach (App\Doctor::onlyActive() as $doc)
                        <li>
                          <img src="/avatars/{{ $doc->user->avatar }}" class="profile-user-img img-responsive img-circle" alt="User Image">
                          <a class="users-list-name" href="{{url('doctors/' .$doc->user->id )}}">{{ $doc->user->person->first_name. ' '.$doc->user->person->family_name }} // {{ $doc->ref_no }}</a>
                          <span class="users-list-date">{{ $doc->specialty->namear. ' -- '.$doc->specialty->nameen }}</span>
                       <input id="rate" name="rate" data-size="xs" class="rating kv-fa rating-loading" data-min="0" data-max="5" data-step="1" value="{{ $doc->userAverageRating }}">        
                        </li>
                        @endforeach      
                      
                    </ul>
                    <!-- /.users-list -->
                  </div>
                  <!-- /.box-body -->
           
                  <!-- /.box-footer -->
                </div>
                <!--/.box -->
              </div>
  </div>
@endsection
@section('scripts')
<script src="{{ asset('/js/star-rating.js') }}"></script>
<script src="{{ asset('/themes/krajee-fa/theme.js') }}"></script>
<script>
    $(document).on('ready', function () {
        $('.kv-fa').rating({
            theme: 'krajee-fa',
            filledStar: '<i class="fa fa-star"></i>',
            emptyStar: '<i class="fa fa-star-o"></i>'
        });
        
       
        $("#rate").rating();
    });
    
    $('.rating').on( 'change', function () {
        $("#docsJson" ).submit();
     });
    </script>
    @endsection