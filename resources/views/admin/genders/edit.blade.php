@extends('admin.layouts.app', ['page' => 'gender'])

@section('title', 'تعديل جنس')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل جنس</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.genders.update', ['gender' => $gender->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">الجنس</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="الجنس"
                            value="{{ old('name', $gender->name) }}"
                            id="name"
                        >
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.genders.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
