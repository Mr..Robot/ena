@extends('admin.layouts.app', ['page' => 'gender'])

@section('title', 'الأجناس')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الأجناس</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.genders.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($genders as $gender)
                        <tr>
                            <td>{{ $gender->id }}</td>
                            <td>{{ $gender->name }}</td>
                            <td>
                                <a href="{{ route('admin.genders.edit', ['gender' => $gender->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('admin.genders.destroy', ['gender' => $gender->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $genders->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
