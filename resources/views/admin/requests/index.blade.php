@extends('admin.layouts.app', ['page' => 'jrequest'])

@section('title', 'طلبات تسجيل الاطباء')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">طلبات اشتراك الاطباء</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم الأول</th>
                        <th>إسم الأب</th>
                        <th>اللقب</th>
                        <th>الشهادات</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($requests as $jrequest)
                        <tr>
                            <td>{{ $jrequest->id }}</td>
                            <td>{{ $jrequest->user->person->first_name }}</td>
                            <td>{{ $jrequest->user->person->second_name }}</td>
                            <td>{{ $jrequest->user->person->family_name }}</td>
                            
                           <td>
                               <a href="{{ $jrequest->certificate->getFirstMediaUrl('link') }}" >
                                 <img src="{{ $jrequest->certificate->getFirstMediaUrl('link') }}"
                                    width="50"
                                    alt="صورة الملف"
                                >
                            </a></td>
                            <td>
                                @if($jrequest->accepted)
                                <span class="label label-success">تم القبول</span> @else
                                <span class="label label-danger">غير مقبول</span> @endif
                            </td>
                            <td>
                                <a onclick=" return confirm('هل أنت متأكد انك تريد قبول هذا الاشتراك؟')" href="{{ route('admin.requests.approve', ['jrequest' => $jrequest->id]) }}">
                                    <i class="fa fa-check"></i>
                                </a>
                                
                                <a onclick="return confirm('هل أنت متأكد انك تريد رفض هدا الاشتراك')" href="{{ route('admin.requests.revoke', ['jrequest' => $jrequest->id]) }}">
                                    <i class="fa fa-times"></i>
                                </a>

                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $requests->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection