@extends('admin.layouts.app', ['page' => 'person'])

@section('title', 'تعديل مستخدم')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل مستخدم</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.people.update', ['person' => $person->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="first_name">الإسم الأول</label>
                        <input type="text"
                            class="form-control"
                            name="first_name"
                            required
                            placeholder="الإسم الأول"
                            value="{{ old('first_name', $person->first_name) }}"
                            id="first_name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="second_name">إسم الأب</label>
                        <input type="text"
                            class="form-control"
                            name="second_name"
                            required
                            placeholder="إسم الأب"
                            value="{{ old('second_name', $person->second_name) }}"
                            id="second_name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="last_name">إسم الجد</label>
                        <input type="text"
                            class="form-control"
                            name="last_name"
                            required
                            placeholder="إسم الجد"
                            value="{{ old('last_name', $person->last_name) }}"
                            id="last_name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="family_name">اللقب</label>
                        <input type="text"
                            class="form-control"
                            name="family_name"
                            required
                            placeholder="اللقب"
                            value="{{ old('family_name', $person->family_name) }}"
                            id="family_name"
                        >
                    </div>

                    <div class="form-group">
                        <label for="date_of_birth">تاريخ الميلاد</label>
                        <input type="date"
                            class="form-control"
                            name="date_of_birth"
                            required
                            placeholder="تاريخ الميلاد"
                            value="{{ old('date_of_birth', $person->date_of_birth) }}"
                            id="date_of_birth"
                        >
                    </div>

                    <div class="form-group">
                        <label for="gender-id">الجنس</label>
                        <select class="form-control"
                            name="gender_id"
                            required
                            id="gender-id"
                        >
                            @foreach ($genders as $gender)
                                <option value="{{ $gender->id }}"
                                    {{ old('gender_id', $person->gender_id) == $gender->id ? 'selected' : '' }}
                                >
                                    {{ $gender->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nationality-id">الجنسية</label>
                        <select class="form-control"
                            name="nationality_id"
                            required
                            id="nationality-id"
                        >
                            @foreach ($nationalities as $nationality)
                                <option value="{{ $nationality->id }}"
                                    {{ old('nationality_id', $person->nationality_id) == $nationality->id ? 'selected' : '' }}
                                >
                                    {{ $nationality->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.people.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
