@extends('admin.layouts.app', ['page' => 'person'])

@section('title', 'المستخدمين')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">المستخدمين</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.people.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم الأول</th>
                        <th>إسم الأب</th>
                        <th>إسم الجد</th>
                        <th>اللقب</th>
                        <th>الجنس</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($people as $person)
                        <tr>
                            <td>{{ $person->id }}</td>
                            <td>{{ $person->first_name }}</td>
                            <td>{{ $person->second_name }}</td>
                            <td>{{ $person->last_name }}</td>
                            <td>{{ $person->family_name }}</td>
                            <td>{{ $person->gender->name }}</td>
                            <td>
                                <a href="{{ route('admin.people.edit', ['person' => $person->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                <form action="{{ route('admin.people.destroy', ['person' => $person->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $people->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
