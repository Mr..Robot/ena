@extends('admin.layouts.app', ['page' => 'day'])

@section('title', 'تعديل يوم')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل يوم</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.days.update', ['day' => $day->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">اليوم</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="اليوم"
                            value="{{ old('name', $day->name) }}"
                            id="name"
                        >
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.days.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
