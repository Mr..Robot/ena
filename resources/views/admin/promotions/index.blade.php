@extends('admin.layouts.app', ['page' => 'promotion'])

@section('title', 'Promotions')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">طلبات الترقية</h3>
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم الأول</th>
                        <th>إسم الأب</th>
                        <th>اللقب</th>
                        <th>الملف الشخصي</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($promotions as $promotion)
                        <tr>
                            <td>{{ $promotion->id }}</td>
                            <td>{{ $promotion->user->person->first_name }}</td>
                            <td>{{ $promotion->user->person->second_name }}</td>
                            <td>{{ $promotion->user->person->family_name }}</td>
                            <td><a href="{{url('doctors',['user'=>$promotion->user->id])}}"> عرض</a></td>
                            <td>
                                @if($promotion->isaccepted)
                                <span class="label label-success">تم القبول</span> @else
                                <span class="label label-danger">غير مقبول</span> @endif
                            </td>
                            <td>
                                <a onclick="if (confirm('هل أنت متأكد انك تريد قبول هذا الاشتراك؟')) { this.parentNode.submit() }" href="{{ route('admin.promotions.approve', ['jrequest' => $promotion->id]) }}">
                                    <i class="fa fa-check"></i>
                                </a>
                            
                                <a onclick="if (confirm('هل أنت متأكد انك تريد رفض هدا الاشتراك')) { this.parentNode.submit() }" href="{{ route('admin.promotions.revoke', ['jrequest' => $promotion->id]) }}">
                                    <i class="fa fa-times"></i>
                                </a>
                            
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $promotions->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
