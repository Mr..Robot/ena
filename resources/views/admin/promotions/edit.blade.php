@extends('admin.layouts.app', ['page' => 'promotion'])

@section('title', 'تعديل Promotion')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل Promotion</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.promotions.update', ['promotion' => $promotion->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="hidden" name="isaccepted" value="0">
                            <input type="checkbox"
                                name="isaccepted"
                                value="1"
                                {{ old('isaccepted', $promotion->isaccepted) == 1 ? 'checked' : '' }}
                            >
                                Isaccepted
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="person-id">المستخدم</label>
                        <select class="form-control"
                            name="person_id"
                            required
                            id="person-id"
                        >
                            @foreach ($people as $person)
                                <option value="{{ $person->id }}"
                                    {{ old('person_id', $promotion->person_id) == $person->id ? 'selected' : '' }}
                                >
                                    {{ $person->firstname }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="certificate-id">الشهادة</label>
                        <select class="form-control"
                            name="certificate_id"
                            required
                            id="certificate-id"
                        >
                            @foreach ($certificates as $certificate)
                                <option value="{{ $certificate->id }}"
                                    {{ old('certificate_id', $promotion->certificate_id) == $certificate->id ? 'selected' : '' }}
                                >
                                    {{ $certificate->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="description-id">الوصف</label>
                        <select class="form-control"
                            name="description_id"
                            required
                            id="description-id"
                        >
                            @foreach ($descriptions as $description)
                                <option value="{{ $description->id }}"
                                    {{ old('description_id', $promotion->description_id) == $description->id ? 'selected' : '' }}
                                >
                                    {{ $description->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.promotions.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
