@extends('admin.layouts.app', ['page' => 'root'])

@section('title', 'الصلاحيات')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الصلاحيات</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم الأول</th>
                        <th>إسم الأب</th>
                        <th>اللقب</th>
                        <th>نوع الحساب</th>
                        <th>الصلاحيات</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($users as $user)
                        <tr>
                            <td>{{ $user->person->id }}</td>
                            <td>{{ $user->person->first_name }}</td>
                            <td>{{ $user->person->second_name }}</td>
                            <td>{{ $user->person->family_name }}</td>
                            <td>{{ $user->isDoctor ? 'أخصائي': 'مستخدم' }}</td>
                            <td>
                                <label class="checkbox-inline">
                                    <input type="hidden" name="isAdmin" value="0">
                                    <input type="checkbox" name="active" disabled value="1"
                                        {{ old('active', $user->isAdmin) == 1 ? 'checked' : '' }} >
                                    مشرف
                                </label>
                        
                            </td>
                            <td>
                                <a href="{{ route('admin.privlages.edit', ['person' => $user->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>
                         
                                    <a title="حظر" href="{{ route('admin.blacklist.add', ['user' => $user->id]) }}">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                          
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="7">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            
        </div>
    </div>
</div>
@endsection
