@extends('admin.layouts.app', ['page' => 'person'])

@section('title', 'تعديل مستخدم')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل صلاحيات المستخدم</h3>
            </div>
<div class="box-body">
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>الإسم الأول</th>
            <th>إسم الأب</th>
            <th>إسم الجد</th>
            <th>اللقب</th>
            <th>الصلاحيات</th>
            <th>العمليات</th>
        </tr>

        <tr>
        <form action="{{route('admin.privlages.update',['user'=>$user->id])}}" method="post">
            @method('PUT')
            @csrf
            <td>{{ $user->person->id }}</td>
            <td>{{ $user->person->first_name }}</td>
            <td>{{ $user->person->second_name }}</td>
            <td>{{ $user->person->last_name }}</td>
            <td>{{ $user->person->family_name }}</td>
            <td><label class="checkbox-inline">
                <input type="hidden" name="isAdmin" value="0">
                <input type="checkbox" name="isAdmin" value="1"
                {{ old('active', $user->isAdmin) == 1 ? 'checked' : '' }} >
                مشرف
                </label>
            </td>
            <td>
                <button type="submit" class="btn btn-primary">حفظ التعديلات</button>
                <a href="{{ route('admin.privlages.index') }}" class="btn btn-default">إلغاء الامر</a>
            </td>   
            </form>
        </tr>
    </table> 
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
