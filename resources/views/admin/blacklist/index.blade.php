@extends('admin.layouts.app', ['page' => 'blacklist']) 
@section('title', 'قائمة الرفض') 
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">قائمة الرفض</h3>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>الإسم الأول</th>
                        <th>إسم الأب</th>
                        <th>اللقب</th>
                        <th>نوع الحساب</th>
                        <th>ازالة من قائمة الرفض</th>
                    </tr>

                    @forelse ($users as $user)
                    <tr>
                        <td>{{ $user->person->id }}</td>
                        <td>{{ $user->person->first_name }}</td>
                        <td>{{ $user->person->second_name }}</td>
                        <td>{{ $user->person->family_name }}</td>
                        <td>{{ $user->isDoctor ? 'أخصائي': 'مستخدم' }}</td>
                        
                        <td>
                            <a title="ازالة" href="{{ route('admin.blacklist.remove', ['user' => $user->id]) }}">
                                    <i class="fa fa-times"></i>
                                </a>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7">لا توجد أي سجلات</td>
                    </tr>
                    @endforelse
                </table>
            </div>


        </div>
    </div>
</div>
@endsection