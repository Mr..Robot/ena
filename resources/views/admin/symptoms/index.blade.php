@extends('admin.layouts.app', ['page' => 'symptom'])

@section('title', 'الأعراض')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">الأعراض</h3>

                <a class="pull-right btn btn-sm btn-primary" href="{{ route('admin.symptoms.create') }}">
                    إضافة 
                </a>
            </div>
            <div class="box-body">
                <table class="table table-bordered">
                    <tr>
                        <th>#</th>
                        <th>اسم العرض</th>
                        <th>الإسم الشائع</th>
                        <th>الحالة</th>
                        <th>العمليات</th>
                    </tr>

                    @forelse ($symptoms as $symptom)
                        <tr>
                            <td>{{ $symptom->id }}</td>
                            <td>{{ $symptom->name }}</td>
                            <td>{{ $symptom->commonname }}</td>
                            <td>
                                @if($symptom->active)
                                <span class="label label-success">مفعل</span> @else
                                <span class="label label-danger">غير مفعل</span> @endif
                            </td>
                            <td>
                                <a href="{{ route('admin.symptoms.edit', ['symptom' => $symptom->id]) }}">
                                    <i class="fa fa-pencil-square-o"></i>
                                </a>

                                {{-- <form action="{{ route('admin.symptoms.destroy', ['symptom' => $symptom->id]) }}"
                                    method="POST"
                                    class="inline pointer"
                                >
                                    @csrf
                                    @method('DELETE')

                                    <a onclick="if (confirm('هل أنت متأكد انك تريد حدف هدا السجل؟')) { this.parentNode.submit() }">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </form> --}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4">لا توجد أي سجلات</td>
                        </tr>
                    @endforelse
                </table>
            </div>

            <div class="box-footer clearfix">
                {{ $symptoms->links('vendor.pagination.default') }}
            </div>
        </div>
    </div>
</div>
@endsection
