@extends('admin.layouts.app', ['page' => 'clink'])

@section('title', 'إضافة  مصحة')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">إضافة  مصحة</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.clinks.store') }}">
                @csrf

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">إسم المصحة</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="إسم المصحة"
                            value="{{ old('name') }}"
                            id="name"
                        >
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ </button>

                    <a href="{{ route('admin.clinks.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
