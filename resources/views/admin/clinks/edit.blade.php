@extends('admin.layouts.app', ['page' => 'clink'])

@section('title', 'تعديل مصحة')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">تعديل مصحة</h3>
            </div>

            <form role="form" method="POST" action="{{ route('admin.clinks.update', ['clink' => $clink->id]) }}">
                @csrf
                @method('PUT')

                <div class="box-body">
                    <div class="form-group">
                        <label for="name">إسم المصحة</label>
                        <input type="text"
                            class="form-control"
                            name="name"
                            required
                            placeholder="إسم المصحة"
                            value="{{ old('name', $clink->name) }}"
                            id="name"
                        >
                    </div>
                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">حفظ التعديلات</button>

                    <a href="{{ route('admin.clinks.index') }}" class="btn btn-default">
                        إلغاء الامر
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
