## Enas

This application is built using Laravel Factory.

### Installation
1. Setup a site in your local environment.
2. Create a database.
3. Copy and extract the .zip file in the project directory.
4. Set config options in the `.env` file.
5. Run `bash install.sh` in the terminal. (You can also run commands of the install.sh file one-by-one manually)

### Access

Open `yoursiteurl.dev/admin` to access the admin panel.
- Username: `admin`
- Password: `123456`

### Details about the app
#### Admin panel theme: [AdminLTE](https://github.com/almasaeed2010/AdminLTE)

#### Models
1. Specialty
2. Disease
3. Symptom
4. Certificate
5. Person
6. Nationality
7. Doctor
8. Appointment
9. Day
10. Gender
11. Description
12. Promotion
13. Clink

#### Packages
1. [Laravel Media Library](https://github.com/spatie/laravel-medialibrary)

Made with Love by [Laravel Factory](https://laravelfactory.com/).
