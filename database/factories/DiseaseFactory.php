<?php

use Faker\Generator as Faker;

$factory->define(App\Disease::class, function (Faker $faker) {
    return [
        'code' => substr($faker->name(), 0, 10),
        'name' => $faker->name(),
        'commonname' => $faker->name(),
        'active' => $faker->boolean(),
        'specialty_id' => function () {
            return App\Specialty::all()->random()->id;
        },
    ];
});
