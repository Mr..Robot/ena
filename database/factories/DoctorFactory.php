<?php

use Faker\Generator as Faker;

$factory->define(App\Doctor::class, function (Faker $faker) {
    return [
        'dgree' => $faker->randomDigitNotNull(),
        'ref_no' => $faker->word(),
        'specialty_id' => function () {
            return App\Specialty::all()->random()->id;
        },
        'user_id' => function () {
            return App\User::all()->random()->id;
        },
    ];
});
