<?php

use Faker\Generator as Faker;

$factory->define(App\Person::class, function (Faker $faker) {
    return [
        'firstname' => $faker->name(),
        'secondname' => $faker->name(),
        'lastname' => $faker->name(),
        'familyname' => $faker->name(),
        'datebirth' => $faker->date(),
        'gender_id' => function () {
            return App\Gender::all()->random()->id;
        },
        'nationality_id' => function () {
            return App\Nationality::all()->random()->id;
        },
        'user_id' => function () {
            return App\User::all()->random()->id;
        },
    ];
});
