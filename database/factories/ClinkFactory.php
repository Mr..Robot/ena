<?php

use Faker\Generator as Faker;

$factory->define(App\Clink::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
    ];
});
