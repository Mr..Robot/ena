<?php

use Faker\Generator as Faker;

$factory->define(App\Certificate::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'doctor_id' => function () {
            return App\Doctor::all()->random()->id;
        },
    ];
});
