<?php

use Faker\Generator as Faker;

$factory->define(App\Appointment::class, function (Faker $faker) {
    return [
        'from' => $faker->time(),
        'to' => $faker->time(),
        'day_id' => function () {
            return App\Day::all()->random()->id;
        },
        'doctor_id' => function () {
            return App\Doctor::all()->random()->id;
        },
        'clink_id' => function () {
            return App\Clink::all()->random()->id;
        },
    ];
});
