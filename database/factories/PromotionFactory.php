<?php

use Faker\Generator as Faker;

$factory->define(App\Promotion::class, function (Faker $faker) {
    return [
        'isaccepted' => $faker->boolean(),
        'person_id' => function () {
            return App\Person::all()->random()->id;
        },
        'certificate_id' => function () {
            return App\Certificate::all()->random()->id;
        },
        'description_id' => function () {
            return App\Description::all()->random()->id;
        },
    ];
});
