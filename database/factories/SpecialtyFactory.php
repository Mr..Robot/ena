<?php

use Faker\Generator as Faker;

$factory->define(App\Specialty::class, function (Faker $faker) {
    return [
        'namear' => $faker->name(),
        'nameen' => $faker->name(),
        'active' => $faker->boolean(),
    ];
});
