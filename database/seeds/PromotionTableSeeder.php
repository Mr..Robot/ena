<?php

use Illuminate\Database\Seeder;

class PromotionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Promotion::class, 5)->create()->each(function ($promotion) {
            $promotion->certificate->addMediaFromUrl('https://source.unsplash.com/random/200x200')
                ->toMediaCollection('link')
            ;
        });
    }
}
