<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(GenderTableSeeder::class);
      $this->call(NationalityTableSeeder::class);   
      $this->call(DayTableSeeder::class);
      $this->call(DescriptionTableSeeder::class);
      $this->call(ClinkTableSeeder::class);
      //$this->call(UsersTableSeeder::class);
         $this->call(SpecialtyTableSeeder::class);
         $this->call(DiseaseTableSeeder::class);
         $this->call(SymptomTableSeeder::class);
      // //  $this->call(CertificateTableSeeder::class);
         
      $this->call(UserTableSeeder::class);
      $this->call(PersonTableSeeder::class);
      $this->call(DoctorTableSeeder::class);
     
         
         
      
      // //  $this->call(PromotionTableSeeder::class);
         
         $this->call(AdminsTableSeeder::class);
         $this->call(AppointmentTableSeeder::class);
    }
}
