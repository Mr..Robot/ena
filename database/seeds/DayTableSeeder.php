<?php
use App\Day;
use Illuminate\Database\Seeder;

class DayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        {
            Day::create([
                'name'=>'السبت']
            )->save();
            Day::create([
                'name'=>'الأحد']
            )->save();
            Day::create([
                'name'=>'الإثنين']
            )->save();
            Day::create([
                'name'=>'الثلاثاء']
            )->save();
            Day::create([
                'name'=>'الأربعاء']
            )->save();
            Day::create([
                'name'=>'الخميس']
            )->save();
            Day::create([
                'name'=>'الجمعة']
            )->save();
        
        }
    }
}
