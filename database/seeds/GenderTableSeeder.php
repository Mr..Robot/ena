<?php

use Illuminate\Database\Seeder;
use App\Gender;

class GenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::create([
            'name'=>'ذكر']
        )->save();
        Gender::create([
            'name'=>'إنثى']
        )->save();
     //   factory(App\Gender::class, 2)->create();
    }
}
