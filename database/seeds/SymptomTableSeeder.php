<?php

use Illuminate\Database\Seeder;

class SymptomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Symptom::class, 5)->create();
    }
}
