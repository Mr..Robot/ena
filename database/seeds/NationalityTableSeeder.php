<?php

use Illuminate\Database\Seeder;
use App\Nationality;

class NationalityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Nationality::create([
            'name'=>'ليبي']
        )->save();
        Nationality::create([
            'name'=>'مصري']
        )->save();
        Nationality::create([
            'name'=>'تونسي']
        )->save();
        Nationality::create([
            'name'=>'مغربي']
        )->save();
    }
}
