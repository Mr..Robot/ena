<?php

use Illuminate\Database\Seeder;
use App\Clink;

class ClinkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Clink::create([
            'name'=>'مصحة رويال']
        )->save();
        Clink::create([
            'name'=>'مصحة اويا']
        )->save();
        Clink::create([
            'name'=>'مصحة المسرة']
        )->save();
        Clink::create([
            'name'=>'المركز الليبي السويسري']
        )->save();
        Clink::create([
            'name'=>'المركز الليبي الاكراني']
        )->save();
    }
}
