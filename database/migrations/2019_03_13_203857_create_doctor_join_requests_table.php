<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorJoinRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_join_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('accepted');
            $table->boolean('revoked');
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();            
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_join_requests');
    }
}
