<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('isaccepted');
            $table->integer('person_id')->unsigned()->index();
            $table->integer('certificate_id')->unsigned()->index();
            $table->integer('description_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('person_id')->references('id')->on('people');
            $table->foreign('certificate_id')->references('id')->on('certificates');
            $table->foreign('description_id')->references('id')->on('descriptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
