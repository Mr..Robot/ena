<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiseaseDoctorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disease_doctor', function (Blueprint $table) {
            $table->integer('disease_id')->unsigned()->index();
            $table->integer('doctor_id')->unsigned()->index();
            
            $table->foreign('disease_id')->references('id')->on('diseases');
            $table->foreign('doctor_id')->references('id')->on('doctors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disease_doctor');
    }
}
