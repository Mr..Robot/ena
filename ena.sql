-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for ena
DROP DATABASE IF EXISTS `ena`;
CREATE DATABASE IF NOT EXISTS `ena` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `ena`;

-- Dumping structure for table ena.admins
DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.admins: ~0 rows (approximately)
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT IGNORE INTO `admins` (`id`, `name`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Shad Hermann', 'admin@test.com', 'admin', '$2y$10$0VrErQRFsUUdoV57E49bEeeY6bgQWI4iiIWiYsaReEXEMF4sTqEte', 'kVX3B46SAD0vhj0TlsczaR3CaHcdE4VviB0Yc1g8khrRDENf2o6FEfHs8oOD', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL);
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- Dumping structure for table ena.admin_password_resets
DROP TABLE IF EXISTS `admin_password_resets`;
CREATE TABLE IF NOT EXISTS `admin_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `admin_password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.admin_password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `admin_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_password_resets` ENABLE KEYS */;

-- Dumping structure for table ena.appointments
DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` time NOT NULL,
  `to` time NOT NULL,
  `day_id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `clink_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `appointments_day_id_index` (`day_id`),
  KEY `appointments_doctor_id_index` (`doctor_id`),
  KEY `appointments_clink_id_index` (`clink_id`),
  CONSTRAINT `appointments_clink_id_foreign` FOREIGN KEY (`clink_id`) REFERENCES `clinks` (`id`),
  CONSTRAINT `appointments_day_id_foreign` FOREIGN KEY (`day_id`) REFERENCES `days` (`id`),
  CONSTRAINT `appointments_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.appointments: ~3 rows (approximately)
/*!40000 ALTER TABLE `appointments` DISABLE KEYS */;
INSERT IGNORE INTO `appointments` (`id`, `from`, `to`, `day_id`, `doctor_id`, `clink_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '10:49:19', '07:42:53', 1, 2, 2, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, '02:00:00', '02:00:00', 1, 10, 1, '2019-03-20 23:52:56', '2019-03-20 23:52:56', NULL),
	(3, '02:00:00', '02:00:00', 1, 10, 1, '2019-03-20 23:54:18', '2019-03-20 23:54:18', NULL),
	(4, '02:00:00', '07:00:00', 5, 10, 4, '2019-03-20 23:54:37', '2019-03-20 23:54:37', NULL),
	(5, '14:30:00', '18:30:00', 4, 10, 5, '2019-03-21 13:28:40', '2019-03-21 13:28:40', NULL);
/*!40000 ALTER TABLE `appointments` ENABLE KEYS */;

-- Dumping structure for table ena.certificates
DROP TABLE IF EXISTS `certificates`;
CREATE TABLE IF NOT EXISTS `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `certificates_doctor_id_index` (`doctor_id`),
  CONSTRAINT `certificates_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.certificates: ~3 rows (approximately)
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;
INSERT IGNORE INTO `certificates` (`id`, `name`, `doctor_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'بكالوريوس', 10, '2019-03-21 01:14:11', '2019-03-20 23:41:21', '2019-03-20 23:41:21'),
	(2, 'Scor_Fix', 10, '2019-03-20 23:37:53', '2019-03-20 23:37:53', NULL),
	(3, 'GOGOGOG', 10, '2019-03-20 23:41:38', '2019-03-20 23:41:38', NULL),
	(4, 'Killing Haslhoof', 10, '2019-03-21 13:29:02', '2019-03-21 13:29:08', '2019-03-21 13:29:08');
/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;

-- Dumping structure for table ena.clinks
DROP TABLE IF EXISTS `clinks`;
CREATE TABLE IF NOT EXISTS `clinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.clinks: ~11 rows (approximately)
/*!40000 ALTER TABLE `clinks` DISABLE KEYS */;
INSERT IGNORE INTO `clinks` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'مصحة رويال', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'مصحة اويا', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'مصحة المسرة', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(4, 'المركز الليبي السويسري', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(5, 'المركز الليبي الاكراني', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(6, 'مصحة الفردوسه', '2019-03-13 14:21:31', '2019-03-13 14:21:48', NULL),
	(7, 'مصحة رويال', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(8, 'مصحة اويا', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(9, 'مصحة المسرة', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(10, 'المركز الليبي السويسري', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(11, 'المركز الليبي الاكراني', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `clinks` ENABLE KEYS */;

-- Dumping structure for table ena.days
DROP TABLE IF EXISTS `days`;
CREATE TABLE IF NOT EXISTS `days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.days: ~14 rows (approximately)
/*!40000 ALTER TABLE `days` DISABLE KEYS */;
INSERT IGNORE INTO `days` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'السبت', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'الأحد', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'الإثنين', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(4, 'الثلاثاء', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(5, 'الأربعاء', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(6, 'الخميس', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(7, 'الجمعة', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(8, 'السبت', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(9, 'الأحد', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(10, 'الإثنين', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(11, 'الثلاثاء', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(12, 'الأربعاء', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(13, 'الخميس', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(14, 'الجمعة', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `days` ENABLE KEYS */;

-- Dumping structure for table ena.descriptions
DROP TABLE IF EXISTS `descriptions`;
CREATE TABLE IF NOT EXISTS `descriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.descriptions: ~10 rows (approximately)
/*!40000 ALTER TABLE `descriptions` DISABLE KEYS */;
INSERT IGNORE INTO `descriptions` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Jordy Paucek DDS', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'Golda Considine Jr.', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'Arvid Stoltenberg', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(4, 'Zachariah Kling', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(5, 'Prof. Camron Fisher', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(6, 'فوزية السليم', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(7, 'باهر وجيه الفدا', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(8, 'تقوى الحصين', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(9, 'المهندسة نعمه العرفج', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(10, 'اسماعيل الراجحي', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `descriptions` ENABLE KEYS */;

-- Dumping structure for table ena.diseases
DROP TABLE IF EXISTS `diseases`;
CREATE TABLE IF NOT EXISTS `diseases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commonname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `specialty_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `diseases_specialty_id_index` (`specialty_id`),
  CONSTRAINT `diseases_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.diseases: ~6 rows (approximately)
/*!40000 ALTER TABLE `diseases` DISABLE KEYS */;
INSERT IGNORE INTO `diseases` (`id`, `code`, `name`, `commonname`, `active`, `specialty_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '543243', 'abdo', 'Lelah Ritchie', 1, 2, '2019-03-10 01:42:21', '2019-03-27 21:55:15', NULL),
	(2, 'Angela 3r', 'Mac Hammes Sr.', 'Prof. Lon Batz I', 1, 2, '2019-03-10 01:42:21', '2019-03-27 23:24:42', NULL),
	(3, 'Kali Rodr', 'Liliane Pagac', 'Zack Schaefer', 1, 2, '2019-03-10 01:42:21', '2019-03-27 23:24:55', NULL),
	(4, 'rKub', 'Mr. Ryann Schmitt', 'Phoebe Schinner', 1, 1, '2019-03-10 01:42:21', '2019-03-27 23:25:07', NULL),
	(5, '33r', 'Cassie Roob', 'Prof. Tara O\'Keefe', 1, 2, '2019-03-10 01:42:21', '2019-03-27 23:25:28', NULL),
	(6, 'rrr', 'Killing Haslhoof', 'كحاحة', 1, 2, '2019-03-11 02:07:49', '2019-03-27 23:25:43', NULL),
	(7, '1221asdf8', 'Killing Haslhoof', 'كحاحة', 1, 2, '2019-03-20 18:54:28', '2019-03-21 17:04:47', '2019-03-21 17:04:47'),
	(8, '345345ertg', 'Scor_Fix4', 'كحاحة', 1, 2, '2019-03-21 15:10:58', '2019-03-21 15:23:00', '2019-03-21 15:23:00');
/*!40000 ALTER TABLE `diseases` ENABLE KEYS */;

-- Dumping structure for table ena.disease_doctor
DROP TABLE IF EXISTS `disease_doctor`;
CREATE TABLE IF NOT EXISTS `disease_doctor` (
  `disease_id` int(10) unsigned NOT NULL,
  `doctor_id` int(10) unsigned NOT NULL,
  KEY `disease_doctor_disease_id_index` (`disease_id`),
  KEY `disease_doctor_doctor_id_index` (`doctor_id`),
  CONSTRAINT `disease_doctor_disease_id_foreign` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`),
  CONSTRAINT `disease_doctor_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.disease_doctor: ~15 rows (approximately)
/*!40000 ALTER TABLE `disease_doctor` DISABLE KEYS */;
INSERT IGNORE INTO `disease_doctor` (`disease_id`, `doctor_id`) VALUES
	(2, 7),
	(1, 7),
	(1, 6),
	(2, 6),
	(3, 6),
	(4, 6),
	(5, 6),
	(6, 6),
	(1, 10),
	(3, 10),
	(5, 10),
	(4, 10),
	(6, 10),
	(2, 1),
	(1, 1);
/*!40000 ALTER TABLE `disease_doctor` ENABLE KEYS */;

-- Dumping structure for table ena.disease_symptom
DROP TABLE IF EXISTS `disease_symptom`;
CREATE TABLE IF NOT EXISTS `disease_symptom` (
  `disease_id` int(10) unsigned NOT NULL,
  `symptom_id` int(10) unsigned NOT NULL,
  KEY `disease_symptom_disease_id_index` (`disease_id`),
  KEY `disease_symptom_symptom_id_index` (`symptom_id`),
  CONSTRAINT `disease_symptom_disease_id_foreign` FOREIGN KEY (`disease_id`) REFERENCES `diseases` (`id`),
  CONSTRAINT `disease_symptom_symptom_id_foreign` FOREIGN KEY (`symptom_id`) REFERENCES `symptoms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.disease_symptom: ~1 rows (approximately)
/*!40000 ALTER TABLE `disease_symptom` DISABLE KEYS */;
INSERT IGNORE INTO `disease_symptom` (`disease_id`, `symptom_id`) VALUES
	(1, 2);
/*!40000 ALTER TABLE `disease_symptom` ENABLE KEYS */;

-- Dumping structure for table ena.doctors
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE IF NOT EXISTS `doctors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dgree` int(11) NOT NULL DEFAULT '1',
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `specialty_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `rate` double(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `doctors_specialty_id_index` (`specialty_id`),
  KEY `doctors_user_id_index` (`user_id`),
  CONSTRAINT `doctors_specialty_id_foreign` FOREIGN KEY (`specialty_id`) REFERENCES `specialties` (`id`),
  CONSTRAINT `doctors_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.doctors: ~10 rows (approximately)
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT IGNORE INTO `doctors` (`id`, `dgree`, `ref_no`, `specialty_id`, `user_id`, `created_at`, `updated_at`, `deleted_at`, `rate`) VALUES
	(1, 1, '12345', 1, 1, '2019-03-10 01:42:21', '2019-03-28 04:33:50', NULL, 5.00),
	(2, 2, '23456', 2, 2, '2019-03-10 01:42:21', '2019-03-28 04:34:13', NULL, 3.00),
	(3, 3, '34567', 3, 3, '2019-03-10 01:42:21', '2019-03-28 04:33:36', NULL, 4.00),
	(4, 5, '45678', 3, 4, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL, 0.00),
	(6, 19, '67890', 5, 15, '2019-03-11 01:25:20', '2019-03-11 01:25:20', NULL, 0.00),
	(7, 19, '78901', 1, 16, '2019-03-14 18:02:42', '2019-03-28 04:58:02', NULL, 5.00),
	(10, 12, '89012', 2, 25, '2019-03-17 14:58:23', '2019-03-17 14:58:23', NULL, 0.00),
	(11, 2, '58745', 2, 27, '2019-03-23 16:22:56', '2019-03-23 16:22:56', NULL, 0.00),
	(12, 7, '555', 2, 28, '2019-03-28 01:33:50', '2019-03-28 01:33:50', NULL, 0.00),
	(13, 5555, '55896311', 2, 31, '2019-03-28 02:41:57', '2019-03-28 02:41:57', NULL, 0.00),
	(14, 5, '88996655', 8, 32, '2019-03-28 03:31:19', '2019-03-28 04:39:20', NULL, 5.00);
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;

-- Dumping structure for table ena.doctor_join_requests
DROP TABLE IF EXISTS `doctor_join_requests`;
CREATE TABLE IF NOT EXISTS `doctor_join_requests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accepted` tinyint(1) DEFAULT NULL,
  `revoked` tinyint(1) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `doctor_join_requests_user_id_index` (`user_id`),
  CONSTRAINT `doctor_join_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.doctor_join_requests: ~5 rows (approximately)
/*!40000 ALTER TABLE `doctor_join_requests` DISABLE KEYS */;
INSERT IGNORE INTO `doctor_join_requests` (`id`, `accepted`, `revoked`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 0, 25, '2019-03-17 14:58:23', '2019-03-21 13:52:08', NULL),
	(2, 1, 0, 27, '2019-03-23 16:22:56', '2019-03-28 03:28:46', NULL),
	(3, NULL, NULL, 28, '2019-03-28 01:33:50', '2019-03-28 01:33:50', NULL),
	(4, NULL, NULL, 31, '2019-03-28 02:41:57', '2019-03-28 02:41:57', NULL),
	(5, 1, 0, 32, '2019-03-28 03:31:19', '2019-03-28 03:40:00', NULL);
/*!40000 ALTER TABLE `doctor_join_requests` ENABLE KEYS */;

-- Dumping structure for table ena.genders
DROP TABLE IF EXISTS `genders`;
CREATE TABLE IF NOT EXISTS `genders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.genders: ~4 rows (approximately)
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT IGNORE INTO `genders` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'ذكر', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'إنثى', '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'ذكر', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(4, 'إنثى', '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;

-- Dumping structure for table ena.mc_conversations
DROP TABLE IF EXISTS `mc_conversations`;
CREATE TABLE IF NOT EXISTS `mc_conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `private` tinyint(1) NOT NULL DEFAULT '1',
  `data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.mc_conversations: ~4 rows (approximately)
/*!40000 ALTER TABLE `mc_conversations` DISABLE KEYS */;
INSERT IGNORE INTO `mc_conversations` (`id`, `private`, `data`, `created_at`, `updated_at`) VALUES
	(24, 1, '[]', '2019-03-21 00:33:19', '2019-03-21 00:34:07'),
	(25, 1, '[]', '2019-03-21 13:21:30', '2019-03-21 13:21:30'),
	(26, 1, '[]', '2019-03-21 13:46:20', '2019-03-21 13:46:37'),
	(27, 1, '[]', '2019-03-21 13:50:27', '2019-03-21 13:50:27'),
	(28, 1, '[]', '2019-03-28 04:48:47', '2019-03-28 04:48:47');
/*!40000 ALTER TABLE `mc_conversations` ENABLE KEYS */;

-- Dumping structure for table ena.mc_conversation_user
DROP TABLE IF EXISTS `mc_conversation_user`;
CREATE TABLE IF NOT EXISTS `mc_conversation_user` (
  `user_id` int(10) unsigned NOT NULL,
  `conversation_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`conversation_id`),
  KEY `mc_conversation_user_conversation_id_foreign` (`conversation_id`),
  CONSTRAINT `mc_conversation_user_conversation_id_foreign` FOREIGN KEY (`conversation_id`) REFERENCES `mc_conversations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mc_conversation_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.mc_conversation_user: ~10 rows (approximately)
/*!40000 ALTER TABLE `mc_conversation_user` DISABLE KEYS */;
INSERT IGNORE INTO `mc_conversation_user` (`user_id`, `conversation_id`, `created_at`, `updated_at`) VALUES
	(1, 26, '2019-03-21 13:46:20', '2019-03-21 13:46:20'),
	(1, 27, '2019-03-21 13:50:27', '2019-03-21 13:50:27'),
	(2, 25, '2019-03-21 13:21:30', '2019-03-21 13:21:30'),
	(2, 27, '2019-03-21 13:50:27', '2019-03-21 13:50:27'),
	(2, 28, '2019-03-28 04:48:47', '2019-03-28 04:48:47'),
	(3, 24, '2019-03-21 00:33:19', '2019-03-21 00:33:19'),
	(25, 24, '2019-03-21 00:33:19', '2019-03-21 00:33:19'),
	(25, 25, '2019-03-21 13:21:30', '2019-03-21 13:21:30'),
	(25, 26, '2019-03-21 13:46:20', '2019-03-21 13:46:20'),
	(32, 28, '2019-03-28 04:48:47', '2019-03-28 04:48:47');
/*!40000 ALTER TABLE `mc_conversation_user` ENABLE KEYS */;

-- Dumping structure for table ena.mc_messages
DROP TABLE IF EXISTS `mc_messages`;
CREATE TABLE IF NOT EXISTS `mc_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mc_messages_user_id_foreign` (`user_id`),
  KEY `mc_messages_conversation_id_foreign` (`conversation_id`),
  CONSTRAINT `mc_messages_conversation_id_foreign` FOREIGN KEY (`conversation_id`) REFERENCES `mc_conversations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mc_messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.mc_messages: ~3 rows (approximately)
/*!40000 ALTER TABLE `mc_messages` DISABLE KEYS */;
INSERT IGNORE INTO `mc_messages` (`id`, `body`, `conversation_id`, `user_id`, `type`, `created_at`, `updated_at`) VALUES
	(29, 'd', 24, 25, 'text', '2019-03-21 00:34:07', '2019-03-21 00:34:07'),
	(30, 'sdf', 26, 25, 'text', '2019-03-21 13:46:25', '2019-03-21 13:46:25'),
	(31, 'public/attachments/NGUiq2qe0hhzik31ktLYmIa2PiyIczTMPQYrTUFx.rtf', 26, 25, 'file', '2019-03-21 13:46:37', '2019-03-21 13:46:37');
/*!40000 ALTER TABLE `mc_messages` ENABLE KEYS */;

-- Dumping structure for table ena.mc_message_notification
DROP TABLE IF EXISTS `mc_message_notification`;
CREATE TABLE IF NOT EXISTS `mc_message_notification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `is_sender` tinyint(1) NOT NULL DEFAULT '0',
  `flagged` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mc_message_notification_user_id_message_id_index` (`user_id`,`message_id`),
  KEY `mc_message_notification_message_id_foreign` (`message_id`),
  KEY `mc_message_notification_conversation_id_foreign` (`conversation_id`),
  CONSTRAINT `mc_message_notification_conversation_id_foreign` FOREIGN KEY (`conversation_id`) REFERENCES `mc_conversations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mc_message_notification_message_id_foreign` FOREIGN KEY (`message_id`) REFERENCES `mc_messages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `mc_message_notification_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.mc_message_notification: ~6 rows (approximately)
/*!40000 ALTER TABLE `mc_message_notification` DISABLE KEYS */;
INSERT IGNORE INTO `mc_message_notification` (`id`, `message_id`, `conversation_id`, `user_id`, `is_seen`, `is_sender`, `flagged`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(54, 29, 24, 3, 0, 0, 0, '2019-03-21 00:34:07', NULL, NULL),
	(55, 29, 24, 25, 1, 1, 0, '2019-03-21 00:34:07', '2019-03-21 13:49:07', NULL),
	(56, 30, 26, 1, 1, 0, 0, '2019-03-21 13:46:25', '2019-03-21 13:50:35', NULL),
	(57, 30, 26, 25, 1, 1, 0, '2019-03-21 13:46:25', '2019-03-21 13:49:09', NULL),
	(58, 31, 26, 1, 1, 0, 0, '2019-03-21 13:46:37', '2019-03-21 13:50:35', NULL),
	(59, 31, 26, 25, 1, 1, 0, '2019-03-21 13:46:37', '2019-03-21 13:49:09', NULL);
/*!40000 ALTER TABLE `mc_message_notification` ENABLE KEYS */;

-- Dumping structure for table ena.media
DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.media: ~2 rows (approximately)
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT IGNORE INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Certificate', 2, 'link', 'Screenshot (27)', 'Screenshot-(27).png', 'image/png', 'public', 49299, '[]', '[]', '[]', 1, '2019-03-20 23:37:53', '2019-03-20 23:37:53'),
	(2, 'App\\Certificate', 3, 'link', 'Screenshot (20)', 'Screenshot-(20).png', 'image/png', 'public', 182063, '[]', '[]', '[]', 2, '2019-03-20 23:41:38', '2019-03-20 23:41:38'),
	(3, 'App\\Certificate', 4, 'link', '37774607_1795189593881026_4443750220173410304_n', '37774607_1795189593881026_4443750220173410304_n.jpg', 'image/jpeg', 'public', 442273, '[]', '[]', '[]', 3, '2019-03-21 13:29:03', '2019-03-21 13:29:03');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;

-- Dumping structure for table ena.messages
DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.messages: ~0 rows (approximately)
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- Dumping structure for table ena.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=259 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.migrations: ~22 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(234, '2014_10_12_000000_create_users_table', 1),
	(235, '2014_10_12_100000_create_password_resets_table', 1),
	(236, '2019_03_05_000000_create_admin_password_resets_table', 1),
	(237, '2019_03_05_000000_create_admins_table', 1),
	(238, '2019_03_05_231537_create_specialties_table', 1),
	(239, '2019_03_05_231538_create_doctors_table', 1),
	(240, '2019_03_05_231539_create_days_table', 1),
	(241, '2019_03_05_231540_create_genders_table', 1),
	(242, '2019_03_05_231542_create_nationalities_table', 1),
	(243, '2019_03_05_231543_create_people_table', 1),
	(244, '2019_03_05_231544_create_certificates_table', 1),
	(245, '2019_03_05_231545_create_descriptions_table', 1),
	(246, '2019_03_05_231546_create_clinks_table', 1),
	(247, '2019_03_05_231547_create_diseases_table', 1),
	(248, '2019_03_05_231548_create_symptoms_table', 1),
	(249, '2019_03_05_231549_create_appointments_table', 1),
	(250, '2019_03_05_231550_create_promotions_table', 1),
	(251, '2019_03_05_231551_create_disease_doctor_table', 1),
	(252, '2019_03_05_231551_create_disease_symptom_table', 1),
	(253, '2019_03_05_234810_create_media_table', 1),
	(254, '2019_03_06_010721_create_chat_tables', 1),
	(255, '2019_03_09_003151_create_messages_table', 1),
	(256, '2019_03_13_203857_create_doctor_join_requests_table', 2),
	(257, '2019_03_19_225920_create_ratings_table', 3),
	(258, '2019_03_27_210933_update_doctors_table', 4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table ena.nationalities
DROP TABLE IF EXISTS `nationalities`;
CREATE TABLE IF NOT EXISTS `nationalities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.nationalities: ~9 rows (approximately)
/*!40000 ALTER TABLE `nationalities` DISABLE KEYS */;
INSERT IGNORE INTO `nationalities` (`id`, `name`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'ليبي', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'مصري', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'تونسي', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(4, 'مغربي', 0, '2019-03-10 01:42:21', '2019-03-13 14:17:19', NULL),
	(5, 'جزا6666', 1, '2019-03-13 14:17:38', '2019-03-13 14:17:47', '2019-03-13 14:17:47'),
	(6, 'ليبي', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(7, 'مصري', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(8, 'تونسي', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(9, 'مغربي', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `nationalities` ENABLE KEYS */;

-- Dumping structure for table ena.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.password_resets: ~1 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table ena.people
DROP TABLE IF EXISTS `people`;
CREATE TABLE IF NOT EXISTS `people` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'ليبيا',
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'طرابلس',
  `date_of_birth` date NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `gender_id` int(10) unsigned NOT NULL,
  `nationality_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `people_user_id_index` (`user_id`),
  KEY `people_gender_id_index` (`gender_id`),
  KEY `people_nationality_id_index` (`nationality_id`),
  CONSTRAINT `people_gender_id_foreign` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `people_nationality_id_foreign` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`id`),
  CONSTRAINT `people_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.people: ~11 rows (approximately)
/*!40000 ALTER TABLE `people` DISABLE KEYS */;
INSERT IGNORE INTO `people` (`id`, `first_name`, `second_name`, `last_name`, `family_name`, `country`, `state`, `date_of_birth`, `user_id`, `gender_id`, `nationality_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Ms. Kristy Altenwerth I', 'Ms. Joannie Brown', 'Alexander Schuppe', 'Mr. Edward Larson', 'ليبيا', 'طرابلس', '1992-02-13', 1, 2, 2, '2019-03-10 01:42:21', '2019-03-21 13:51:12', NULL),
	(2, 'Giuseppe Carroll', 'Mr. Cody Fritsch', 'Daija Lynch', 'Rosie Satterfield', NULL, NULL, '1994-12-24', 2, 1, 4, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'Gracie Turner', 'Dr. Prince Sporer', 'Lavern Schoen', 'Lesly Funk', NULL, NULL, '1992-01-30', 3, 2, 4, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(4, 'Ms. Sydnee Langosh', 'Ernestine Bailey', 'Dr. Enos Beer', 'Lela Watsica', NULL, NULL, '1975-10-03', 4, 1, 1, '2019-03-10 01:42:21', '2019-03-17 10:13:50', NULL),
	(5, 'عبدالرحمن', 'علي', 'محمد', 'المهيدوي', NULL, NULL, '2012-08-14', 5, 1, 4, '2019-03-10 01:42:21', '2019-03-17 10:12:19', NULL),
	(9, 'عبدالرحمن', 'علي', 'محمد', 'المهيدوي', NULL, NULL, '1992-06-20', 15, 1, 1, '2019-03-11 01:25:20', '2019-03-17 14:25:45', NULL),
	(10, 'عبدالرحمن', 'علي', 'محمد', 'المهيداوي', NULL, NULL, '1992-06-20', 16, 1, 1, '2019-03-14 18:02:42', '2019-03-14 18:02:42', NULL),
	(13, 'خليفةنايس', 'مسعود', 'كعبش طاح', 'للاهم', 'ليبيا', 'طرابلس', '1910-04-07', 25, 2, 1, '2019-03-17 14:58:23', '2019-03-23 15:12:59', NULL),
	(15, 'جمعة', 'ولدخليفة', 'ولدمسعود', 'الاب', 'ليبيا', 'طرابلس', '2019-03-20', 27, 1, 1, '2019-03-23 16:22:56', '2019-03-23 16:22:56', NULL),
	(16, 'rdtfyguhi', 'yughi', 'uihykjbl', 'ikb', 'ليبيا', 'طرابلس', '2019-03-20', 28, 1, 1, '2019-03-28 01:33:50', '2019-03-28 01:33:50', NULL),
	(17, 'عبدالرحمن', 'MrCodyFritsch', 'Daija Lynch', 'PaytonFay', 'ly', 'tr', '2019-03-28', 30, 1, 1, '2019-03-28 02:07:36', '2019-03-28 02:07:36', NULL),
	(18, 'hyy', 'yyu', 'uui', 'iio', 'ليبيا', 'طرابلس', '2019-03-19', 31, 1, 2, '2019-03-28 02:41:57', '2019-03-28 02:41:57', NULL),
	(19, 'utyig', 'iugugbkj', 'kjvbjctug', 'ugvjhkb', 'ليبيا', 'طرابلس', '2019-03-20', 32, 1, 3, '2019-03-28 03:31:19', '2019-03-28 03:31:19', NULL);
/*!40000 ALTER TABLE `people` ENABLE KEYS */;

-- Dumping structure for table ena.promotions
DROP TABLE IF EXISTS `promotions`;
CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isaccepted` tinyint(1) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promotions_person_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.promotions: ~2 rows (approximately)
/*!40000 ALTER TABLE `promotions` DISABLE KEYS */;
INSERT IGNORE INTO `promotions` (`id`, `isaccepted`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(2, 1, 1, '2019-03-20 23:39:34', '2019-03-20 22:09:07', NULL),
	(3, 1, 2, '2019-03-20 23:39:41', '2019-03-20 22:09:09', NULL),
	(5, 0, 1, '2019-03-21 14:05:05', '2019-03-21 14:08:21', NULL);
/*!40000 ALTER TABLE `promotions` ENABLE KEYS */;

-- Dumping structure for table ena.ratings
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE IF NOT EXISTS `ratings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `rateable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rateable_id` bigint(20) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ratings_rateable_type_rateable_id_index` (`rateable_type`,`rateable_id`),
  KEY `ratings_rateable_id_index` (`rateable_id`),
  KEY `ratings_rateable_type_index` (`rateable_type`),
  KEY `ratings_user_id_index` (`user_id`),
  CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.ratings: ~42 rows (approximately)
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;
INSERT IGNORE INTO `ratings` (`id`, `created_at`, `updated_at`, `rating`, `rateable_type`, `rateable_id`, `user_id`) VALUES
	(1, '2019-03-19 23:08:48', '2019-03-19 23:08:48', 5, 'App\\Doctor', 3, 3),
	(2, '2019-03-19 23:09:49', '2019-03-19 23:09:49', 5, 'App\\Doctor', 3, 3),
	(3, '2019-03-19 23:10:00', '2019-03-19 23:10:00', 5, 'App\\Doctor', 3, 3),
	(4, '2019-03-19 23:10:14', '2019-03-19 23:10:14', 5, 'App\\Doctor', 4, 4),
	(5, '2019-03-19 23:10:18', '2019-03-19 23:10:18', 5, 'App\\Doctor', 5, 5),
	(6, '2019-03-19 23:10:24', '2019-03-19 23:10:24', 5, 'App\\Doctor', 6, 15),
	(7, '2019-03-19 23:10:30', '2019-03-23 14:24:39', 2, 'App\\Doctor', 10, 25),
	(8, '2019-03-19 23:10:43', '2019-03-19 23:10:43', 5, 'App\\Doctor', 1, 1),
	(9, '2019-03-19 23:21:40', '2019-03-19 23:21:40', 5, 'App\\Doctor', 1, 1),
	(10, '2019-03-19 23:21:55', '2019-03-19 23:21:55', 5, 'App\\Doctor', 1, 1),
	(11, '2019-03-19 23:23:13', '2019-03-19 23:23:13', 5, 'App\\Doctor', 1, 1),
	(12, '2019-03-19 23:23:32', '2019-03-19 23:23:32', 5, 'App\\Doctor', 1, 1),
	(13, '2019-03-19 23:27:48', '2019-03-19 23:27:48', 5, 'App\\Doctor', 1, 1),
	(14, '2019-03-19 23:28:10', '2019-03-19 23:28:10', 5, 'App\\Doctor', 1, 1),
	(15, '2019-03-19 23:28:15', '2019-03-19 23:28:15', 5, 'App\\Doctor', 1, 1),
	(16, '2019-03-19 23:28:16', '2019-03-19 23:28:16', 5, 'App\\Doctor', 1, 1),
	(17, '2019-03-19 23:28:16', '2019-03-19 23:28:16', 5, 'App\\Doctor', 1, 1),
	(18, '2019-03-19 23:28:16', '2019-03-19 23:28:16', 5, 'App\\Doctor', 1, 1),
	(19, '2019-03-19 23:28:17', '2019-03-19 23:28:17', 5, 'App\\Doctor', 1, 1),
	(20, '2019-03-19 23:28:17', '2019-03-19 23:28:17', 5, 'App\\Doctor', 1, 1),
	(21, '2019-03-19 23:29:42', '2019-03-19 23:29:42', 5, 'App\\Doctor', 1, 1),
	(22, '2019-03-19 23:30:51', '2019-03-19 23:30:51', 5, 'App\\Doctor', 6, 15),
	(131, '2019-03-20 01:30:35', '2019-03-20 01:30:35', 5, 'App\\Doctor', 10, 25),
	(138, '2019-03-20 01:34:47', '2019-03-20 01:34:47', 5, 'App\\Doctor', 1, 1),
	(139, '2019-03-20 01:34:50', '2019-03-20 01:34:50', 5, 'App\\Doctor', 1, 1),
	(140, '2019-03-20 01:35:02', '2019-03-20 01:35:02', 5, 'App\\Doctor', 1, 1),
	(141, '2019-03-20 01:35:02', '2019-03-20 01:35:02', 5, 'App\\Doctor', 1, 1),
	(142, '2019-03-20 01:36:45', '2019-03-20 01:36:45', 5, 'App\\Doctor', 1, 1),
	(143, '2019-03-20 01:38:38', '2019-03-20 01:38:38', 5, 'App\\Doctor', 10, 25),
	(174, '2019-03-20 02:10:48', '2019-03-20 02:14:55', 3, 'App\\Doctor', 3, 25),
	(175, '2019-03-20 02:24:49', '2019-03-21 13:24:24', 5, 'App\\Doctor', 6, 25),
	(176, '2019-03-21 13:23:30', '2019-03-21 13:23:42', 2, 'App\\Doctor', 1, 25),
	(177, '2019-03-23 16:22:56', '2019-03-23 16:22:56', 5, 'App\\Doctor', 11, 27),
	(178, '2019-03-28 01:33:50', '2019-03-28 01:33:50', 5, 'App\\Doctor', 12, 28),
	(179, '2019-03-28 02:29:11', '2019-03-28 02:29:11', 5, 'App\\Doctor', 1, 30),
	(180, '2019-03-28 02:31:33', '2019-03-28 02:32:38', 4, 'App\\Doctor', 3, 30),
	(181, '2019-03-28 02:41:57', '2019-03-28 02:41:57', 5, 'App\\Doctor', 13, 31),
	(182, '2019-03-28 03:31:19', '2019-03-28 04:39:20', 5, 'App\\Doctor', 14, 32),
	(183, '2019-03-28 04:33:36', '2019-03-28 04:33:36', 4, 'App\\Doctor', 3, 32),
	(184, '2019-03-28 04:33:50', '2019-03-28 04:33:50', 5, 'App\\Doctor', 1, 32),
	(185, '2019-03-28 04:34:13', '2019-03-28 04:34:13', 3, 'App\\Doctor', 2, 32),
	(186, '2019-03-28 04:58:02', '2019-03-28 04:58:02', 5, 'App\\Doctor', 7, 32);
/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;

-- Dumping structure for table ena.specialties
DROP TABLE IF EXISTS `specialties`;
CREATE TABLE IF NOT EXISTS `specialties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namear` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nameen` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.specialties: ~12 rows (approximately)
/*!40000 ALTER TABLE `specialties` DISABLE KEYS */;
INSERT IGNORE INTO `specialties` (`id`, `namear`, `nameen`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Mill Gibson', 'Dr. Carlie Mann IV', 1, '2019-03-10 01:42:21', '2019-03-21 00:23:52', NULL),
	(2, 'Dr. Uriah Corkery', 'Alexander Daniel', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(3, 'Stephany Lesch', 'Dorthy Ratke', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(5, 'Cristopher Star', 'Hoyt Jaskolsk', 1, '2019-03-10 01:42:21', '2019-03-13 14:09:02', NULL),
	(8, 'انف اذن وحنجرة', 'Nosal', 1, '2019-03-21 00:23:14', '2019-03-21 00:23:14', NULL),
	(9, 'صدرية1', 'chest', 1, '2019-03-21 00:23:35', '2019-03-21 13:31:02', NULL),
	(10, 'انف اذن وحنجرة', 'Cuagh', 1, '2019-03-21 13:30:43', '2019-03-21 13:30:54', '2019-03-21 13:30:54'),
	(11, 'الدكتورة روان الحنتوشي', 'نانسي العسكر', 0, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(12, 'هازار رافع الفيفي', 'راشد فؤاد الأحمري', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(13, 'ريان عبد العفو علي السعيد', 'السيدة حياة المطرفي', 0, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(14, 'رزق حمزة ساهر الحسين', 'نانسي الداوود', 1, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL),
	(15, 'الآنسة ملاك الفيفي', 'صدام نورس السماعيل', 0, '2019-03-28 05:03:23', '2019-03-28 05:03:23', NULL);
/*!40000 ALTER TABLE `specialties` ENABLE KEYS */;

-- Dumping structure for table ena.student
DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `Stu_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Stu_FName` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `Stu_LName` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `Stu_Spe` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `Stu_add` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Stu_ID`),
  UNIQUE KEY `Email` (`Email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table ena.student: ~0 rows (approximately)
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT IGNORE INTO `student` (`Stu_ID`, `Stu_FName`, `Stu_LName`, `Email`, `Stu_Spe`, `Stu_add`, `type`) VALUES
	(1, 'esar', 'esar', 'u@yahoo.com', 'e', 'طرابلس', 'أنتي');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;

-- Dumping structure for table ena.symptoms
DROP TABLE IF EXISTS `symptoms`;
CREATE TABLE IF NOT EXISTS `symptoms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commonname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.symptoms: ~10 rows (approximately)
/*!40000 ALTER TABLE `symptoms` DISABLE KEYS */;
INSERT IGNORE INTO `symptoms` (`id`, `name`, `commonname`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'molestias', 'molestias', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(2, 'facilis', 'enim', 1, '2019-03-10 01:42:21', '2019-03-27 23:38:46', NULL),
	(3, 'alias', 'est', 1, '2019-03-10 01:42:21', '2019-03-21 15:58:06', NULL),
	(4, 'quae', 'aut', 1, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(5, 'alias', 'tempore', 0, '2019-03-10 01:42:21', '2019-03-10 01:42:21', NULL),
	(6, 'gggggg', 'كحاحة', 1, '2019-03-13 14:12:17', '2019-03-13 14:12:17', NULL),
	(7, 'GOGOGOG', 'كحاحة', 0, '2019-03-13 14:12:54', '2019-03-13 14:13:46', NULL),
	(8, 'GOGOGOG', 'كحاحة', 1, '2019-03-21 00:03:30', '2019-03-21 13:31:51', '2019-03-21 13:31:51'),
	(9, 'زكمة', 'كحاحة', 1, '2019-03-21 13:31:44', '2019-03-21 13:31:44', NULL),
	(10, 'adsf', 'dfhgdfgasdf', 1, '2019-03-21 15:26:10', '2019-03-21 15:26:15', '2019-03-21 15:26:15');
/*!40000 ALTER TABLE `symptoms` ENABLE KEYS */;

-- Dumping structure for table ena.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(10) DEFAULT NULL,
  `isAdmin` bit(1) DEFAULT b'0',
  `isDoctor` bit(1) DEFAULT b'0',
  `active` bit(1) DEFAULT b'0',
  `avatar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'avatar.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ena.users: ~13 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `type_id`, `isAdmin`, `isDoctor`, `active`, `avatar`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Ms. Lacy Murphy Jr.', 'kaya67@example.org', 1, b'0', b'1', b'1', '7.png', '2019-03-10 01:42:21', '$2y$10$j6TGHoVOBPuOfkYn2C9/Qe8sHJMMUNeZTTJrCbVR7uj9KGYdYtL72', '7Vhuwa15pd9XIYjEZzJ4r9uSB0JFB9e66enGxYcDAFnopNgnNaYd4gl3bOYV', '2019-03-10 01:42:21', '2019-03-20 21:59:21', NULL),
	(2, 'Miss Lysanne Ondricka', 'sven.baumbach@example.org', 2, b'0', b'1', b'1', '6.png', '2019-03-10 01:42:21', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '1Zln3Necx7', '2019-03-10 01:42:21', '2019-03-20 21:59:18', NULL),
	(3, 'Esta Roberts', 'dean08@example.net', 2, b'0', b'1', b'1', '5.png', '2019-03-10 01:42:21', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7DgHOtpNTJ', '2019-03-10 01:42:21', '2019-03-20 18:49:37', NULL),
	(4, 'Mr. Isac Rosenbaum', 'vkrajcik@example.net', 2, b'0', b'1', b'1', '4.png', '2019-03-10 01:42:21', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YOtfukcpcT', '2019-03-10 01:42:21', '2019-03-20 18:49:34', NULL),
	(5, 'Gia Mante', 'qdamore@example.org', 2, b'0', b'0', b'1', '3.png', '2019-03-10 01:42:21', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'JcJrgcGaQL', '2019-03-10 01:42:21', '2019-03-17 10:27:42', NULL),
	(15, 'عبدالرحمن', 'xile.meko@gmail.com', 2, b'0', b'1', b'1', '8.png', NULL, '$2y$10$OeIrh.TDLLxw4YWW7ZM7Re8YFFSRaaLMMAcqIj3HubOdLrF2W2Rfy', 'w6fyCswGfDVStm4YvxNwJl70zSUkiWQW852FaaNDYkKddErV7cqIb92dOL8R', '2019-03-11 01:25:20', '2019-03-27 21:47:54', NULL),
	(16, 'we', 'admin@1argon.com', 1, b'0', b'1', b'1', '9.png', '2019-03-23 18:18:51', '$2y$10$iRSB4p7tz7Mk/5wE8HnmTO5gqXC0zNBpW0SQf4u9xEdR9BY58lQP6', NULL, '2019-03-12 19:12:48', '2019-03-12 19:12:48', NULL),
	(25, 'مسعود', 'masood@yaho.com', 2, b'1', b'1', b'1', '1553175450.jpg', '2019-03-23 18:18:49', '$2y$10$j6TGHoVOBPuOfkYn2C9/Qe8sHJMMUNeZTTJrCbVR7uj9KGYdYtL72', '352Pb9e3RsQuUUMFN0iwtZXroxHs7zjbYDI1uarSOAj5aANjRTDl6BqgbAw4', '2019-03-17 14:58:23', '2019-03-23 14:48:48', NULL),
	(27, 'جمعةdrtjrfgj', 'masood5@yaho.com', 1, b'0', b'0', b'1', '1553364256.jpg', '2019-03-23 17:43:27', '$2y$10$vb6Eloi7DR7ZZSw0JLOBDOA3Gqp0pm2oJwTzIdeX4qUzY.lgIVFhu', 'O07b5g9ayNpfG3lS8MH6NWwpxuogsUFe89h2lOUHgnX7vJS9nBRyaFevHxYM', '2019-03-23 16:22:56', '2019-03-28 03:28:46', NULL),
	(28, 'rdtfyguhi', 'fooo@gg.com', 2, b'0', b'1', b'1', 'avatar.png', '2019-03-28 02:14:11', '$2y$10$w6FXB.FHPQAcg0.iG9pUfOJZzERAuWpm.HYi0OpKaRFVpl58Kutyq', NULL, '2019-03-28 01:33:50', '2019-03-28 02:14:11', NULL),
	(30, 'عبدالرحمن', 'fok@g.com', 2, b'0', b'0', b'1', '1553740065.png', '2019-03-28 02:13:43', '$2y$10$o9RE7eb8vm711zlE7mQ7fOcVEA35HbIMbyk7k0K29YzetcSoBKY3u', 'Tzo6pLxwyilPG97E43MNEfamsgRT5zlW1TQtBMCSTQ31Gr2rdbSDxqUjolER', '2019-03-28 02:07:36', '2019-03-28 02:27:45', NULL),
	(31, 'hyy', 'fok1@g.com', 2, b'0', b'1', b'1', 'avatar.png', '2019-03-28 02:42:59', '$2y$10$1f1GXiz0i0j0hwIOa8XnEeqzK61QCvB1pxyEfsv07aUOGFXdylw5u', 'XfTCM9mGldz2HCEA1vnhB6rZDKc7PvpZhzebmQaGs3ICYU97Gs0tV7Tnde6H', '2019-03-28 02:41:57', '2019-03-28 02:42:59', NULL),
	(32, 'احمد', 'fok12@g.com', 2, b'0', b'1', b'1', 'avatar.png', '2019-03-28 03:39:14', '$2y$10$CTI9BbJxzxC8m2grLYrvee25yOjPQQEOmI66jmC0rYeQgAhM2SBjO', 'a2zyS5nIMjJv61fuhU0z4r7UU7xjmrv253rMwhECrvSqKJbmv0vg6zNsnX0N', '2019-03-28 03:31:19', '2019-03-28 03:40:00', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
