<?php

use App\Specialty;
use App\Disease;

// /*
// |--------------------------------------------------------------------------
// | Web Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register web routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | contains the "web" middleware group. Now create something great!
// |
// */
 Route::get('/', 'HomeController@land')->name('land');
 Route::name('dashboard.')->prefix('dashboard')->group(function () {
     Route::group(['middleware' => 'auth:web'], function () {
            Route::get('/', 'DashoardController@dashboard')->name('index');
            Route::resource('requests', 'SpecialtyController');
            Route::resource('specialties', 'SpecialtyController');
            Route::resource('diseases', 'DiseaseController');
            Route::resource('certificates', 'CertificateController');
            Route::resource('appointments', 'AppointmentController');            
            Route::resource('symptoms', 'SymptomController');
            Route::resource('promotions', 'PromotionController');

    });
 });

Route::get('doctors/register', 'DoctorController@registeration')->name('doc.register');
Route::post('/result/{specialty}', 'SearchController@result')->name('result');

Route::get('/result_doctors', 'SearchController@result_doctors')->name('result_doctors');
Route::get('/result/{specialty}', 'SearchController@getSymptoms')->name('getSymptoms');
Route::get('promotion/create/{doctor}', 'DoctorController@promote')->name('doc.promote');
 Auth::routes(['verify'=>true]);

 Route::get('/home',  'HomeController@search')->name('home');

Route::get('/profile', 'HomeController@profile')->name('profile');

Route::get('/verify', 'HomeController@verify')->name('verify');
Route::get('/verify/send/', 'HomeController@send')->name('verify.send');

Route::post('/profile', 'ProfileController@profileUpdate')->name('user.profile_update');
Route::post('/password', 'ProfileController@passwordUpdate')->name('user.password_update');

Route::get('/search', 'HomeController@search')->name('search');
Route::get('doctors/all', 'DoctorController@index')->name('doc.all');
Route::get('doctors/{user?}', 'DoctorController@show')->name('doc.show');
Route::post('doctor/{doctor}/rate', 'DoctorController@rateDoc')->name('doc.rate');
Route::put('doctor/{person}', 'DoctorController@update')->name('doctor.update');
Route::put('doctor/{person}/diseases', 'DoctorController@editDisease')->name('doc.update');
  Route::post('user/register', 'MainController@user_store')->name('user.register');
     
Route::post('doctors/register', 'DoctorController@store')->name('doc.store');

 Route::name('chat.')->prefix('chats')->group(function () {
    Route::get('/', 'ChatController@index');
     Route::get('/{user}', 'ChatController@show');
    Route::post('/{user}', 'ChatController@send')->name('send');
 });


// Admin routes
Route::name('admin.')->prefix('admin')->namespace('Admin')->group(function () {
    Route::namespace('Auth')->middleware('guest:admin')->group(function () {
        Route::get('/', 'LoginController@showLoginForm')->name('login');
        Route::post('/', 'LoginController@login');
        Route::get('forgot-password', 'ForgotPasswordController@showLinkRequestForm')->name('forgot_password');
        Route::post('forgot-password', 'ForgotPasswordController@sendResetLinkEmail');
        Route::get('password/reset/{token}/{email?}', 'ResetPasswordController@showResetForm')->name('reset_password_link');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('reset_password');
    });
    Route::group(['middleware' => 'auth:admin'], function () {
        // Dashboard
        Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');

        Route::resource('specialties', 'SpecialtyController');
        Route::resource('admins', 'AdminsController');
        Route::resource('admins', 'AdminsController');
        Route::get('admins/{admin}/activation', 'AdminsController@activation')->name('admins.activation');

        Route::get('blacklist', 'BlacklistController@index')->name('blacklist.all');
        Route::get('blacklist/{user}/add', 'BlacklistController@add')->name('blacklist.add');
        Route::get('blacklist/{user}/remove', 'BlacklistController@remove')->name('blacklist.remove');

        Route::resource('diseases', 'DiseaseController');
        
        Route::resource('symptoms', 'SymptomController');

        Route::resource('people', 'PersonController');

        Route::resource('nationalities', 'NationalityController');

        Route::resource('doctors', 'DoctorController');

        Route::resource('days', 'DayController');

        Route::resource('genders', 'GenderController');

        Route::resource('descriptions', 'DescriptionController');

        Route::get('requests', 'DoctorJoinRequestController@index')->name('requests.index');
        Route::get('requests/{jrequest}/revoke', 'DoctorJoinRequestController@revoke')->name('requests.revoke');
        Route::get('requests/{jrequest}/approve', 'DoctorJoinRequestController@approve')->name('requests.approve');

        Route::resource('clinks', 'ClinkController');

        Route::resource('privlages', 'privlageController');
      
        // Profile
        Route::get('/profile', 'AdminController@profile')->name('profile');
        Route::post('/profile', 'AdminController@profileUpdate');
        Route::post('/password', 'AdminController@passwordUpdate')->name('password_update');

        // Logout
        Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    });
});

