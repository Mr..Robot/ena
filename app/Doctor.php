<?php

namespace App;
use willvincent\Rateable\Rateable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Doctor extends Model
{
    use SoftDeletes, Rateable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dgree', 'ref_no', 'specialty_id'
    ];

    protected $appends = ['specialtyname','rated', 'fullname'];

    public function getSpecialtynameAttribute()
    {
        return ;//$this->specialty;//->namear;  
    }

        public function getRatedAttribute()
    {
        return $this->averageRating();
    }

    public function getFullnameAttribute()
    {
        return ;// $this->user->person;//->first_name. ' ' . $this->user->person->family_name;
    }

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'ref_no' => 'required|string',
            'specialty_id' => 'required|numeric|exists:specialties,id',
            'diseases' => 'required|array',
            'diseases.*' => 'required|numeric|exists:diseases,id',
        ];
    }

    /**
     * Get the specialty for the Doctor.
     */
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

    /**
     * Get the certificates for the Doctor.
     */
    public function certificates()
    {
        return $this->hasMany('App\Certificate');
    }

    /**
     * Get the appointments for the Doctor.
     */
    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the diseases for the Doctor.
     */
    public function diseases()
    {
        return $this->belongsToMany('App\Disease');
    }
       public function promotions()
    {
        return $this->hasMany('App\Promotion');
    }
    
    public function promote()
    {
        if($this->degree == 1){
            $this->degree =2;
        }

    }
    
    public static function onlyActive()
    {
        return static::where('active',true)->get();
    }
    

    /**
     * Get the diseases for the Doctor.
     */
    public function description()
    {
        switch ($this->degree) {
            case 1:
              return 'أخصائي';
            break;
            case 2:
               return 'إستشاري';
            break;
            default:
                 return 'أخصائي';
            break;
        }
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['specialty'])->paginate(10);
    }
}
