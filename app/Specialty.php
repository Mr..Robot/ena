<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Specialty extends Model
{
    use SoftDeletes;

    // public static $spec = null;
    // public static $namear  = null;
    // public static $nameen = null;
    // public static $active = null;

    // public function __construct()
    // {
    //     $spec = $this;
    // }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'namear', 'nameen', 'active'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules($id = null)
    {
        return [
            'namear' => 'required|string|unique:specialties,namear,' . $id,
            'nameen' => 'required|string|unique:specialties,nameen,' . $id,
            'active' => 'required|boolean',
        ];
    }

    /**
     * Get the diseases for the Specialty.
     */
    public function diseases()
    {
        return $this->hasMany('App\Disease');
    }

    /**
     * Get the doctors for the Specialty.
     */
    public function doctors()
    {
        return $this->hasMany('App\Doctor');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::paginate(10);
    }
    public static function onlyActive()
    {
        return static::where('active',true)->get();
    }
}
