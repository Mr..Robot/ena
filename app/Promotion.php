<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'isaccepted','user_id',
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'isaccepted' => 'boolean',
            'user_id' => 'numeric|exists:people,id',
            'certificate_id' => 'numeric|exists:certificates,id',
            'description_id' => 'numeric|exists:descriptions,id',
        ];
    }

    /**
     * Get the person for the Promotion.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['user'])->paginate(10);
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public function approve()
    {
        $this->isaccepted = true;
        return $this; 
    }
    public function revoke()
    {
        $this->isaccepted = false;
        return $this;
    }
}
