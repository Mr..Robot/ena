<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Symptom extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'commonname', 'active'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules($id = null)
    {
        return [
            'name' => 'required|string|regex:/^[\pL\s\-]+$/u|unique:symptoms,name,' . $id,
            'commonname' => 'required|string|regex:/^[\pL\s\-]+$/u',
            'active' => 'required|boolean',
        ];
    }

    /**
     * Get the diseases for the Symptom.
     */
    public function diseases()
    {
        return $this->belongsToMany('App\Disease');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::paginate(10);
    }
    public static function onlyActive()
    {
        return static::where('active',true)->get();
    }
}
