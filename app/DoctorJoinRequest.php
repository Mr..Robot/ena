<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorJoinRequest extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'accepted','revoked', 'user_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
 
    /**
     * Get the user for the Promotion.
     */
    public function user()
    {
        return $this->belongsTo('App\user');
    }

    /**
     * Get the certificates for the Doctor.
     */
    public function certificate()
    {
        return $this->hasOne('App\Certificate');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public function approve()
    {
        $this->accepted = true;
        $this->revoked = false;
        return $this; 
    }
    public function revoke()
    {
        $this->accepted = false;
        $this->revoked = true;
        return $this;
    }
    public static function getList()
    {
        return static::with(['user.doctor', 'user.person','certificate'])->paginate(10);
    }


}