<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disease extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'commonname', 'active', 'specialty_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules($id = null)
    {
        return [
            'code' => 'required|string|max:10|unique:diseases,code,' .$id,
            'name' => 'required|string|unique:diseases,name,' .$id,
            'commonname' => 'nullable|string',
            'active' => 'required|boolean',
            'specialty_id' => 'required|numeric|exists:specialties,id',
            'symptoms' => 'array',
            'symptoms.*' => 'numeric|exists:symptoms,id',
        ];
    }

    /**
     * Get the specialty for the Disease.
     */
    public function specialty()
    {
        return $this->belongsTo('App\Specialty');
    }

    /**
     * Get the symptoms for the Disease.
     */
    public function symptoms()
    {
        return $this->belongsToMany('App\Symptom');
    }

    /**
     * Get the doctors for the Disease.
     */
    public function doctors()
    {
        return $this->belongsToMany('App\Doctor');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['specialty'])->paginate(10);
    }
    public static function onlyActive()
    {
        return static::where('active',true)->get();
    }
}
