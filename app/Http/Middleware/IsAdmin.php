<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $redirectToRoute = null)
    {
        if (! $request->user()->IsAdmin()) {
        return $request->expectsJson()
                ? abort(403, 'You are not admin.')
                : Redirect::route($redirectToRoute ?: 'home');
    }
        return $next($request);
    }
}
