<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class ForceLogout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('web')->check())
        {
            $user = Auth::user();
            if (is_null($user->email_verified_at) ) 
            {
                Auth::logout();
                return redirect()->route('verify');
            }else
            {
                if(!$user->active)
                {
                    if($user->isDoctor)
                    { 
                        Auth::logout();
                        return redirect()->route('login')->with(['type' => 'error', 'message' => 'الرجاء الانتظار حتى يقوم الادمن بقبول إشتراكك']);;
                    }else {
                        $user->active = 1;
                        $user->save();
                        }
                    }
                }
            }
                return $next($request);
        }
    }
        // elseif(Auth::guard('admin')->check())
        // {
        //     return 2;
        // }
        // else
        // {
        //     return 3;
        // }
     
    // if(!Auth::guard('admin'))
       
        // // You might want to create a method on your model to
        // // prevent direct access to the `logout` property. Something
        // // like `markedForLogout()` maybe.
        // if (!$user->active) {
        //     // Not for the next time!
        //     // Maybe a `unmarkForLogout()` method is appropriate here.
        //     // Log her out
        //     Auth::logout();

        //     return redirect()->route('login');
        // }

        // return $next($request);