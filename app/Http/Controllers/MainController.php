<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nationality;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Doctor;
use App\Person;
use Carbon\Carbon;
use App\Specialty;

class MainController extends Controller
{
// public function __construct()
// {
//    $this->middleware('guest')
// }

   


    public function user_store(Request $request)
    {

        $rules = [
            'first_name' => 'required|string|min:2|max:50|alpha',
            // 'second_name' => 'required|string|min:2|max:50|alpha',
            // 'last_name' => 'required|string|min:2|max:50|alpha',
            'family_name' => 'required|string|min:2|max:50|alpha',
            'gender' => 'required|integer|min:0',
            'nationality_id' => 'min:1|required|integer|exists:nationalities,id',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];

      
        $messages = [
            'gender.required' => 'الرجاء أختيار الجنس',
            'gender.min' => 'الرجاء أختيار الجنس',
            'nationality_id.min' => 'الرجاء أختيار الجنسية'

        ];

       $this->validate($request, $rules,$messages);


       $user = new User;
       $user->email = $request->email;
       $user->password = bcrypt($request->get('password'));
       $user->name = $request->first_name;
       $user->type_id = 2;
       $user->isDoctor = 1;
     //  $user->person_id = $person->id;
       $user->active = 0;
     //  $user->avatar  =  $fileNameToStore;

       $user->save();

        $person = new Person;
        $person->first_name = $request->first_name;
        $person->second_name = $request->second_name;
        $person->last_name = $request->last_name;
        $person->family_name = $request->family_name;
        $person->country = $request->country;
        $person->state = $request->state;
        $person->date_of_birth = Carbon::parse($request->date_of_birth)->format('Y-m-d');
        $person->gender_id = $request->gender;
        $person->nationality_id = $request->nationality_id;
        $person->user_id = $user->id;


        $person->save();

        return redirect()->route('verify');
    }
}
