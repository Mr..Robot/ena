<?php

namespace App\Http\Controllers;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

        /**
     * Updates user profile details
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate()
    { // dd(request());
        $validatedData = request()->validate(User::profileValidationRules());

       // dd($validatedData);
        $user = Auth::User();

        $user->update($validatedData);

        return back()->with(['type' => 'success', 'message' => 'تم تحديث الملف الشخصي بنجاح']);
    }

    /**
     * Updates user password
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function passwordUpdate()
    {
        $rules = [
            'password' => 'required|confirmed',
        ];

      
        $messages = [
            'password.required' => 'الرجاء ادخال كلمة المرور الجديدة',
            'password.confirmed' => 'الرجاء تأكيد كلمة المرور'
        ];

       $this->validate($request, $rules,$messages);

        $validatedData = request()->validate(User::passwordValidationRules());

        $user = Auth::User();

        if (Hash::check(request('current_password'), $user->password)) {
            $user->password = bcrypt(request('password'));
            $user->save();

            return back()->with(['type' => 'success', 'message' => 'تم تحديث كلمة المرور بنجاح']);
        }

        return back()->with(['type' => 'error', 'message' => 'كلمة المرور الحالية غير صحيحة']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );

    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();
    	}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
