<?php

namespace App\Http\Controllers;
use Auth;
use App\Disease;
use App\Specialty;
use App\Symptom;
use App\Http\Controllers\Controller;

class DiseaseController extends Controller
{
    //     public function __Construct()
    // {
    //     request()->request->add(['doctor_id'=>Auth::user()->doctor->id]);
    // }
    /**
     * Display a list of Diseases.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diseases = Disease::getList();

        return view('dashboard.diseases.index', compact('diseases'));
    }

    /**
     * Show the form for creating a new Disease
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specialties = Specialty::all();
        $symptoms = Symptom::all();

        return view('dashboard.diseases.add', compact('specialties', 'symptoms'));
    }

    /**
     * Save new Disease
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Disease::validationRules());

        unset($validatedData['symptoms']);
        $disease = Disease::create($validatedData);

        $disease->symptoms()->sync(request('symptoms'));

        return redirect()->route('dashboard.diseases.index')->with([
            'type' => 'success',
            'message' => 'تم اضافة المرض'
        ]);
    }

    /**
     * Show the form for editing the specified Disease
     *
     * @param \App\Disease $disease
     * @return \Illuminate\Http\Response
     */
    public function edit(Disease $disease)
    {
        $specialties = Specialty::all();
        $symptoms = Symptom::all();

        $disease->symptoms = $disease->symptoms->pluck('id')->toArray();

        return view('dashboard.diseases.edit', compact('disease', 'specialties', 'symptoms'));
    }

    /**
     * Update the Disease
     *
     * @param \App\Disease $disease
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Disease $disease)
    {
        $messages = [
            'code.max' => 'حقل الرمز يجب ان لا يتجاوز 10 خانات',
        ];

       // dd($messages);
        $validatedData = request()->validate(Disease::validationRules($disease->id), $messages);


        unset($validatedData['symptoms']);
        $disease->update($validatedData);

        $disease->symptoms()->sync(request('symptoms'));

        return redirect()->route('dashboard.diseases.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل المرض'
        ]);
    }

    /**
     * Delete the Disease
     *
     * @param \App\Disease $disease
     * @return void
     */
    public function destroy(Disease $disease)
    {
        if ($disease->symptoms()->count() || $disease->doctors()->count()) {
            return redirect()->route('dashboard.diseases.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف المرض بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $disease->delete();

        return redirect()->route('dashboard.diseases.index')->with([
            'type' => 'success',
            'message' => 'تم حذف المرض'
        ]);
    }
}
