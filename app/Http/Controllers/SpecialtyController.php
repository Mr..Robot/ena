<?php

namespace App\Http\Controllers;
use Auth;
use App\Specialty;
use App\Http\Controllers\Controller;

class SpecialtyController extends Controller
{

    //     public function __Construct()
    // {
    //     request()->request->add(['doctor_id'=>Auth::user()->doctor->id]);
    // }

    /**
     * Display a list of Specialties.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialties = Specialty::getList();

        return view('dashboard.specialties.index', compact('specialties'));
    }

    /**
     * Show the form for creating a new Specialty
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.specialties.add');
    }

    /**
     * Save new Specialty
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
      //  dd(request()->all());
        $validatedData = request()->validate(Specialty::validationRules());

        $specialty = Specialty::create($validatedData);

        return redirect()->route('dashboard.specialties.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة تخصص'
        ]);
    }

    /**
     * Show the form for editing the specified Specialty
     *
     * @param \App\Specialty $specialty
     * @return \Illuminate\Http\Response
     */
    public function edit(Specialty $specialty)
    {
        return view('dashboard.specialties.edit', compact('specialty'));
    }

    /**
     * Update the Specialty
     *
     * @param \App\Specialty $specialty
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Specialty $specialty)
    {
        $validatedData = request()->validate(
            Specialty::validationRules($specialty->id)
        );

        $specialty->update($validatedData);

        return redirect()->route('dashboard.specialties.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل التخصص'
        ]);
    }

    /**
     * Delete the Specialty
     *
     * @param \App\Specialty $specialty
     * @return void
     */
    public function destroy(Specialty $specialty)
    {
        if ($specialty->diseases()->count() || $specialty->doctors()->count()) {
            return redirect()->route('dashboard.specialties.index')->with([
                'type' => 'error',
                'message' => 'لا يمكن حذف هذا السجل نظرًا لوجود العلاقات.'
            ]);
        }

        $specialty->delete();

        return redirect()->route('dashboard.specialties.index')->with([
            'type' => 'success',
            'message' => 'تم حذف التخصص بنجاح'
        ]);
    }
}
