<?php

namespace App\Http\Controllers;
use App\User;
use Auth;

use App\Gender;
use App\Nationality;
use App\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // /**
    //  * Create a new controller instance.
    //  *
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function land()
    {
        return view('main');
    }
 
    public function search()
    {
        return view('search');
    }
    // public function profile()
    // {
    //     return view('search');
    // }

        /**
     * Displays the profile page to the user
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
         $user = Auth::user();

        $person = $user->person;
        $genders = Gender::all();
        $nationalities = Nationality::all();
        return view('user', compact('person','user', 'genders', 'nationalities'));
    }

     /**
     * Updates user profile details
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function profileUpdate()
    {
        $validatedData = request()->validate(User::profileValidationRules());

        $user = Auth::user();

        $user->update($validatedData);

        return back()->with(['type' => 'success', 'message' => 'تم تحديث الملف الشخصي بنجاح']);
    }

    /**
     * Updates user password
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function passwordUpdate()
    {
        $validatedData = request()->validate(User::passwordValidationRules());

        $user = Auth::user();

        if (Hash::check(request('current_password'), $user->password)) {
            $user->password = bcrypt(request('password'));
            $user->save();

            return back()->with(['type' => 'success', 'message' => 'تم تحديث كلمة المرور بنجاح']);
        }

        return back()->with(['type' => 'error', 'message' => 'كلمة المرور الحالية غير صحيحة']);
    }



    
        /**
     * Show the email verification notice.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function verify (Request $request)
    {
        if (Auth::check()) {
             return $request->user()->hasVerifiedEmail()
                        ? redirect()->route('login')
                        : view('auth.emailverify');
        }else {
          return  view('auth.emailverify');
        }
       // dd($request);
       
    }


    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send (Request $request)
    {
        $user = User::where('email',$request->email)->first();
        if(is_null($user)){
             return back()->with(['type' => 'error', 'message' => 'البريد الالكتروني غير موجود في قاعدة البيانات']);
        }

        if ($user->hasVerifiedEmail()) {
            return redirect()->route('login');
        }
        $user->sendEmailVerificationNotification();

        return back()->with('resent', true);
    }
    
}
