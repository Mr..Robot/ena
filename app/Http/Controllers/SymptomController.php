<?php

namespace App\Http\Controllers;

use Auth;
use App\Symptom;
use App\Http\Controllers\Controller;

class SymptomController extends Controller
{
    //     public function __Construct()
    // {
    //     request()->request->add(['doctor_id'=>Auth::user()->doctor->id]);
    // }
    /**
     * Display a list of Symptoms.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $symptoms = Symptom::getList();

        return view('dashboard.symptoms.index', compact('symptoms'));
    }

    /**
     * Show the form for creating a new Symptom
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.symptoms.add');
    }

    /**
     * Save new Symptom
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Symptom::validationRules());

        $symptom = Symptom::create($validatedData);

        return redirect()->route('dashboard.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة العرض'
        ]);
    }

    /**
     * Show the form for editing the specified Symptom
     *
     * @param \App\Symptom $symptom
     * @return \Illuminate\Http\Response
     */
    public function edit(Symptom $symptom)
    {
        return view('dashboard.symptoms.edit', compact('symptom'));
    }

    /**
     * Update the Symptom
     *
     * @param \App\Symptom $symptom
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Symptom $symptom)
    {
        $validatedData = request()->validate(
            Symptom::validationRules($symptom->id)
        );

        $symptom->update($validatedData);

        return redirect()->route('dashboard.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تم حذف العرض'
        ]);
    }

    /**
     * Delete the Symptom
     *
     * @param \App\Symptom $symptom
     * @return void
     */
    public function destroy(Symptom $symptom)
    {
        if ($symptom->diseases()->count()) {
            return redirect()->route('dashboard.symptoms.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف العرض بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $symptom->delete();

        return redirect()->route('dashboard.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تم حذف العرض'
        ]);
    }
}
