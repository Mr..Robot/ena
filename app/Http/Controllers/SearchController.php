<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specialty;
use App\Symptom;
use App\Disease;
use App\User;
use App\Doctor;

class SearchController extends Controller
{
    public function result_doctors(Request $request)
    {
        $du = null;
        $dd = null;
        if(!is_null($request->ids)){
            $symptoms = Symptom::find($request->ids);
            $diseases = Disease::with('doctors.user.person','doctors.specialty')->get();

            // $collection = collect([1, 2, 3, 4, 5]);

            // $diseases->transform(function ($item, $key) {
                
            //     $item->doctors->transform(function ($doc, $key) {
            //  return   ->forget($doc);
            // });
            // });

          //  $collection->all();
            foreach ($diseases as $key1 => $dissss) {
                foreach ($dissss->doctors as $key => $doc) {
                    if ($doc->active==0) {
                        $dissss->doctors->pull($key);
                    }
                }
            }
            $diss = collect(new Disease());
            $docs = collect(new Doctor());
            foreach ($diseases as $k1 => $dis) {
             //   return $dis->specialty->namear;
                foreach ($dis->symptoms as $key2 => $sym) {
                    foreach($symptoms as $osym)
                    {
                        if($sym->id==$osym->id){
                            $diss->prepend($dis);
                        }
                    }
                }
            }
            
            $diseases = $diss->unique('id'); 
        // return 
       return  $diseases->values();
             }
        return null;
        //     foreach ($diseases as $key => $dis) {
        //        foreach ($dis->doctors as $key => $doc) {
        //            return $dis->doctors[3]->id;
        //         $docs->prepend($doc);
        //         }
        //         $d['id'] = $dis->id;
        //         $d['name'] = $dis->name. ' || '.$dis->commonname;
        //         $doc['docId'] = $dis['doctor'];//. ' || '.$dis->commonname;
        //         $doc['docName'] = $dis->name. ' || '.$dis->commonname;
        //         $doc['docSpec'] = $dis->name. ' || '.$dis->commonname;
        //         $doc['docAvata'] = $dis->name. ' || '.$dis->commonname;
        //         $doc['docRate'] = $dis->name. ' || '.$dis->commonname;
        //     //    $d['name'] = $dis->name. ' || '.$dis->commonname;




                
        //         $dd[] = $d;  
        //     }
        //    // return $dd;
        //     $docs = $docs->unique('id'); 
        //     $du = null;
        //     foreach ($dis->doctors as $key => $doc) {
        //         $u = $doc->user;
        //         $p = $u->person;
        //         $user['id'] = $u->id;
        //         $user['name'] = $p->first_name. ' '.$p->family_name;
        //         $user['avatar'] = $u->avatar;
        //         $user['specialty'] = $doc->specialty->namear;
        //         $du[] = $user;    
        //     }
        // $res['diseases'] =  $dd;
        // $res['docs'] = $du;
        // return $res;

        }

    public function getSymptoms(Specialty $specialty)
    {
        $symptoms = collect(new Symptom());
        $symptoms->pop();
        foreach ($specialty->diseases as $des) {
            foreach ($des->symptoms as $key => $sym) {
                //if(!$symptoms->contains($sym)){
                    $symptoms->prepend($sym);
               // }
            }
            // $symptoms = $symptoms->merge($des->symptoms->where('active',true));
        }    
        $uniqueCollection = $symptoms->unique('id'); 

        return $uniqueCollection;
    }
}
