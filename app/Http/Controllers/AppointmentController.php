<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Day;
use App\Doctor;
use App\Clink;
use Auth;
use App\Http\Controllers\Controller;

class AppointmentController extends Controller
{

    // public function __Construct()
    // {
    //     request()->request->add(['doctor_id'=>Auth::user()->doctor->id]);
    // }
    /**
     * Display a list of Appointments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doc = Auth::user()->doctor;
        $appointments = Appointment::getList()->where('doctor_id',$doc->id);

        return view('dashboard.appointments.index', compact('appointments'));
    }

    /**
     * Show the form for creating a new Appointment
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $days = Day::all();
        // $doctors = Doctor::all();
        $clinks = Clink::all();

        return view('dashboard.appointments.add', compact('days', 'clinks'));
    }

    /**
     * Save new Appointment
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Appointment::validationRules());

        $appointment = Appointment::create($validatedData);

        return redirect()->route('dashboard.appointments.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافى الموعد'
        ]);
    }

    /**
     * Show the form for editing the specified Appointment
     *
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        $days = Day::all();
      //  $doctors = Doctor::all();
        $clinks = Clink::all();

        return view('dashboard.appointments.edit', compact('appointment', 'days', 'clinks'));
    }

    /**
     * Update the Appointment
     *
     * @param \App\Appointment $appointment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Appointment $appointment)
    {
        $validatedData = request()->validate(
            Appointment::validationRules($appointment->id)
        );

        $appointment->update($validatedData);

        return redirect()->route('dashboard.appointments.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل الموعد'
        ]);
    }

    /**
     * Delete the Appointment
     *
     * @param \App\Appointment $appointment
     * @return void
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();

        return redirect()->route('dashboard.appointments.index')->with([
            'type' => 'success',
            'message' => 'تم حذف الموعد'
        ]);
    }
}
