<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Doctor;
use App\Gender;
use App\Nationality;
use App\Person;
use App\Specialty;
use App\Disease;
use App\Certificate;
use Auth;
use App\Promotion;
use Carbon\Carbon;
use App\DoctorJoinRequest;
use Image;
class DoctorController extends Controller
{
    public function index()
    {
          if(request()->has('specialty') && request()->specialty != 0){
            $doctors = Doctor::where('active',true)->where('specialty_id',request()->specialty)->with('specialty','user.person')->get()->chunk(3);
            $id = request()->specialty;
        }else{
            $doctors = Doctor::where('active',true)->with('specialty','user.person')->get()->chunk(3);
            $id = 0;
        }   
        return view('doctors.index',compact('doctors','id'));
    }
    public function rateDoc(Request $request)
    {
        request()->validate(['rate' => 'required']);
    
        $doc = Doctor::find($request->id);
        
        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->rate;
        $rating->user_id = Auth::id();

        if(is_null($doc->ratings()->where('user_id', Auth::id())->first())){
            $doc->ratings()->save($rating);
            
        }else{
      
        $rating = $doc->ratings()->where('user_id', Auth::id())->first();
            $rating->rating = $request->rate;
            $rating->update();
        }
        
        $doc->rate = $doc->userAverageRating;
        $doc->save();
        return redirect()->back();
    }

    public function dashboard(User $user)
    {
       //  dd($user);
        if ($user->isAdmin) {
           //dd($user);
            if (Auth::id() == $user->id) {
           //     dd($user);
               return view('layouts.dashboard', compact('user'));// 'person','doctor', 'genders','diseases', 'nationalities','certificates'));
            }
        }      
        return redirect()->back();
    }

    public function loadProfile(User $user)
    {

        //  $doctor = $user->doctor;
        // $rating = new \willvincent\Rateable\Rating;
        // $rating->rating = 5;
        // $rating->user_id = $user->id;
        // $doctor->ratings()->save($rating);

            $doctor = $user->doctor;
            $person = $user->person;
            $genders = Gender::all();
            $nationalities = Nationality::all();
            $diseases = Disease::where('specialty_id',$doctor->specialty_id)->get();
            $certificates = Certificate::all();
         //   $diseases = Disease::all();

            $doctor->diseases = $doctor->diseases->pluck('id')->toArray();
            $doctor->certificates = $doctor->certificates->pluck('id')->toArray();

            return view('doctors.show', compact('person','user','doctor', 'genders','diseases', 'nationalities','certificates'));
    }


    
    
    // public function jsonDocs()
    // {
    //     if(request()->has('specialty') && request()->specialty != 0){
    //         $doctors = Doctor::where('specialty_id',request()->specialty)->with('specialty','user.person')->get()->chunk(3);
    //         $id = request()->specialty;
    //     }else{
    //         $doctors = Doctor::with('specialty','user.person')->get()->chunk(3);
    //         $id = 0;
    //     }   
    //     return view('doctors.index',compact('doctors','id'));
    // }



    public function show(User $user)
    {
    //  $user =  is_null($user)? Auth::User()  : $user;
    //  dd($user);
        // if(!is_null($user)){
            if($user->isDoctor){
            return $this->loadProfile($user);
        // }
        }else {
            
            return redirect('/doctors/all');//->back();
        }
    }

    public function update(Request $request, Person $person)
    {
        $validatedData = request()->validate(
            Person::validationRules($person->id)
        );

       $person->update($validatedData);
       // Handle the user upload of avatar
    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($request->file('avatar'))->resize(300, 300)->save('avatars/'.$filename);
    		$user = $person->user;
    		$user->avatar = $filename;
    		$user->save();
    	}
        return back();
    }
    public function editDisease(Request $request)
    {
        $user = Auth::user();
        //dd($user->id);
        $doctor = $user->doctor;     
        $doctor->diseases()->sync(request('diseases'));
          return back();
    }
//تعرض فورم تسجيل الدكتور
    public function registeration()
    {


        $nationalities = Nationality::all();
        $specialties = Specialty::all();
        $data = [
            'nationalities' => $nationalities,
            'specialties'=> $specialties
        ];

        if (Auth::guest()){
            return view ('doctors.register')->with($data);
        } else {
            $user = Auth::user();
            if($user->isDoctor){
            $doctor = $user->doctor;
            $person = $user->person;
            $genders = Gender::all();
            $nationalities = Nationality::all();
            $diseases = Disease::all();
            $certificates = Certificate::all();
         //   $diseases = Disease::all();

            $doctor->diseases = $doctor->diseases->pluck('id')->toArray();
            $doctor->certificates = $doctor->certificates->pluck('id')->toArray();

            return view('doctors.show', compact('person','user','doctor', 'genders','diseases', 'nationalities','certificates'));
        }else {
            return redirect()->back();
        }
        }
    }

    // public function promote(Doctor $doctor)
    // {
    //     $user = $doctor->user;
    //     $promotion = new Promotion();
    //     $promotion->user_id = $user->id;
    //     $promotion->isaccepted = 0;
    //     $promotion->save();
    //     return redirect()->back();

    // }
   
    public function store(Request $request)
    {
        $rules = [
            'first_name' => 'required|string|min:2|max:50|regex:/^[\pL\s\-]+$/u',
            'second_name' => 'required|string|min:2|max:50|regex:/^[\pL\s\-]+$/u',
            'last_name' => 'required|string|min:2|max:50|regex:/^[\pL\s\-]+$/u',
            'family_name' => 'required|string|min:2|max:50|regex:/^[\pL\s\-]+$/u',
            'date_of_birth' => 'required|date',
            'gender' => 'required|integer|min:0',
            'nationality_id' => 'required|integer|exists:nationalities,id',
            'specialty_id' => 'required|integer|exists:specialties,id',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'link' => 'required|max:5999',
            'dgree' => 'required|integer',
            'ref_no' => 'required|integer|unique:doctors,ref_no',
        ];

        $messages = [
            'first_name.regex'=>'الاسم الاول يجب ان يتكون من حروف فقط',
            'second_name.regex'=>'اسم الاب يجب ان يتكون من حروف فقط',
            'last_name.regex'=>'اسم الجد يجب ان يتكون من حروف فقط',
            'family_name.regex'=>'اللفب يجب ان يتكون من حروف فقط',
            'gender.required' => 'الرجاء أختيار الجنس',
            'gender.min' => 'الرجاء أختيار الجنس',
            'ref_no.unique'=> 'الرقم النقابي مستخدم مسبقاً',
            'ref_no.integer'=> 'الرقم النقابي يجب ان يتكون من ارقام فقطً',
            'dgree.integer'=> 'الدرجة يجب ان تتكون من ارقام فقطً'

        ];

       $this->validate($request, $rules,$messages);
       
       $filename = 'avatar.png';
    //    if($request->hasFile('avatar')){
    // 		$avatar = $request->file('avatar');
    // 		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    //         Image::make($request->file('avatar'))->resize(300, 300)->save('avatars/'.$filename);
    // 	}
       $user = new User;
       $user->email = $request->email;
       $user->password = bcrypt($request->get('password'));
       $user->name = $request->first_name;
       $user->type_id = 2;
       $user->active = 0;
       $user->isDoctor = 1;
       $user->IsAdmin = 0;
       $user->avatar  =  $filename;

       $user->save();
       
        $person = new Person;
        $person->first_name = $request->first_name;
        $person->second_name = $request->second_name;
        $person->last_name = $request->last_name;
        $person->family_name = $request->family_name;
        $person->date_of_birth = Carbon::parse($request->date_of_birth)->format('Y-m-d');
        $person->gender_id = $request->gender;
        $person->nationality_id = $request->nationality_id;
        $person->user_id = $user->id;

        $person->save();

        $doctor = New Doctor;
        $doctor->dgree = $request->dgree;
        $doctor->ref_no = $request->ref_no;
        $doctor->user_id = $user->id;
        $doctor->specialty_id = $request->specialty_id;

        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = 5;
        $rating->user_id = $user->id;
        $doctor->save();
        $doctor->ratings()->save($rating);
        
        $jrequest = New DoctorJoinRequest;
        $jrequest->user_id = $user->id;
        $jrequest->save();

        $request->request->add(['doctor_id' => $doctor->id]);
        $request->request->add(['doctor_join_request_id' => $jrequest->id]);
        $request->request->add(['name' => 'newCer']);
     
        $validatedData = request()->validate(Certificate::validationRules());

        unset($validatedData['link']);
        $certificate = Certificate::create($validatedData);

        $certificate->addMediaFromRequest('link')->toMediaCollection('link');

        return redirect()->route('verify');
    }


}
