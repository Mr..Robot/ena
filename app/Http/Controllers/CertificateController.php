<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\Doctor;
use App\Http\Controllers\Controller;
use Auth;
class CertificateController extends Controller
{
    //     public function __Construct()
    // {
    //     request()->request->add(['doctor_id'=> Auth::user()->doctor->id]);
    // }
    /**
     * Display a list of Certificates.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $doc = Auth::user()->doctor;
        $certificates = Certificate::getList()->where('doctor_id',$doc->id);

        return view('dashboard.certificates.index', compact('certificates'));
    }

    /**
     * Show the form for creating a new Certificate
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // $doctors = Doctor::all();

        return view('dashboard.certificates.add');//, compact('doctors'));
    }

    /**
     * Save new Certificate
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
                  
       
        $validatedData = request()->validate(Certificate::validationRules());

        unset($validatedData['link']);
        $certificate = Certificate::create($validatedData);

        $certificate->addMediaFromRequest('link')->toMediaCollection('link');

        return redirect()->route('dashboard.certificates.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة الشهادة '
        ]);
    }

    /**
     * Show the form for editing the specified Certificate
     *
     * @param \App\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
       // $doctors = Doctor::all();

        return view('dashboard.certificates.edit', compact('certificate'));
    }

    /**
     * Update the Certificate
     *
     * @param \App\Certificate $certificate
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Certificate $certificate)
    {
        $validatedData = request()->validate(
            Certificate::validationRules($certificate->id)
        );

        unset($validatedData['link']);
        $certificate->update($validatedData);

        $certificate->addMediaFromRequest('link')->toMediaCollection('link');

        return redirect()->route('dashboard.certificates.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل الشهادة '
        ]);
    }

    /**
     * Delete the Certificate
     *
     * @param \App\Certificate $certificate
     * @return void
     */
    public function destroy(Certificate $certificate)
    {
        // if ($certificate->promotions()->count()) {
        //     return redirect()->route('dashboard.certificates.index')->with([
        //         'type' => 'error',
        //         'message' => 'This record cannot be deleted as there are relationship dependencies.'
        //     ]);
        // }

        $certificate->delete();

        return redirect()->route('dashboard.certificates.index')->with([
            'type' => 'success',
            'message' => 'تم حذف الشهادة'
        ]);
    }
}
