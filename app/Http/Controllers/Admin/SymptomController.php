<?php

namespace App\Http\Controllers\Admin;

use App\Symptom;
use App\Http\Controllers\Controller;

class SymptomController extends Controller
{
    /**
     * Display a list of Symptoms.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $symptoms = Symptom::getList();

        return view('admin.symptoms.index', compact('symptoms'));
    }

    /**
     * Show the form for creating a new Symptom
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.symptoms.add');
    }

    /**
     * Save new Symptom
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validatedData = request()->validate(Symptom::validationRules());

        $symptom = Symptom::create($validatedData);

        return redirect()->route('admin.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة العرض'
        ]);
    }

    /**
     * Show the form for editing the specified Symptom
     *
     * @param \App\Symptom $symptom
     * @return \Illuminate\Http\Response
     */
    public function edit(Symptom $symptom)
    {
        return view('admin.symptoms.edit', compact('symptom'));
    }

    /**
     * Update the Symptom
     *
     * @param \App\Symptom $symptom
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Symptom $symptom)
    {
        $validatedData = request()->validate(
            Symptom::validationRules($symptom->id)
        );

        $symptom->update($validatedData);

        return redirect()->route('admin.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل العرض'
        ]);
    }

    /**
     * Delete the Symptom
     *
     * @param \App\Symptom $symptom
     * @return void
     */
    public function destroy(Symptom $symptom)
    {
        if ($symptom->diseases()->count()) {
            return redirect()->route('admin.symptoms.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف العرض بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $symptom->delete();

        return redirect()->route('admin.symptoms.index')->with([
            'type' => 'success',
            'message' => 'تم حذف العرض'
        ]);
    }
}
