<?php

namespace App\Http\Controllers\Admin;

use App\Specialty;
use App\Http\Controllers\Controller;

class SpecialtyController extends Controller
{
    /**
     * Display a list of Specialties.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specialties = Specialty::getList();

        return view('admin.specialties.index', compact('specialties'));
    }

    /**
     * Show the form for creating a new Specialty
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.specialties.add');
    }

    /**
     * Save new Specialty
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $mesages = [
            'namear.unique' => 'الاسم مستخدم مسبقاً',
            'nameen.unique' => 'الاسم مستخدم مسبقاً',
        ];
        $validatedData = request()->validate(Specialty::validationRules(),$mesages);
         

        $specialty = Specialty::create($validatedData);

        return redirect()->route('admin.specialties.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة التخصص'
        ]);
    }

    /**
     * Show the form for editing the specified Specialty
     *
     * @param \App\Specialty $specialty
     * @return \Illuminate\Http\Response
     */
    public function edit(Specialty $specialty)
    {
        return view('admin.specialties.edit', compact('specialty'));
    }

    /**
     * Update the Specialty
     *
     * @param \App\Specialty $specialty
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Specialty $specialty)
    {
        $mesages = [
            'namear.unique' => 'الاسم مستخدم مسبقاً',
            'nameen.unique' => 'الاسم مستخدم مسبقاً',
        ];
        $validatedData = request()->validate(
            Specialty::validationRules($specialty->id),$mesages);


        $specialty->update($validatedData);

        return redirect()->route('admin.specialties.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل التخصص'
        ]);
    }

    /**
     * Delete the Specialty
     *
     * @param \App\Specialty $specialty
     * @return void
     */
    public function destroy(Specialty $specialty)
    {
        if ($specialty->diseases()->count() || $specialty->doctors()->count()) {
            return redirect()->route('admin.specialties.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف التخصص بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $specialty->delete();

        return redirect()->route('admin.specialties.index')->with([
            'type' => 'success',
            'message' => 'تم حذف التخصص'
        ]);
    }
}
