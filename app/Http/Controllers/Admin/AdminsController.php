<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class AdminsController extends Controller
{    
    /**
     * Display a list of People.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all()->except(1);

        return view('admin.admins.index', compact('admins'));
    }

    /**
     * Show the form for creating a new Admin
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.add');
    }

    /**
     * Save new Admin
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validatedData = request()->validate(Admin::crateValidationRules());
      $validatedData['password'] = bcrypt($request->password);
        $admin = Admin::create($validatedData);

        return redirect()->route('admin.admins.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة مشرف جديد'
        ]);
    }

    /**
     * Show the form for editing the specified Admin
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.admins.edit', compact('admin'));
    }

      /**
     * Show the form for editing the specified Admin
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\Response
     */
    public function activation(Admin $admin)
    {
        $admin->active = !$admin->active;
        $admin->save();
         return redirect()->route('admin.admins.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل فاعلية المشرف'
        ]);
    }

    /**
     * Update the Admin
     *
     * @param \App\Admin $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Admin $admin)
    {
        $validatedData = request()->validate(
            Admin::profileValidationRules($admin->id)
        );

        $admin->update($validatedData);

        return redirect()->route('admin.admins.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل بيانات المشرف'
        ]);
    }

    /**
     * Delete the Admin
     *
     * @param \App\Admin $admin
     * @return void
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();

        return redirect()->route('admin.admins.index')->with([
            'type' => 'success',
            'message' => 'تم حذف المشرف بنجاح'
        ]);
    }
}
