<?php

namespace App\Http\Controllers\Admin;



use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlacklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users =  User::where('blocked', true)->with('person')->get();
        return view('admin.blacklist.index', compact('users'));
    }
    public function add(User $user)
    {
        $user->blocked = true;
        $user->active = false;
        $user->save();
        if($user->isDoctor)
        {
             $doc = $user->doctor;//
             $doc->active = false;
             $doc->save();
        }

     return redirect()->back();
    }
    public function remove(User $user)
    {
        $user->blocked = false;
        $user->active = true;
        $user->save();
        if($user->isDoctor)
        {
             $doc = $user->doctor;//
             $doc->active = true;
             $doc->save();
        }
        
     return redirect()->back();
    }
}
