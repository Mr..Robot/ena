<?php

namespace App\Http\Controllers\Admin;

use App\Clink;
use App\Http\Controllers\Controller;

class ClinkController extends Controller
{
    /**
     * Display a list of Clinks.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clinks = Clink::getList();

        return view('admin.clinks.index', compact('clinks'));
    }

    
    /**
     * Show the form for creating a new Clink
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clinks.add');
    }

    /**
     * Save new Clink
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $mesages = [
            'name.unique' => 'الاسم مستخدم مسبقاً',   
        ];
        $validatedData = request()->validate(Clink::validationRules(), $mesages);

        $clink = Clink::create($validatedData);

        return redirect()->route('admin.clinks.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة المصحة'
        ]);
    }

    /**
     * Show the form for editing the specified Clink
     *
     * @param \App\Clink $clink
     * @return \Illuminate\Http\Response
     */
    public function edit(Clink $clink)
    {
        return view('admin.clinks.edit', compact('clink'));
    }

    /**
     * Update the Clink
     *
     * @param \App\Clink $clink
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Clink $clink)
    {
         $mesages = [
            'name.unique' => 'الاسم مستخدم مسبقاً',   
        ];
        $validatedData = request()->validate(
            Clink::validationRules($clink->id)
        , $mesages);

        $clink->update($validatedData);

        return redirect()->route('admin.clinks.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل المصحة'
        ]);
    }

    /**
     * Delete the Clink
     *
     * @param \App\Clink $clink
     * @return void
     */
    public function destroy(Clink $clink)
    {
        if ($clink->appointments()->count()) {
            return redirect()->route('admin.clinks.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف المصحة بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $clink->delete();

        return redirect()->route('admin.clinks.index')->with([
            'type' => 'success',
            'message' => 'تم حذف المصحة'
        ]);
    }
}
