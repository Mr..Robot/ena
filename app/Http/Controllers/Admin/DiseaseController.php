<?php

namespace App\Http\Controllers\Admin;

use App\Disease;
use App\Specialty;
use App\Symptom;
use App\Http\Controllers\Controller;

class DiseaseController extends Controller
{
    /**
     * Display a list of Diseases.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diseases = Disease::getList();

        return view('admin.diseases.index', compact('diseases'));
    }

    /**
     * Show the form for creating a new Disease
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $specialties = Specialty::all();
        $symptoms = Symptom::all();

        return view('admin.diseases.add', compact('specialties', 'symptoms'));
    }

    /**
     * Save new Disease
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
                $messages = [
            'code.max' => 'حقل الرمز يجب ان لا يتجاوز 10 خانات',
            'code.unique' => 'الرمز مستخدم من قبل',
        ];
        $validatedData = request()->validate(Disease::validationRules(),$messages);

        unset($validatedData['symptoms']);
        $disease = Disease::create($validatedData);

        $disease->symptoms()->sync(request('symptoms'));

        return redirect()->route('admin.diseases.index')->with([
            'type' => 'success',
            'message' => 'تمت اضافة المرض'
        ]);
    }

    /**
     * Show the form for editing the specified Disease
     *
     * @param \App\Disease $disease
     * @return \Illuminate\Http\Response
     */
    public function edit(Disease $disease)
    {
        $specialties = Specialty::all();
        $symptoms = Symptom::all();

        $disease->symptoms = $disease->symptoms->pluck('id')->toArray();

        return view('admin.diseases.edit', compact('disease', 'specialties', 'symptoms'));
    }

    /** 
     * Update the Disease
     *
     * @param \App\Disease $disease
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Disease $disease)
    {
        $messages = [
            'code.max' => 'حقل الرمز يجب ان لا يتجاوز 10 خانات',
            'code.unique' => 'الرمز مستخدم من قبل',
        ];

      //  dd($messages);
        $validatedData = request()->validate(Disease::validationRules($disease->id), $messages);


        unset($validatedData['symptoms']);
        $disease->update($validatedData);

        $disease->symptoms()->sync(request('symptoms'));

        return redirect()->route('admin.diseases.index')->with([
            'type' => 'success',
            'message' => 'تم تعديل المرض'
        ]);
    }

    /**
     * Delete the Disease
     *
     * @param \App\Disease $disease
     * @return void
     */
    public function destroy(Disease $disease)
    {
        if ($disease->symptoms()->count() || $disease->doctors()->count()) {
            return redirect()->route('admin.diseases.index')->with([
                'type' => 'error',
                'message' => 'لايمكن حذف المرض بسبب وجود علاقات مع جداول اخرى'
            ]);
        }

        $disease->delete();

        return redirect()->route('admin.diseases.index')->with([
            'type' => 'success',
            'message' => 'تم حذف المرض'
        ]);
    }
}
