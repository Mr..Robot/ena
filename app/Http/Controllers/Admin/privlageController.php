<?php

namespace App\Http\Controllers\Admin;
use App\Doctor;
use App\User;
use App\Person;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class privlageController extends Controller
{
        /**
     * Display a list of Doctors.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =  User::where('blocked', false)->with('person')->get();

        return view('admin.privilages.index', compact('users'));
    }

        public function edit($id)
    {

        $user =  User::where('id',$id)->with('person')->first();
     
        return view('admin.privilages.edit', compact('user'));
    }
        public function update (Request $request, $id)
    {
      //  dd($request->all());

        $user =  User::where('id', $id)->first();
        $isAdmin = $request->isAdmin == 1? true:false;
        //$user->update($request->isAdmin);
         $user->isAdmin = $isAdmin;
         $user->save();


        return $this->index();
    }
}
