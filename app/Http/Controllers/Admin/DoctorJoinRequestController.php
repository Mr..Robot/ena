<?php

    namespace App\Http\Controllers\Admin;
    
    use App\DoctorJoinRequest;
    use App\Person;
    use App\User;
    
    use App\Certificate;
    use App\Description;
    use App\Http\Controllers\Controller;

    class DoctorJoinRequestController extends Controller
    {
        /**
         * Display a list of DoctorJoinRequests.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
    //         $users = User::where(['isDoctor'=>true,'active'=>false])->whereNotNull('email_verified_at')->get();
    //       //  dd($users);
             $requests = DoctorJoinRequest::getList();
    //   dd($requests);
             return view('admin.requests.index', compact('requests'));
        }
    
        // /**
        //  * Show the form for creating a new DoctorJoinRequest
        //  *
        //  * @return \Illuminate\Http\Response
        //  */
        // public function create()
        // {
        //     $people = Person::all();
        //     $certificates = Certificate::all();
        //     $descriptions = Description::all();
    
        //     return view('admin.requests.add', compact('people', 'certificates', 'descriptions'));
        // }
    
        // /**
        //  * Save new DoctorJoinRequest
        //  *
        //  * @return \Illuminate\Http\RedirectResponse
        //  */
        // public function store()
        // {
        //     $validatedData = request()->validate(DoctorJoinRequest::validationRules());
    
        //     $jrequest = DoctorJoinRequest::create($validatedData);
    
        //     return redirect()->route('admin.requests.index')->with([
        //         'type' => 'success',
        //         'message' => ' added'
        //     ]);
        // }
    
        // /**
        //  * Show the form for editing the specified DoctorJoinRequest
        //  *
        //  * @param \App\DoctorJoinRequest $jrequest
        //  * @return \Illuminate\Http\Response
        //  */
        // public function edit(DoctorJoinRequest $jrequest)
        // {
        //     $people = Person::all();
        //     $certificates = Certificate::all();
        //     $descriptions = Description::all();
    
        //     return view('admin.requests.edit', compact('jrequest', 'people', 'certificates', 'descriptions'));
        // }
    
        /**
         * Update the DoctorJoinRequest
         *
         * @param \App\DoctorJoinRequest $jrequest
         * @return \Illuminate\Http\RedirectResponse
         */
        public function approve(DoctorJoinRequest $jrequest)
        {
            $jrequest->approve()->save();
            $user = $jrequest->user;
            $user->active = true;
            $user->save();

            // $jrequest->delete();
            return redirect()->route('admin.requests.index')->with([
                'type' => 'success',
                'message' => ' تم قبول طلب تسجيل الطبيب'
            ]);
        }
    
        /**
         * Delete the DoctorJoinRequest
         *
         * @param \App\DoctorJoinRequest $jrequest
         * @return void
         */
        public function revoke(DoctorJoinRequest $jrequest)
        {    
            $jrequest->revoke()->save();
            $user = $jrequest->user;
            $user->active = false;
            $user->save();



            return redirect()->route('admin.requests.index')->with([
                'type' => 'error',
                'message' => ' تم رفض طلب تسجيل الطبيب'
            ]);
        }
    }
    