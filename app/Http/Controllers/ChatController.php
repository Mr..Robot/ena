<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Chat;
use Musonza\Chat\Models\Conversation;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class ChatController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function loadConUsers()
    {
        $users[] = null;
        $conversations = Chat::conversations()->for(Auth::user())->get();

        foreach ($conversations as $key => $conversation) {
          foreach ($conversation->users as $key => $user) {
              if ($user->id != Auth::id()) {
                $user->setAttribute('last_message', !is_null($conversation->last_message) ? Carbon::createFromTimeStamp(strtotime($conversation->last_message->created_at))->diffForHumans() : 'No Messages');                  
                $users[] = $user;
              }
          }
        }
        return $users;
    }
    public function index()
    {
        $users = $this->loadConUsers();
        $messages = null;
        $user = null;
        return view('chat', compact('users','messages','user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    { // if ($user->isDoctor) {
        $users = $this->loadConUsers();
     
        $conversation= null;
        $messages = null;
        $conversation = Chat::conversations()->between(Auth::id(),$user->id);
        if(is_null($conversation))
            $conversation = Chat::createConversation([Auth::id(),$user->id]);

        $messages = $conversation->messages;
        Chat::conversation($conversation)->for(Auth::user())->readAll();
        return view('chat', compact('users','messages','user'));
  //   } else {
              return redirect('/chats');
    //    }
    }

    public function send(Request $request, User $user)
    {
      //  if ($user->isDoctor) {
         
        $this->validate($request, [
            'message'=>'required'
        ]);

      
        $users = $this->loadConUsers();

        $conversation = Chat::conversations()->between(Auth::user(), $user->id);
        $path = $request->message;
 
        if(!is_null($request->attach)) {         
            $path = Storage::putFile('public/attachments', $request->attach);
            $message = Chat::message($path)
                ->type('file')
                ->from(Auth::user())
                ->to($conversation)
                ->send();
        }else{

            $message = Chat::message($path)
            ->from(Auth::user())
            ->to($conversation)
            ->send();
        }
        
        $messages = $conversation->messages;
        return view('chat', compact('users','messages','user'));

        // }else {
        //     return redirect()->back();
        // }
    }
      
        // public function store()
        // {
        //     $participants = [auth()->user()->id];
        //     $conversation = Chat::createConversation($participants);
        //     return response($conversation);
        // }
        // public function participants($conversationId)
        // {
        //     $participants = Chat::conversations()->getById($conversationId)->users;
        //     return response($participants);
        // }
        // public function join(Request $request, Conversation $conversation)
        // {
        //     Chat::conversation($conversation)->addParticipants(auth()->user());
        //     return response('');
        // }
        // public function leaveConversation(Request $request, Conversation $conversation)
        // {
        //     Chat::conversation($conversation)->removeParticipants(auth()->user());
        //     return response('');
        // }
        // public function getMessages(Request $request, Conversation $conversation)
        // {
        //     $messages = Chat::conversation($conversation)->for(auth()->user())->getMessages();
        //     return response($messages);
        // }
        // public function deleteMessages(Conversation $conversation)
        // {
        //     Chat::conversation($conversation)->for(auth()->user())->clear();
        //     return response('');
        // }
        // public function sendMessage(Request $request, User $user)
        // {
        //     $conversation = Chat::conversations()->between(Auth::user(), $user);
        //     $message = Chat::message($request->message)
        //               ->from(auth()->user())
        //               ->to($conversation)
        //               ->send();
        //   return response($message);
        // }
}
