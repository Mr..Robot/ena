<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'second_name','country','state', 'last_name', 'family_name', 'datebirth', 'gender_id', 'nationality_id'
    ];

    protected $appends = ['fullName'];
    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'first_name' => 'required|string',
            'second_name' => 'required|string',
            'last_name' => 'required|string',
            'family_name' => 'required|string',
            'country'=> 'string',
            'state'=> 'string',
            'date_of_birth' => 'required|date',
            'gender_id' => 'required|numeric|exists:genders,id',
            'nationality_id' => 'required|numeric|exists:nationalities,id',
        ];
    }

    /**
     * Get the gender for the Person.
     */
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    /**
     * Get the nationality for the Person.
     */
    public function nationality()
    {
        return $this->belongsTo('App\Nationality');
    }

    /**
     * Get the promotions for the Person.
     */
 
    public function fullname()
    {
        return $this->fullName;
    }
       public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->family_name;
    }


    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['gender', 'nationality'])->paginate(10);
    }
}
