<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Blacklist extends User
{
    public static function alls()
    {
        return static::where('blocked', false)->get();
    }
}
