<?php

namespace App;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certificate extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'link', 'doctor_id','doctor_join_request_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'name' => 'required|string',
            'link' => 'required|image',
            'doctor_id' => 'required|numeric|exists:doctors,id',
            'doctor_join_request_id' => 'required|numeric|exists:doctor_join_requests,id',
        ];
    }

    /**
     * Spatie media library collections
     *
     * @return void
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('link')->singleFile();
    }

    /**
     * Get the doctor for the Certificate.
     */
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }

        /**
     * Get the doctor for the Certificate.
     */
    public function jrequest()
    {
        return $this->belongsTo('App\DoctorJoinRequest');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['doctor', 'media'])->paginate(10);
    }
}
