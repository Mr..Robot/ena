<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from', 'to', 'day_id', 'doctor_id', 'clink_id'
    ];

    /**
     * Validation rules
     *
     * @return array
     **/
    public static function validationRules()
    {
        return [
            'from' => 'required',
            'to' => 'required',
            'doctor_id' => 'numeric',
            'day_id' => 'required|numeric|exists:days,id',
            'clink_id' => 'required|numeric|exists:clinks,id',
        ];
    }

    /**
     * Get the day for the Appointment.
     */
    public function day()
    {
        return $this->belongsTo('App\Day');
    }

    /**
     * Get the doctor for the Appointment.
     */
    public function doctor()
    {
        return $this->belongsTo('App\Doctor');
    }

    /**
     * Get the clink for the Appointment.
     */
    public function clink()
    {
        return $this->belongsTo('App\Clink');
    }

    /**
     * Returns the paginated list of resources
     *
     * @return \Illuminate\Pagination\Paginator
     **/
    public static function getList()
    {
        return static::with(['day', 'doctor', 'clink'])->paginate(10);
    }
}
